﻿Imports System.Data.Entity
Imports System.Data.Entity.ModelConfiguration.Conventions
Imports Ubbie.Entidades.Negocio
Imports Ubbie.Entidades.Seguridad
Imports Ubbie.Entidades.Soporte

Public NotInheritable Class DBEF
    Inherits DbContext

    Public Shared Property DatabaseConnectionString As String

    Public Sub New()
        MyBase.New(DatabaseConnectionString)
        Database.SetInitializer(Of DBEF)(Nothing)
    End Sub

    Public Sub New(connectionString As String)
        MyBase.New(connectionString)
        Database.SetInitializer(Of DBEF)(Nothing)
    End Sub

    'Negocio
    Public Property Noticia As DbSet(Of Noticia)
    Public Property NoticiaCategoria As DbSet(Of NoticiaCategoria)
    Public Property Reclamo As DbSet(Of Reclamo)
    Public Property Consorcio As DbSet(Of Consorcio)
    Public Property Unidad As DbSet(Of Unidad)
    Public Property TipoUnidad As DbSet(Of TipoUnidad)
    Public Property Gasto As DbSet(Of Gasto)
    Public Property TipoGasto As DbSet(Of TipoGasto)
    Public Property Expensa As DbSet(Of Expensa)
    Public Property PagoExpensa As DbSet(Of PagoExpensa)


    'Seguridad
    Public Property Bitacora As DbSet(Of Bitacora)
    Public Property Familia As DbSet(Of Familia)
    Public Property Patente As DbSet(Of Patente)
    Public Property FamiliaPatente As DbSet(Of FamiliaPatente)
    Public Property Rol As DbSet(Of Rol)
    Public Property RolFamilia As DbSet(Of RolFamilia)
    Public Property RolFamiliaPatente As DbSet(Of RolFamiliaPatente)

    Public Property Usuario As DbSet(Of Usuario)
    Public Property UsuarioNotificacion As DbSet(Of UsuarioNotificacion)

    'Soporte
    Public Property ConfigSetting As DbSet(Of ConfigSetting)
    Public Property ConfigGroup As DbSet(Of ConfigGroup)
    Public Property Email As DbSet(Of Email)
    Public Property EmailAttachment As DbSet(Of EmailAttachment)
    Public Property EmailTemplate As DbSet(Of EmailTemplate)
    Public Property File As DbSet(Of File)
    Public Property FileContent As DbSet(Of FileContent)
    Public Property EventLog As DbSet(Of EventLog)

    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
        modelBuilder.Conventions.Remove(Of PluralizingTableNameConvention)()
    End Sub

    Public Overloads Function SaveChanges(usuarioId As Integer) As Integer

        For Each e In ChangeTracker.Entries().Where(Function(x) x.State = EntityState.Added Or x.State = EntityState.Modified)


            Dim fechaCreacion = e.Entity.GetType().GetProperty("FechaCreacion")
            Dim creadoPor = e.Entity.GetType().GetProperty("CreadoPor")

            Dim fechaEdicion = e.Entity.GetType().GetProperty("FechaEdicion")
            Dim editadoPor = e.Entity.GetType().GetProperty("EditadoPor")

            If (fechaCreacion IsNot Nothing) Then
                e.Property("FechaCreacion").CurrentValue = Now
            End If

            If (creadoPor IsNot Nothing) Then
                e.Property("CreadoPor").CurrentValue = usuarioId
            End If

            If (fechaEdicion IsNot Nothing) Then
                e.Property("FechaEdicion").CurrentValue = Now
            End If

            If (editadoPor IsNot Nothing) Then
                e.Property("EditadoPor").CurrentValue = usuarioId
            End If

        Next



        Return SaveChanges()
    End Function

End Class
