﻿namespace Ubbie.Servicios.Base
{
    public class UsuarioActivo
    {
        public int Id { get; set; }
        public string DireccionIP { get; set; }
    }
}
