﻿using System.Collections.Generic;

namespace Ubbie.Servicios.Base
{
    public class PagerParameters
    {
		// Input
        public string Query { get; set; }
        public Dictionary<string, object> Parameters { get; set; } = new Dictionary<string, object>();

        public int PageSize { get; set; }
		public int PageIndex { get; set; }
		public string SortField { get; set; }
		public string SortDirection { get; set; }

		// Output
		public int RowCount { get; set; }
		public bool PartialResults { get; set; }

		// Calculated
		public int PageCount => (PageSize > 0) ? (RowCount / PageSize) + 1 : 0;
        public int FirstRow => (PageIndex * PageSize) + 1;
        public int LastRow => ((PageIndex + 1) * PageSize) > RowCount ? RowCount : ((PageIndex + 1) * PageSize);
    }
}
