﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ubbie.Servicios.Base
{
    public class ServiceResponse<T>
    {
        public int ReturnValue { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnName { get; set; }
        public List<ServiceError> Errors { get; set; } = new List<ServiceError>();
        public T Data { get; set; }

        public string MessageTitle { get; set; }
        public string MessageText { get; set; }

        public bool Redirect => !string.IsNullOrEmpty(RedirectUrl);
        public bool Reload { get; set; }
        public string RedirectUrl { get; set; }
        public int RedirectTime { get; set; } // In miliseconds

        public bool Status => !Errors.Any();


        public List<string> HtmlViews { get; set; } = new List<string>();






        public void AddError(Exception ex)
        {
            Errors.Add(new ServiceError(ex));
        }
        public void AddError(string errorMessage)
        {
            Errors.Add(new ServiceError(errorMessage));
        }
        public void AddError(string errorCode, string errorMessage)
        {
            Errors.Add(new ServiceError(errorCode, errorMessage));
        }
        public void AddError(string errorCode, string errorMessage, ServiceErrorLevel errorLevel)
        {
            Errors.Add(new ServiceError(errorCode, errorMessage, errorLevel));
        }
        public void AddError(ServiceError serviceError)
        {
            Errors.Add(serviceError);
        }
        public void AddErrors(List<ServiceError> serviceErrorList)
        {
            foreach (var e in serviceErrorList)
                Errors.Add(e);
        }
    }

    public class ServiceResponse : ServiceResponse<object> { }

}
