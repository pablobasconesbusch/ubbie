﻿using System;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;
using Ubbie.DAL;

namespace Ubbie.Servicios.Base
{
    public class ServicioBase
    {
        private const string DBContextName = "DBContext";
        private const string ActiveUserContextName = "ActiveUser";

        internal static DBEF DB
        {
            get
            {
                var db = CallContext.GetData(DBContextName) as DBEF;

                if (db == null)
                {
                    db = new DBEF();
                    CallContext.SetData(DBContextName, db);
                }

                return db;
            }
        }
        public static void ResetContext()
        {
            DisposeContext();
            CallContext.SetData(DBContextName, new DBEF());
        }
        public static void DisposeContext()
        {
            var db = CallContext.GetData(DBContextName) as DBEF;

            if (db != null)
            {
                db.Dispose();
                CallContext.SetData(DBContextName, null);
            }
        }

        public static UsuarioActivo UsuarioActivo => CallContext.GetData(ActiveUserContextName) as UsuarioActivo;
        public static void SetActiveUser(UsuarioActivo usuarioActivo)
        {
            CallContext.SetData(ActiveUserContextName, usuarioActivo);
        }

        public static DataTable ExecuteQuery(string query)
        {
            var dt = new DataTable();
            try
            {
                if (DB.Database.Connection.State != ConnectionState.Open)
                    DB.Database.Connection.Open();


                var da = new SqlDataAdapter(query, DB.Database.Connection.ConnectionString);
                da.FillSchema(dt, SchemaType.Source);
                da.FillLoadOption = LoadOption.PreserveChanges;
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (DB.Database.Connection.State != ConnectionState.Open)
                    DB.Database.Connection.Close();
            }

            return dt;
        }

        public static int ExecuteSqlCommand(string query)
        {
            try
            {
                if (DB.Database.Connection.State != ConnectionState.Open)
                    DB.Database.Connection.Open();


                return DB.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, query);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (DB.Database.Connection.State != ConnectionState.Open)
                    DB.Database.Connection.Close();
            }

        }

    }
}
