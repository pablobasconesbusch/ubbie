﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Servicios.Base;

namespace Ubbie.Servicios.Seguridad
{
    public class ServicioNotificaciones : ServicioBase
    {

        public List<UsuarioNotificacion> GetList(int activeUserId)
        {
            var weekAgo = DateTime.UtcNow.AddDays(-7);

            return DB
                .UsuarioNotificacion
                .Where(x => x.FechaEnvio > weekAgo)
                .ToList();
        }


        public ServiceResponse ReadNotifications(List<int> notificationIds)
        {
            var sr = new ServiceResponse();

            var userNotifications = DB.UsuarioNotificacion.Where(x => notificationIds.Contains(x.Id)).ToList();
            userNotifications.ForEach(x => x.Leido = true);
            DB.SaveChanges();

            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }
    }
}
