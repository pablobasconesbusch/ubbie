﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Seguridad
{
    public class ServicioDigitos : ServicioBase
    {
        public static void RecalcularDigitosVerificadores()
        {
            var tablas = ExecuteQuery("SELECT * FROM DVV");

            // Recorremos todas las tablas
            for (var t = 0; t < tablas.Rows.Count; t++)
            {
                var dvhUpdateQuery = "";
                var dvvCalculado = "";
                var tablaActual = ExecuteQuery($"SELECT * FROM {tablas.Rows[t][0]}");


                // Recorremos todas las filas para esa tabla
                for (var f = 0; f < tablaActual.Rows.Count; f++)
                {
                    var dvhCalculado = "";

                    for (var c = 0; c < tablaActual.Columns.Count; c++)
                    {
                        if (tablaActual.Columns[c].ColumnName != "DVH")
                        {
                            var value = tablaActual.Rows[f][c];
                            if (value is DateTime)
                            {
                                var date = Convert.ToDateTime(value);
                                value = date.ToString("G", new CultureInfo("es-AR"));
                            }
                            if (value is decimal)
                            {
                                var valueD = Convert.ToDecimal(value);
                                value = valueD.ToString("C", new CultureInfo("es-AR"));
                            }
                            dvhCalculado += value.ToString();
                        }
                    }

                    dvhCalculado = UtilidadesSeguridad.CrearHash(dvhCalculado);
                    dvhUpdateQuery += $"UPDATE {tablaActual.TableName} SET DVH = '{dvhCalculado}' WHERE Id = '{tablaActual.Rows[f]["Id"]}';";

                    dvvCalculado += dvhCalculado;
                }
                if (dvhUpdateQuery == "")
                {
                    continue;
                }
                ExecuteSqlCommand(dvhUpdateQuery);

                dvvCalculado = UtilidadesSeguridad.CrearHash(dvvCalculado);
                ExecuteSqlCommand($"UPDATE DVV SET DVV = '{dvvCalculado}' WHERE Tabla = '{tablaActual.TableName}'");

            }
        }

        public static ServiceResponse ComprobarIntegridad(ServiceResponse sr)
        {
            // Obtenemos el nombre de todas las tablas en la base de datos
            var nombreTablas = ExecuteQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG='consorcio' AND TABLE_NAME <> 'sysdiagrams'");
            var todasLasTablas = nombreTablas.AsEnumerable().Select(x => x.ItemArray[0].ToString()).ToList();

            Configuration.Application.Tables.ToList().ForEach(x =>
            {
                if (!todasLasTablas.Contains(x))
                {
                    sr.AddError($"Falta la tabla {x} de la base de datos");
                    sr.ReturnValue = -3;
                }
            });

            if (!sr.Status)
                return sr;


            var tablas = ExecuteQuery("SELECT * FROM DVV");

            // Recorremos todas las tablas
            for (var t = 0; t < tablas.Rows.Count; t++)
            {
                var tablaActual = ExecuteQuery($"SELECT * FROM {tablas.Rows[t][0]}");

                var dvvGuardado = tablas.Rows[t][1].ToString();
                var dvvCalculado = "";

                // Recorremos todas las filas para esa tabla
                for (var f = 0; f < tablaActual.Rows.Count; f++)
                {
                    var dvhGuardado = tablaActual.Rows[f]["DVH"].ToString();
                    var dvhCalculado = "";

                    dvvCalculado += dvhGuardado;

                    for (var c = 0; c < tablaActual.Columns.Count; c++)
                    {
                        if (tablaActual.Columns[c].ColumnName != "DVH")
                        {
                            var value = tablaActual.Rows[f][c];
                            if (value is DateTime)
                            {
                                var date = Convert.ToDateTime(value);
                                value = date.ToString("G", new CultureInfo("es-AR"));
                            }
                            if (value is decimal)
                            {
                                var valueD = Convert.ToDecimal(value);
                                value = valueD.ToString("C", new CultureInfo("es-AR"));
                            }
                            dvhCalculado += value.ToString();
                        }
                    }
                    dvhCalculado = UtilidadesSeguridad.CrearHash(dvhCalculado);

                    // Si para esta fila, el DVH es distinto, significa que esta fila fue la afectada
                    if (dvhGuardado != dvhCalculado)
                        sr.AddError($"Tabla afectada: {tablaActual.TableName}. Fila afectada: {f + 1}");
                }

                dvvCalculado = UtilidadesSeguridad.CrearHash(dvvCalculado);

                // Significa que se eliminaron registros
                if (dvvGuardado != dvvCalculado)
                    sr.AddError($"Tabla afectada: {tablaActual.TableName}. Uno o más registros han sido eliminados");
            }

            if (!sr.Status)
                sr.ReturnValue = -2;

            return sr;
        }
    }
}
