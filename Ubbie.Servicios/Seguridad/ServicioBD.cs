﻿using System;
using Ubbie.Framework.Extensiones;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Seguridad
{
    public class ServicioBD : ServicioBase
    {
        public ServiceResponse Backup(string archivoBackupNombre)
        {
            var sr = new ServiceResponse();
            var query = $"BACKUP DATABASE Consorcio TO DISK = N'{archivoBackupNombre}'";

            try
            {
                ExecuteSqlCommand(query);
                ServicioBitacora.Log($"Se realizó un backup. El archivo se ubica en {archivoBackupNombre}", Constants.BitacoraCriticidad.MEDIA);
                ServicioDigitos.RecalcularDigitosVerificadores();
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                if (ex.InnerException.IsNotNull())
                    sr.AddError(ex.InnerException);
            }

            return sr;
        }

        public ServiceResponse Restore(string nombreArchivo)
        {
            var sr = new ServiceResponse();
            var query = "ALTER DATABASE Consorcio SET SINGLE_USER WITH ROLLBACK IMMEDIATE USE master DROP DATABASE Consorcio " +
                $"RESTORE DATABASE Consorcio FROM DISK = N'{nombreArchivo}' ALTER DATABASE Consorcio SET MULTI_USER";

            try
            {
                ExecuteSqlCommand(query);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                if (ex.InnerException.IsNotNull())
                    sr.AddError(ex.InnerException);
            }

            return sr;
        }
    }
}
