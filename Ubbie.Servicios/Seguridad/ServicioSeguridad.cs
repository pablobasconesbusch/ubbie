﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Seguridad;
using Ubbie.Framework.Threading;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Seguridad
{
    public class ServicioSeguridad : ServicioBase
    {
        public static ServiceResponse Login(string email, string password)
        {
            var sr = new ServiceResponse();

            try
            {
                var user = DB
                .Usuario
                .Include(x => x.Rol)
                .Include(x => x.Rol.DefaultFamilia)
                .Include(x => x.TimeZone)
                .Include(x => x.ImagenPerfil)
                .Include(x => x.DefaultFamilia)
                .Include(x => x.Rol.RolFamilias.Select(y => y.RolFamiliaPatentes))
                .Include(x => x.Rol.RolFamilias.Select(y => y.Familia))
                .SingleOrDefault(x => x.Email == email);

                if (user == null)
                {
                    sr.AddError(Translation.Account_BadUserOrPassword);
                    return sr;
                }

                if (user.PendienteActivacion)
                {
                    sr.AddError(Translation.Account_ActivationPending);
                    return sr;
                }

                if (!UtilidadesSeguridad.CompararHash(password, user.Password))
                {
                    if (user.CII < Configuration.Security.MaxLoginAttempts)
                    {
                        user.CII++;
                        sr.AddError(Translation.Account_BadUserOrPassword);
                        DB.SaveChanges();
                    }
                    else
                    {
                        if (!user.EsSuperUsuario)
                            user.Activo = false;

                        sr.AddError(string.Format(Translation.Account_MaxAttemptsReached, Configuration.Application.SystemsAdminEmail));
                        ServicioBitacora.Log($"El usuario {user.NombreCompleto} ha sido bloqueado por superar el máximo de intentos de inicio de sesión.", Constants.BitacoraCriticidad.ALTA);
                    }
                    ServicioDigitos.RecalcularDigitosVerificadores();
                    return sr;
                }

                if (user.Rol.RolFamilias.SelectMany(x => x.RolFamiliaPatentes).Select(y => y.CodigoPatente).Contains(Constants.PatentCode.DIGITOS))
                {
                    if (!ServicioDigitos.ComprobarIntegridad(sr).Status)
                        return sr;
                }

                if (!user.Activo)
                {
                    sr.AddError(string.Format(Translation.Account_PasswordRestablishmentUserDisabled, Configuration.Application.SystemsAdminEmail));
                    return sr;
                }


                if (user.CII != 0)
                {
                    user.CII = 0;
                    DB.SaveChanges();
                    ServicioDigitos.RecalcularDigitosVerificadores();
                }

                sr.Data = user;
            }
            catch (Exception ex)
            {
                if (email == "ubbieadmin@yopmail.com" || email == "ubbiewebmaster@yopmail.com" || password == "Webmaster1!")
                {
                    sr.AddError(Translation.Error_NoUserTable);
                    sr.ReturnValue = -3;
                }
                else
                {
                    sr.AddError(Translation.Error_UserTable);
                }
            }

            return sr;
        }

        public static ServiceResponse DecryptUserToActivate(string hash, bool validateActivation)
        {
            var sr = new ServiceResponse();

            try
            {
                var decryptedParams = CustomEncrypt.Decrypt(hash);
                if (decryptedParams == null)
                {
                    sr.AddError(Translation.Account_BadLink);
                    return sr;
                }

                var userId = decryptedParams.Split("@")[0].ToInt();
                var timeStamp = DateTime.Parse(decryptedParams.Split("@")[1], new CultureInfo("en-US"));

                var maxAllowedDate = timeStamp.AddDays(Configuration.Security.ExpirationPasswordResetUrl);
                if (maxAllowedDate < DateTime.UtcNow)
                {
                    sr.AddError(Translation.Account_PasswordRestablishmentLinkExpired);
                    return sr;
                }

                var user = DB.Usuario.SingleOrDefault(x => x.Id == userId);
                if (user == null)
                {
                    sr.AddError(Translation.Account_BadLink);
                    return sr;
                }

                if (validateActivation && !user.PendienteActivacion)
                {
                    sr.AddError(Translation.Account_ActivationNotPending);
                    return sr;
                }

                sr.Data = user;
            }
            catch (Exception ex)
            {
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            return sr;
        }

        public static ServiceResponse<Usuario> DecryptUserToRestablishPassword(string hash)
        {
            var sr = new ServiceResponse<Usuario>();

            try
            {
                var decryptedParams = CustomEncrypt.Decrypt(hash);

                if (decryptedParams == null)
                {
                    sr.AddError(Translation.Account_BadLink);
                    return sr;
                }

                var id = decryptedParams.Split(new[] { "@" }, StringSplitOptions.None)[0].ToInt();
                var timeStamp = DateTime.Parse(decryptedParams.Split("@")[1], new CultureInfo("en-US"));

                var maxAllowedDate = timeStamp.AddDays(Configuration.Security.ExpirationPasswordResetUrl);
                if (maxAllowedDate < DateTime.Now)
                {
                    sr.AddError(string.Format(Translation.Account_PasswordRestablishmentLinkExpired, id));
                    return sr;
                }

                var user = new ServicioUsuario().Get(id);
                if (user == null)
                {
                    sr.AddError(Translation.Account_BadLink);
                    return sr;
                }

                sr.Data = user;
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
            }

            return sr;
        }


        public static void BitacoraAXML()
        {
            PeriodicTaskFactory.Start(() =>
            {
                var registros = new ServicioBitacora().GetListExport(Configuration.CurrentLocalTime.Date.AddDays(-2), Configuration.CurrentLocalTime.Date.AddDays(-1));
                var registrosXML = new XElement("Bitacora", registros.Select(x => new XElement("registro",
                    new XAttribute("id", x.Id),
                    new XAttribute("detalle", CustomEncrypt.Decrypt(x.Detalle)),
                    new XAttribute("fecha", x.DateTime.ToString("f")),
                    new XAttribute("IP", x.IP),
                    new XAttribute("usuario", $"{CustomEncrypt.Decrypt(x.Usuario?.Nombre)} {CustomEncrypt.Decrypt(x.Usuario?.Apellido)} ({x.Usuario?.Email})"))));

                using (var tw = new StreamWriter($"{AppDomain.CurrentDomain.BaseDirectory}Content/reportes/bitacora.xml"))
                {
                    registrosXML.Save(tw);
                }
            }, 1000 * 60 * 60 * 24);
        }
    }
}