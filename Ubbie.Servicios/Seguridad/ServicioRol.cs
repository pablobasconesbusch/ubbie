﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Seguridad
{
    public class ServicioRol : ServicioBase, IQueryServiceBase<Rol, string>, ISetupServiceBase<Rol>
    {
        public Rol Get(string key)
        {
            return DB
                .Rol
                .Include(u => u.RolFamilias.Select(x => x.RolFamiliaPatentes))
                .AsNoTracking()
                .SingleOrDefault(r => r.Codigo == key);
        }

        public Familia GetResource(string key)
        {
            return DB
                .Familia
                .AsNoTracking()
                .SingleOrDefault(r => r.Codigo == key);
        }


        public List<Rol> GetList()
        {
            return DB
                .Rol
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<Familia> GetResourceList()
        {
            return DB
                .Familia
                .Include(x => x.FamiliaPatentes.Select(y => y.Familia))
                .Include(x => x.FamiliaPatentes.Select(y => y.Patente))
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            if (!sr.Status)
                return sr;

            var query = DB
                .Rol
                .Where(x => !x.Eliminado && x.Codigo != Constants.RoleCodeSystem.ADMIN)
                .AsNoTracking();

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Codigo.Contains(p.Query) || x.Nombre.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        public ServiceResponse SaveEntity(Rol entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            var dbEntity = DB
                .Rol
                .Include(x => x.RolFamilias.Select(y => y.RolFamiliaPatentes))
                .SingleOrDefault(x => x.Codigo == entity.Codigo);

            if (dbEntity == null)
            {
                DB.Rol.Add(entity);
                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Rol creado {entity.Nombre}", Constants.BitacoraCriticidad.BAJA);
                ServicioDigitos.RecalcularDigitosVerificadores();

                return sr;
            }


            var roleResourceToAdd = entity.RolFamilias.Where(x => !dbEntity.RolFamilias.Select(y => y.CodigoFamilia).Contains(x.CodigoFamilia)).ToList();
            var roleResourceToDelete = dbEntity.RolFamilias.Where(x => !entity.RolFamilias.Select(y => y.CodigoFamilia).Contains(x.CodigoFamilia)).ToList();
            var roleResourcePatentToDelete = roleResourceToDelete.SelectMany(x => x.RolFamiliaPatentes).ToList();

            DB.RolFamilia.AddRange(roleResourceToAdd);
            DB.RolFamilia.RemoveRange(roleResourceToDelete);
            DB.RolFamiliaPatente.RemoveRange(roleResourcePatentToDelete);

            var roleResourceToUpdate = dbEntity.RolFamilias.Where(x => entity.RolFamilias.Select(y => y.CodigoFamilia).Contains(x.CodigoFamilia)).ToList();
            roleResourceToUpdate.ForEach(x =>
            {
                var entityRoleResource = entity.RolFamilias.Single(y => y.CodigoFamilia == x.CodigoFamilia);

                var roleResourcePatentsToAdd = entityRoleResource.RolFamiliaPatentes.Where(y => !x.RolFamiliaPatentes.Select(z => z.CodigoPatente).Contains(y.CodigoPatente)).ToList();
                var roleResourcePatentsToDelete = x.RolFamiliaPatentes.Where(y => !entityRoleResource.RolFamiliaPatentes.Select(z => z.CodigoPatente).Contains(y.CodigoPatente)).ToList();

                DB.RolFamiliaPatente.AddRange(roleResourcePatentsToAdd);
                DB.RolFamiliaPatente.RemoveRange(roleResourcePatentsToDelete);

            });

            dbEntity.Map(entity, x => new { x.Nombre, x.DefaultCodigoFamilia });
            DB.SaveChanges(UsuarioActivo.Id);

            ServicioBitacora.Log($"Rol editado {entity.Nombre}", Constants.BitacoraCriticidad.BAJA);
            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse DeleteEntity(Rol entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            entity = (Rol)sr.Data;
            entity.Eliminado = true;

            ServicioBitacora.Log($"Rol eliminado {entity.Nombre}", Constants.BitacoraCriticidad.MEDIA);
            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Rol entity)
        {
            var sr = new ServiceResponse();

            if (entity.Codigo == Constants.RoleCodeSystem.ADMIN)
            {
                var roleResource = entity.RolFamilias.SingleOrDefault(x => x.CodigoFamilia == Constants.ResourceCode.ROLE);
                if (roleResource == null)
                {
                    sr.AddError("The ADMIN role must have the Role Management module");
                    return sr;
                }
            }

            if (!entity.RolFamilias.Any())
            {
                sr.AddError("Debe seleccionar al menos una patente para esa familia");
                return sr;
            }

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Rol entity)
        {
            var sr = new ServiceResponse();

            entity = DB.Rol.Include(x => x.Usuarios).SingleOrDefault(x => x.Codigo == entity.Codigo);
            if (entity == null)
            {
                sr.AddError("The role is incorrect or does not exist");
                return sr;
            }

            if (entity.Eliminado)
            {
                sr.AddError(Translation.Validation_RoleDeleted);
                return sr;
            }

            if (entity.Usuarios.Any())
            {
                sr.AddError(Translation.Validation_RoleHasUsers);
                return sr;
            }

            if (entity.Codigo == Constants.RoleCodeSystem.ADMIN)
            {
                sr.AddError(Translation.Validation_RoleSystem);
                return sr;
            }


            sr.Data = entity;
            return sr;
        }

    }
}
