﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;
namespace Ubbie.Servicios.Seguridad
{
    public class ServicioUsuario : ServicioBase, IQueryServiceBase<Usuario, int>, ISetupServiceBase<Usuario>
    {
        //public void Encrypt()
        //{
        //    var usuaarios = DB.Usuario.ToList();

        //    usuaarios.ForEach(x =>
        //    {
        //        x.Apellido = CustomEncrypt.Encrypt(x.Apellido);
        //        x.Nombre = CustomEncrypt.Encrypt(x.Nombre);
        //    });

        //    DB.SaveChanges();
        //}

        public Usuario Get(int key)
        {
            return DB
                .Usuario
                .Include(x => x.Rol)
                .Include(x => x.Rol.DefaultFamilia)
                .Include(x => x.TimeZone)
                .Include(x => x.ImagenPerfil)
                .Include(x => x.DefaultFamilia)
                .Include(x => x.Rol.RolFamilias.Select(y => y.RolFamiliaPatentes))
                .Include(x => x.Rol.RolFamilias.Select(y => y.Familia))
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .SingleOrDefault(x => x.Id == key);
        }

        public Usuario Get(string email)
        {
            return DB
                .Usuario
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .SingleOrDefault(x => x.Email == email);
        }

        public List<Usuario> GetUserPatentList()
        {
            return DB
                .Usuario
                .Include(x => x.Rol)
                .Include(x => x.Rol.DefaultFamilia)
                .Include(x => x.TimeZone)
                .Include(x => x.DefaultFamilia)
                .Include(x => x.Rol.RolFamilias.Select(y => y.RolFamiliaPatentes.Select(z => z.Familia.FamiliaPatentes.Select(xx => xx.Patente))))
                .Include(x => x.Rol.RolFamilias.Select(y => y.RolFamiliaPatentes.Select(z => z.Patente)))
                .Include(x => x.Rol.RolFamilias.Select(y => y.Familia))
                .Where(x => !x.Eliminado && !x.EsSuperUsuario)
                .AsNoTracking()
                .ToList();
        }

        public List<Usuario> GetList()
        {
            return DB
                .Usuario
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<Usuario> GetListForExport()
        {
            return DB
                .Usuario
                .Where(x => !x.Eliminado && x.Email != null && x.Nombre != null && x.Apellido != null && x.DNI.HasValue)
                .ToList();
        }

        public List<Usuario> GetList(string query)
        {
            return DB
                .Usuario
                .Where(x => !x.Eliminado && (CustomEncrypt.Decrypt(x.Nombre).StartsWith(query) || CustomEncrypt.Decrypt(x.Apellido).StartsWith(query)))
                .AsNoTracking()
                .ToList();
        }


        public List<Usuario> GetFakeLogins()
        {
            return DB
                .Usuario
                .Include(x => x.Rol)
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<Usuario> GetUsersByRole(string roleCode)
        {
            return DB
                .Usuario
                .Where(x => !x.Eliminado && x.CodigoRol == roleCode)
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Usuario
                .Include(x => x.Rol)
                .AsNoTracking()
                .Where(x => !x.Eliminado && !x.EsSuperUsuario);

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Email.StartsWith(p.Query) || x.Rol.Nombre.StartsWith(p.Query) || x.Apellido.StartsWith(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        public ServiceResponse SaveEntity(Usuario entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            using (var transaction = DB.Database.BeginTransaction())
            {
                try
                {
                    entity.Email = entity.Email.ToLower();

                    if (entity.Id == 0)
                    {
                        entity.Password = string.Empty;
                        entity.TimeZoneCode = "ARGE-ST";
                        entity.PendienteActivacion = true;
                        entity.DVH = "";

                        var dbRole = new ServicioRol().Get(entity.CodigoRol);
                        entity.DefaultCodigoFamilia = dbRole.DefaultCodigoFamilia;

                        DB.Usuario.Add(entity);
                        ServicioBitacora.Log($"Usuario creado {entity.NombreCompleto} ({entity.Email})", Constants.BitacoraCriticidad.BAJA);

                        if (entity.PendienteActivacion)
                            sr = EmailingService.NewUser(entity);

                        if (!sr.Status)
                        {
                            transaction.Rollback();
                            return sr;
                        }
                    }
                    else
                    {
                        var dbEntity = DB.Usuario.Single(x => x.Id == entity.Id);

                        if (dbEntity.CodigoRol != entity.CodigoRol)
                        {
                            var dbRole = new ServicioRol().Get(entity.CodigoRol);
                            entity.DefaultCodigoFamilia = dbRole.DefaultCodigoFamilia;
                        }

                        if (dbEntity.Email != entity.Email && dbEntity.PendienteActivacion)
                        {
                            sr = EmailingService.NewUser(entity);

                            if (!sr.Status)
                            {
                                transaction.Rollback();
                                return sr;
                            }
                        }

                        if (dbEntity.EsSuperUsuario && !entity.Activo && DB.Usuario.Count(x => x.EsSuperUsuario) == 1)
                        {
                            sr.AddError(Translation.Validation_DisableOnlySuperUser);
                            return sr;
                        }


                        dbEntity.Map(entity, x => new
                        {
                            x.Nombre,
                            x.Apellido,
                            x.Email,
                            x.DNI,
                            x.Telefono,
                            x.FechaNacimiento,
                            x.CodigoRol,
                            x.DefaultCodigoFamilia,
                            x.ImagenPerfilId,
                            x.DebeResetearPassword,
                            x.Activo
                        });

                        ServicioBitacora.Log($"Usuario editado: {entity.NombreCompleto} ({entity.Email})", Constants.BitacoraCriticidad.BAJA);

                    }

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    sr.AddError(ex);
                    EventLogService.Error(Constants.EventCode.APP, ex);
                    transaction.Rollback();
                }
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }
        public ServiceResponse DeleteEntity(Usuario entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            entity = (Usuario)sr.Data;
            entity.Activo = false;
            entity.Eliminado = true;

            ServicioBitacora.Log($"User deleted {entity.NombreCompleto} ({entity.Email})", Constants.BitacoraCriticidad.BAJA);
            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Usuario entity)
        {
            var sr = new ServiceResponse();

            entity.Email = entity.Email.ToLower();
            if (DB.Usuario.Any(x => x.Email == entity.Email && x.Id != entity.Id && !x.Eliminado))
            {
                sr.AddError(Translation.Validation_EmailExists);
                return sr;
            }

            if (DB.Usuario.Any(x => x.DNI == entity.DNI && x.Id != entity.Id))
            {
                sr.AddError(Translation.Validation_DNIExists);
                return sr;
            }

            var dbUser = DB.Usuario.SingleOrDefault(x => x.Id == entity.Id);
            if (dbUser != null && dbUser.Eliminado)
            {
                sr.AddError(Translation.Validation_UserDeleted);
                return sr;
            }

            return sr;
        }
        public ServiceResponse ValidateDeleteEntity(Usuario entity)
        {
            var sr = new ServiceResponse();

            entity = DB.Usuario.SingleOrDefault(x => x.Id == entity.Id);

            if (entity == null)
            {
                sr.AddError(Translation.Validation_UserIncorrect);
                return sr;
            }

            if (entity.Eliminado)
            {
                sr.AddError(Translation.Validation_UserDeleted);
                return sr;
            }

            var superUsers = DB.Usuario.Count(x => x.EsSuperUsuario);
            if (entity.EsSuperUsuario && superUsers == 1)
            {
                sr.AddError(Translation.Validation_DeleteOnlySuperUser);
                return sr;
            }

            sr.Data = entity;
            return sr;
        }

        public ServiceResponse SaveProfile(Usuario entity)
        {
            var sr = new ServiceResponse();

            var user = DB.Usuario.Single(x => x.Id == entity.Id);
            user.Map(entity, x => new { x.Nombre, x.Apellido, x.ImagenPerfilId });

            if (!string.IsNullOrEmpty(entity.Password))
            {
                user.Password = UtilidadesSeguridad.CrearHash(entity.Password);
            }

            DB.SaveChanges(UsuarioActivo.Id);
            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }


        public ServiceResponse ActivateUser(Usuario user, string newPassword, bool isActivatingUser)
        {
            var sr = new ServiceResponse();

            if (Configuration.Security.PasswordStrengthPolicy && !TypeValidator.IsValidPassword(newPassword))
            {
                sr.AddError(Translation.Account_PasswordStrengthPolicyText);
                return sr;
            }

            user.Password = UtilidadesSeguridad.CrearHash(newPassword);

            if (isActivatingUser)
            {
                user.PendienteActivacion = false;
                user.Activo = true;
                user.CII = 0;
            }

            if (user.CII != 0)
            {
                user.CII = 0;
                user.Activo = true;
            }

            DB.SaveChanges(user.Id);
            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ResendActivationLink(Usuario entity)
        {
            var sr = new ServiceResponse();

            var user = new ServicioUsuario().Get(entity.Id);
            if (user.IsNull() || !user.PendienteActivacion)
            {
                sr.AddError(Translation.Account_ActivationNotPending);
                return sr;
            }

            return EmailingService.NewUser(user);
        }

        public ServiceResponse RestablishPassword(string email)
        {
            var sr = new ServiceResponse();

            if (string.IsNullOrEmpty(email))
            {
                sr.AddError(Translation.Validation_FieldEmpty);
                return sr;
            }

            var user = Get(email);
            if (user == null)
            {
                sr.AddError(Translation.Account_PasswordRestablishmentUserNotFound);
                return sr;
            }

            if (!user.Activo && user.CII < 3)
            {
                sr.AddError(Translation.Account_PasswordRestablishmentUserDisabled);
                return sr;
            }

            return EmailingService.RestablishPassword(user);
        }

        public List<Familia> GetAvailableResources(int userId)
        {
            return DB
                .Usuario
                .Where(x => x.Id == userId)
                .SelectMany(x => x.Rol.RolFamilias.Select(y => y.Familia))
                .ToList();
        }
    }
}