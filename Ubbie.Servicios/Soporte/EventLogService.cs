﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.DAL;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Extensiones;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Soporte
{
    #region Enums

    #endregion

    public class EventLogService : ServicioBase
    {

        public EventLog Get(int entityId)
        {
            return DB
                .EventLog
                .SingleOrDefault(x => x.Id == entityId);
        }

        public ServiceResponse GetList(EventLogParameters p)
        {
            var sr = new ServiceResponse();

            p.FilterByDateFrom = p.FilterByDateFrom.AddDays(-1);
            p.FilterByDateTo = p.FilterByDateTo.AddDays(1);

            var query = DB
                .EventLog
                .Include(x => x.User)
                .Where(x => x.Date >= p.FilterByDateFrom.Date && x.Date <= p.FilterByDateTo.Date)
                .AsNoTracking()
                .AsQueryable();

            if (!string.IsNullOrEmpty(p.Query))
                query = query.Where(x => x.Path.StartsWith(p.Query) || x.EventCode.StartsWith(p.Query) || x.Data.StartsWith(p.Query));

            if (!string.IsNullOrEmpty(p.FilterByLogLevel))
                query = query.Where(x => x.LogLevel == p.FilterByLogLevel);

            if (p.FilterByUserIds != null && p.FilterByUserIds.Any())
                query = query.Where(x => p.FilterByUserIds.Contains(x.UserId.Value));


            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }


        #region Log Error

        public static int Error(string eventCode, Exception ex)
        {
            return Error(eventCode, ex.ToString());
        }

        public static int Error(string eventCode, string description)
        {
            return Log(Constants.LogLevelCode.ERROR, eventCode, description);
        }

        public static int Error(EventLog eventlog)
        {
            eventlog.LogLevel = Constants.LogLevelCode.ERROR;
            return Log(eventlog);
        }

        #endregion

        #region Log Warn
        public static int Warn(string eventCode, Exception ex)
        {
            return Warn(eventCode, ex.ToString());
        }

        public static int Warn(string eventCode, string description)
        {
            return Log(Constants.LogLevelCode.WARN, eventCode, description);
        }

        public static int Warn(EventLog eventlog)
        {
            eventlog.LogLevel = Constants.LogLevelCode.WARN;
            return Log(eventlog);
        }
        #endregion

        #region Log Info
        public static int Info(string eventCode, Exception ex)
        {
            return Info(eventCode, ex.ToString());
        }

        public static int Info(string eventCode, string description)
        {
            return Log(Constants.LogLevelCode.INFO, eventCode, description);
        }

        public static int Info(EventLog eventlog)
        {
            eventlog.LogLevel = Constants.LogLevelCode.INFO;
            return Log(eventlog);
        }
        #endregion

        #region Log Debug
        public static int Debug(string eventCode, Exception ex)
        {
            return Debug(eventCode, ex.ToString());
        }

        public static int Debug(string eventCode, string description)
        {
            return Log(Constants.LogLevelCode.DEBUG, eventCode, description);
        }

        public static int Debug(EventLog eventlog)
        {
            eventlog.LogLevel = Constants.LogLevelCode.DEBUG;
            return Log(eventlog);
        }
        #endregion

        #region Log
        public static int Log(string logLevelCode, string eventCode, string description)
        {
            var eventLog = new EventLog();
            eventLog.LogLevel = logLevelCode;
            eventLog.EventCode = eventCode;
            eventLog.ServerException = description;

            return Log(eventLog);
        }


        private static int Log(EventLog eventLog)
        {
            if (!Configuration.Support.EnableLogging)
                return 0;

            var systemLogLevel = Configuration.Support.LogLevel;

            if (systemLogLevel == Constants.LogLevelCode.ERROR && eventLog.LogLevel != Constants.LogLevelCode.ERROR)
                return 0;

            if (systemLogLevel == Constants.LogLevelCode.WARN && (eventLog.LogLevel != Constants.LogLevelCode.ERROR || eventLog.LogLevel != Constants.LogLevelCode.WARN))
                return 0;

            if (systemLogLevel == Constants.LogLevelCode.INFO && eventLog.LogLevel == Constants.LogLevelCode.DEBUG)
                return 0;

            eventLog.Date = DateTime.UtcNow;
            eventLog.UserId = UsuarioActivo?.Id;

            var db = new DBEF();
            db.EventLog.Add(eventLog);
            db.SaveChanges();

            return eventLog.Id;
        }

        #endregion

    }


    public class EventLogParameters : PagerParameters
    {
        public DateTime FilterByDateFrom { get; set; }
        public DateTime FilterByDateTo { get; set; }
        public string FilterByLogLevel { get; set; }
        public List<int> FilterByUserIds { get; set; }

    }
}
