﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Soporte;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;

namespace Ubbie.Servicios.Soporte
{
    public class FileService : ServicioBase, IQueryServiceBase<File, int>, ISetupServiceBase<File>
    {
        public File Get(int key)
        {
            return DB
                .File
                .Include(x => x.FileContent)
                .AsNoTracking()
                .Single(x => x.Id == key);
        }

        public File GetWithoutData(int key)
        {
            return DB
                .File
                .AsNoTracking()
                .Single(x => x.Id == key);
        }

        public List<File> GetList()
        {
            return DB
                .File
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            throw new NotImplementedException();
        }

        public ServiceResponse SaveEntity(File entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            DB.File.Add(entity);
            DB.SaveChanges(UsuarioActivo.Id);

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse DeleteEntity(File entity)
        {
            var sr = new ServiceResponse();

            DB.File.Remove(entity);
            DB.SaveChanges(UsuarioActivo.Id);

            return sr;

        }

        public ServiceResponse DeleteEntity(int id)
        {
            var file = DB.File.Single(x => x.Id == id);
            return DeleteEntity(file);
        }

        public ServiceResponse ValidateSetupList(PagerParameters p)
        {
            throw new NotImplementedException();
        }

        public ServiceResponse ValidateSaveEntity(File entity)
        {
            var sr = new ServiceResponse();
            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(File entity)
        {
            throw new NotImplementedException();
        }
    }
}