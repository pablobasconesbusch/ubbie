﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioNoticia : ServicioBase, IQueryServiceBase<Noticia, int>, ISetupServiceBase<Noticia>
    {
        #region Query

        public Noticia Get(int key)
        {
            return DB
                .Noticia
                .Include(x => x.NoticiaCategoria)
                .AsNoTracking()
                .SingleOrDefault(x => x.Id == key);
        }

        public List<Noticia> GetList()
        {
            return DB
                .Noticia
                .Include(x => x.NoticiaCategoria)
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<NoticiaCategoria> GetCategorias()
        {
            return DB
                .NoticiaCategoria
                .AsNoTracking()
                .ToList();
        }
        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Noticia
                .Include(x => x.NoticiaCategoria)
                .Where(x => !x.Eliminado)
                .AsNoTracking();

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Titulo.Contains(p.Query) || x.NoticiaCategoria.Nombre.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        #endregion

        #region Setup

        public ServiceResponse SaveEntity(Noticia entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                if (entity.Id == 0)
                {
                    entity.DVH = "";
                    DB.Noticia.Add(entity);
                    DB.SaveChanges(UsuarioActivo.Id);
                    ServicioBitacora.Log($"Noticia creada: {entity.Titulo}", Constants.BitacoraCriticidad.BAJA);
                }
                else
                {
                    var dbEntity = DB.Noticia.Single(x => x.Id == entity.Id);

                    dbEntity.Map(entity, x => new
                    {
                        x.Cuerpo,
                        x.Fecha,
                        x.ImagenId,
                        x.ImagenHDId,
                        x.NoticiaCategoriaId,
                        x.Titulo
                    });

                    ServicioBitacora.Log($"Noticia editada: {dbEntity.Titulo}", Constants.BitacoraCriticidad.BAJA);
                }
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Noticia entity)
        {
            var sr = new ServiceResponse();

            return sr;
        }

        public ServiceResponse DeleteEntity(Noticia entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                var dbEntity = DB.Noticia.Single(x => x.Id == entity.Id);
                dbEntity.Eliminado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Noticia eliminada: {dbEntity.Titulo}", Constants.BitacoraCriticidad.BAJA);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Noticia entity)
        {
            var sr = new ServiceResponse();

            return sr;
        }

        #endregion
    }
}