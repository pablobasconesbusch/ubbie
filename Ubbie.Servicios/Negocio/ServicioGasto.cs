﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioGasto : ServicioBase, IQueryServiceBase<Gasto, int>, ISetupServiceBase<Gasto>
    {
        #region Query

        public Gasto Get(int key)
        {
            return DB
                .Gasto
                .Include(x => x.TipoGasto)
                .Include(x => x.Consorcio)
                .AsNoTracking()
                .SingleOrDefault(x => x.Id == key);
        }

        public List<Gasto> GetList()
        {
            return DB
                .Gasto
                .Include(x => x.TipoGasto)
                .Include(x => x.Consorcio)
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<TipoGasto> GetTipoGastos()
        {
            return DB
                .TipoGasto
                .AsNoTracking()
                .ToList();
        }

        public List<Gasto> GastosMensuales(DateTime fechaDesde, DateTime fechaHasta)
        {
            return DB
                .Gasto
                .Where(x => x.Fecha >= fechaDesde && x.Fecha < fechaHasta)
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Gasto
                .Include(x => x.TipoGasto)
                .Include(x => x.Consorcio)
                .Where(x => !x.Eliminado)
                .AsNoTracking();

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Descripcion.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        #endregion

        #region Setup

        public ServiceResponse SaveEntity(Gasto entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                if (entity.Id == 0)
                {
                    entity.DVH = "";
                    DB.Gasto.Add(entity);
                    DB.SaveChanges(UsuarioActivo.Id);
                    ServicioBitacora.Log($"Gasto creado: {entity.Descripcion}", Constants.BitacoraCriticidad.BAJA);
                }
                else
                {
                    var dbEntity = DB.Gasto.Single(x => x.Id == entity.Id);

                    dbEntity.Map(entity, x => new
                    {
                        x.Descripcion,
                        x.Fecha,
                        x.Monto,
                        x.NroComprobante,
                        x.ConsorcioId,
                        x.TipoGastoId
                    });

                    ServicioBitacora.Log($"Gasto editado: {dbEntity.Descripcion}", Constants.BitacoraCriticidad.BAJA);
                }
            }
            catch (Exception ex)
            {
                sr.AddError(ex.InnerException.InnerException);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Gasto entity)
        {
            var sr = new ServiceResponse();

            if (entity.Monto <= 0)
            {
                sr.AddError(Translation.Validation_AboveZero);
            }

            return sr;
        }

        public ServiceResponse DeleteEntity(Gasto entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                var dbEntity = DB.Gasto.Single(x => x.Id == entity.Id);
                dbEntity.Eliminado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Gasto eliminado: {dbEntity.Descripcion}", Constants.BitacoraCriticidad.BAJA);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Gasto entity)
        {
            var sr = new ServiceResponse();

            var dbGasto = DB.Gasto.SingleOrDefault(x => x.Id == entity.Id);
            if (dbGasto != null && dbGasto.Eliminado)
            {
                sr.AddError(Translation.Validation_EntityDeleted);
                return sr;
            }

            return sr;
        }

        #endregion
    }
}