﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioUnidad : ServicioBase, IQueryServiceBase<Unidad, int>, ISetupServiceBase<Unidad>
    {
        #region Query

        public Unidad Get(int key)
        {
            return DB
                .Unidad
                .Include(x => x.TipoUnidad)
                .Include(x => x.Consorcio)
                .Include(x => x.Propietario)
                .AsNoTracking()
                .SingleOrDefault(x => x.Id == key);
        }

        public List<Unidad> GetList()
        {
            return DB
                .Unidad
                .Include(x => x.TipoUnidad)
                .Include(x => x.Consorcio)
                .Include(x => x.Propietario)
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public List<TipoUnidad> GetTipoUnidades()
        {
            return DB
                .TipoUnidad
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Unidad
                .Include(x => x.TipoUnidad)
                .Include(x => x.Consorcio)
                .Include(x => x.Propietario)
                .Where(x => !x.Eliminado)
                .AsNoTracking();

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Nombre.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        #endregion

        #region Setup

        public ServiceResponse SaveEntity(Unidad entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                if (entity.Id == 0)
                {
                    entity.DVH = "";
                    DB.Unidad.Add(entity);
                    DB.SaveChanges(UsuarioActivo.Id);
                    ServicioBitacora.Log($"Unidad creada: {entity.Nombre}", Constants.BitacoraCriticidad.BAJA);
                }
                else
                {
                    var dbEntity = DB.Unidad.Single(x => x.Id == entity.Id);

                    dbEntity.Map(entity, x => new
                    {
                        x.Nombre,
                        x.Ambientes,
                        x.ConsorcioId,
                        x.PropietarioId,
                        x.TipoUnidadId,
                        x.Metros
                    });

                    ServicioBitacora.Log($"Unidad editada: {dbEntity.Nombre}", Constants.BitacoraCriticidad.BAJA);
                }
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Unidad entity)
        {
            var sr = new ServiceResponse();

            if (entity.Metros <= 0 || entity.Ambientes <= 0)
            {
                sr.AddError(Translation.Validation_AboveZero);
            }

            return sr;
        }

        public ServiceResponse DeleteEntity(Unidad entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                var dbEntity = DB.Unidad.Single(x => x.Id == entity.Id);
                dbEntity.Eliminado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Unidad eliminada: {dbEntity.Nombre}", Constants.BitacoraCriticidad.BAJA);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Unidad entity)
        {
            var sr = new ServiceResponse();

            var dbUnidad = DB.Unidad.SingleOrDefault(x => x.Id == entity.Id);
            if (dbUnidad != null && dbUnidad.Eliminado)
            {
                sr.AddError(Translation.Validation_EntityDeleted);
                return sr;
            }

            return sr;
        }

        #endregion
    }
}