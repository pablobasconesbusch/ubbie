﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioConsorcio : ServicioBase, IQueryServiceBase<Consorcio, int>, ISetupServiceBase<Consorcio>
    {
        #region Query

        public Consorcio Get(int key)
        {
            return DB
                .Consorcio
                .AsNoTracking()
                .SingleOrDefault(x => x.Id == key);
        }

        public List<Consorcio> GetList()
        {
            return DB
                .Consorcio
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Consorcio
                .Where(x => !x.Eliminado)
                .AsNoTracking();

            if (!p.Query.IsNullOrEmpty())
                query = query.Where(x => x.Nombre.Contains(p.Query) || x.CUIT.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        #endregion

        #region Setup

        public ServiceResponse SaveEntity(Consorcio entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                if (entity.Id == 0)
                {
                    entity.DVH = "";
                    DB.Consorcio.Add(entity);
                    DB.SaveChanges(UsuarioActivo.Id);
                    ServicioBitacora.Log($"Consorcio creado: {entity.Nombre}", Constants.BitacoraCriticidad.BAJA);
                }
                else
                {
                    var dbEntity = DB.Consorcio.Single(x => x.Id == entity.Id);

                    dbEntity.Map(entity, x => new
                    {
                        x.Nombre,
                        x.Direccion,
                        x.Numero,
                        x.CUIT,
                        x.CodigoPostal
                    });

                    ServicioBitacora.Log($"Consorcio editado: {dbEntity.Nombre}", Constants.BitacoraCriticidad.BAJA);
                }
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Consorcio entity)
        {
            var sr = new ServiceResponse();

            if (DB.Consorcio.Any(x => x.CUIT == entity.CUIT && x.Id != entity.Id))
                sr.AddError(Translation.Validation_SameCUIT);

            return sr;
        }

        public ServiceResponse DeleteEntity(Consorcio entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                var dbEntity = DB.Consorcio.Single(x => x.Id == entity.Id);
                dbEntity.Eliminado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Consorcio eliminado: {dbEntity.Nombre}", Constants.BitacoraCriticidad.BAJA);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Consorcio entity)
        {
            var sr = new ServiceResponse();

            var dbConsorcio = DB.Consorcio.SingleOrDefault(x => x.Id == entity.Id);
            if (dbConsorcio != null && dbConsorcio.Eliminado)
            {
                sr.AddError(Translation.Validation_EntityDeleted);
                return sr;
            }

            return sr;
        }

        #endregion
    }
}