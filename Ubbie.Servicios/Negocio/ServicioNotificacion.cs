﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioNotificacion : ServicioBase
    {
        public UsuarioNotificacion Get(int key)
        {
            return DB.UsuarioNotificacion.Include(x => x.Usuario).FirstOrDefault(x => x.Id == key);
        }

        public List<UsuarioNotificacion> GetListByUser(bool read)
        {
            if (UsuarioActivo == null)
                return new List<UsuarioNotificacion>();

            var query = DB
                .UsuarioNotificacion
                .Include(x => x.Usuario)
                .Where(x => x.UsuarioId == UsuarioActivo.Id && x.Leido == read)
                .OrderByDescending(x => x.Id)
                .ToList();

            var UsuarioNotificacionList = query.Select(x => new UsuarioNotificacion
            {
                Id = x.Id,
                Titulo = x.Titulo,
                FechaEnvio = x.FechaEnvio
            }).ToList();

            return UsuarioNotificacionList;
        }

        public ServiceResponse SaveEntity(UsuarioNotificacion entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                if (entity.Id == 0)
                {
                    DB.UsuarioNotificacion.Add(entity);
                    DB.SaveChanges(UsuarioActivo.Id);
                }

            }
            catch (Exception ex)
            {
                EventLogService.Error(Constants.EventCode.APP, ex);
                sr.AddError(ex);

                if (ex.InnerException.IsNotNull())
                    sr.AddError(ex.InnerException.InnerException);
            }

            ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(UsuarioNotificacion entity)
        {
            var sr = new ServiceResponse();

            return sr;
        }


        public ServiceResponse ViewNotificacion(int UsuarioNotificacionId)
        {
            var sr = new ServiceResponse();

            var dbEntity = DB.UsuarioNotificacion.Single(x => x.Id == UsuarioNotificacionId);
            dbEntity.Leido = true;

            DB.SaveChanges(UsuarioActivo.Id);

            ServicioDigitos.RecalcularDigitosVerificadores();


            return sr;
        }

        public ServiceResponse ViewAllNotificaciones(int userId)
        {
            var sr = new ServiceResponse();

            var dbUsuarioNotificacions = DB.UsuarioNotificacion.Where(x => x.UsuarioId == userId && !x.Leido).ToList();
            dbUsuarioNotificacions.ForEach(x =>
            {
                x.Leido = true;
            });

            DB.SaveChanges(UsuarioActivo.Id);

            ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }
    }
}
