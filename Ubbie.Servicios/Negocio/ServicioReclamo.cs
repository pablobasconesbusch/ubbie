﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Inheritable;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioReclamo : ServicioBase, IQueryServiceBase<Reclamo, int>, ISetupServiceBase<Reclamo>
    {
        public Reclamo Get(int key)
        {
            return DB
                .Reclamo
                .Include(x => x.Usuario)
                .SingleOrDefault(x => x.Id == key);
        }


        public List<Reclamo> GetListForExport()
        {
            return DB
                .Reclamo
                .Include(x => x.Usuario)
                .Where(x => !x.Eliminado && x.Solucionado)
                .AsNoTracking()
                .ToList();
        }

        public List<Reclamo> GetList()
        {
            return DB
                .Reclamo
                .Include(x => x.Usuario)
                .Where(x => !x.Eliminado)
                .AsNoTracking()
                .ToList();
        }

        public string ObtenerNroReclamo()
        {
            var now = Configuration.CurrentLocalTime;
            return $"{now.Month}{now.Day}{now.Hour}{now.Minute}{now.Second}{now.Millisecond}";
        }

        public ServiceResponse GetSetupList(PagerParameters p)
        {
            var sr = new ServiceResponse();

            var query = DB
                .Reclamo
                .Include(x => x.Usuario)
                .Where(x => !x.Eliminado)
                .AsNoTracking();

            // Filter query
            if (!string.IsNullOrEmpty(p.Query))
                query = query.Where(x => CustomEncrypt.Decrypt(x.Usuario.Nombre).Contains(p.Query) || CustomEncrypt.Decrypt(x.Usuario.Apellido).Contains(p.Query) || x.Numero.Contains(p.Query));

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        public ServiceResponse SaveEntity(Reclamo entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            using (var transaction = DB.Database.BeginTransaction())
            {
                try
                {
                    if (entity.Id == 0)
                    {
                        entity.DVH = "";
                        entity.Pendiente = true;
                        DB.Reclamo.Add(entity);
                        DB.SaveChanges(UsuarioActivo.Id);
                        ServicioBitacora.Log($"Se creó el reclamo {entity.Numero}", Constants.BitacoraCriticidad.ALTA);

                        var usuariosANotificar = DB
                            .Usuario
                            .Where(x => x.CodigoRol == Constants.RoleCode.CONSORCIOADMIN)
                            .ToList();

                        sr.Data = usuariosANotificar;
                        sr.ReturnCode = entity.Comentario;
                    }
                    else
                    {
                        var dbEntity = DB.Reclamo.SingleOrDefault(x => x.Id == entity.Id);

                        if (dbEntity.IsNull())
                        {
                            sr.AddError("The entity does not exist");
                            return sr;
                        }

                        dbEntity.Map(entity, x => new
                        {
                            x.Comentario
                        });

                        DB.SaveChanges(UsuarioActivo.Id);
                        ServicioBitacora.Log($"Se editó el reclamo {entity.Numero}", Constants.BitacoraCriticidad.ALTA);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    EventLogService.Error(Constants.EventCode.APP, ex);
                    sr.AddError(ex);

                    if (ex.InnerException.IsNotNull())
                        sr.AddError(ex.InnerException.InnerException);

                    transaction.Rollback();
                    return sr;
                }
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse DeleteEntity(Reclamo entity)
        {
            var sr = ValidateDeleteEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                var dbEntity = DB.Reclamo.Single(x => x.Id == entity.Id);
                dbEntity.Eliminado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Se eliminó el reclamo {entity.Id}", Constants.BitacoraCriticidad.MEDIA);
            }
            catch (Exception ex)
            {
                EventLogService.Error(Constants.EventCode.APP, ex);
                sr.AddError(ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;
            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Reclamo entity)
        {
            var sr = new ServiceResponse();

            if (string.IsNullOrEmpty(entity.Comentario))
                sr.AddError(Translation.Validation_CommentsEmpty);

            return sr;
        }

        public ServiceResponse ValidateDeleteEntity(Reclamo entity)
        {
            var sr = new ServiceResponse();

            return sr;
        }

        public ServiceResponse FinalizarReclamo(Reclamo entity)
        {
            var sr = new ServiceResponse();

            try
            {
                var dbEntity = DB.Reclamo.Single(x => x.Id == entity.Id);
                dbEntity.Pendiente = false;

                dbEntity.Map(entity, x => new
                {
                    x.Solucion,
                    x.Solucionado
                });

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Se finalizó el reclamo {entity.Id}", Constants.BitacoraCriticidad.MEDIA);

                var usuariosANotificar = DB
                            .Usuario
                            .Single(x => x.Id == dbEntity.UsuarioId);

                sr.Data = usuariosANotificar;
                sr.ReturnCode = dbEntity.Solucion;
            }
            catch (Exception ex)
            {
                EventLogService.Error(Constants.EventCode.APP, ex);
                sr.AddError(ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }
    }
}
