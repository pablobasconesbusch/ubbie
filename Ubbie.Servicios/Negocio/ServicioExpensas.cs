﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Negocio;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Negocio
{
    public class ServicioExpensas : ServicioBase
    {
        public List<PagoExpensa> GetExpensasSinPagar(int usuarioId)
        {
            return DB
                .PagoExpensa
                .Include(x => x.Expensa)
                .Where(x => x.PropietarioId == usuarioId)
                .OrderByDescending(x => x.Id)
                .AsNoTracking()
                .ToList();
        }

        public List<PagoExpensa> GetExpensasPagas(int usuarioId)
        {
            return DB
                .PagoExpensa
                .Include(x => x.Expensa)
                .Where(x => x.PropietarioId == usuarioId && x.Pagado)
                .OrderByDescending(x => x.Id)
                .AsNoTracking()
                .ToList();
        }

        public ServiceResponse EfectuarPago(int pagoExpensaId)
        {
            var sr = new ServiceResponse();

            try
            {
                var dbPagoExpensa = DB.PagoExpensa.Include(x => x.Expensa).Single(x => x.Id == pagoExpensaId);

                dbPagoExpensa.Pagado = true;

                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Se pagó la expensa del mes {dbPagoExpensa.Expensa.Mes}, año {dbPagoExpensa.Expensa.Año}, del usuario {dbPagoExpensa.PropietarioId}", Constants.BitacoraCriticidad.MEDIA);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                sr.AddError(ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse GuardarExpensa(Expensa entity)
        {
            var sr = ValidateSaveEntity(entity);

            if (!sr.Status)
                return sr;

            try
            {
                entity.DVH = "";
                DB.Expensa.Add(entity);
                DB.SaveChanges(UsuarioActivo.Id);
                ServicioBitacora.Log($"Se liquidó la expensa para el año {entity.Año}, mes {entity.Mes}", Constants.BitacoraCriticidad.ALTA);
            }
            catch (Exception ex)
            {
                sr.AddError(ex.InnerException.InnerException);
                EventLogService.Error(Constants.EventCode.APP, ex);
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            sr.ReturnValue = entity.Id;

            return sr;
        }


        public ServiceResponse LiquidarExpensas(DateTime day, int expensaId)
        {
            var sr = new ServiceResponse();

            using (var transaction = DB.Database.BeginTransaction())
            {
                try
                {
                    var primerDiaDelMes = new DateTime(day.Year, day.Month, 1);
                    var ultimoDiaDelMes = primerDiaDelMes.AddMonths(1);

                    var gastosMensuales = new ServicioGasto().GastosMensuales(primerDiaDelMes, ultimoDiaDelMes);
                    var unidadesFuncionales = new ServicioUnidad().GetList();

                    var totalM2 = (decimal)unidadesFuncionales.Sum(x => x.Metros);

                    var costoPorM2 = gastosMensuales.Sum(x => x.Monto) / totalM2;

                    // ForEach por unidad funcional, voy creando el PagoExpensa
                    unidadesFuncionales.ForEach(x =>
                    {
                        var pagoExpensa = new PagoExpensa
                        {
                            ExpensaId = expensaId,
                            Monto = costoPorM2 * (decimal)x.Metros,
                            UnidadId = x.Id,
                            PropietarioId = x.PropietarioId,
                            DVH = ""
                        };
                        DB.PagoExpensa.Add(pagoExpensa);
                        DB.SaveChanges(UsuarioActivo.Id);
                    });

                    sr.Data = gastosMensuales;

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    sr.AddError(ex.InnerException.InnerException);
                    EventLogService.Error(Constants.EventCode.APP, ex);
                    transaction.Rollback();
                }
            }

            if (sr.Status)
                ServicioDigitos.RecalcularDigitosVerificadores();

            return sr;
        }

        public ServiceResponse ValidateSaveEntity(Expensa entity)
        {
            var sr = new ServiceResponse();

            if (DB.Expensa.Any(x => x.Año == entity.Año && x.Mes == entity.Mes && x.Id != entity.Id))
                sr.AddError(Translation.Validation_ExpenseSameMonth);

            return sr;
        }
    }
}
