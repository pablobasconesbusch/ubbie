﻿using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Ubbie.DAL;


namespace Ubbie.Servicios.Models
{
    public static class Configuration
    {
        public static DateTime CurrentLocalTime
        {
            get
            {
                var timezone = TimeZoneInfo.FindSystemTimeZoneById("Argentina Standard Time");
                return TimeZoneInfo.ConvertTime(DateTime.Now, timezone);
            }
        }

        public static DatabaseConfig Database { get; set; } = new DatabaseConfig();

        public static ApplicationConfig Application { get; set; } = new ApplicationConfig();
        public static SupportConfig Support { get; set; } = new SupportConfig();
        public static SecurityConfig Security { get; set; } = new SecurityConfig();
        public static EmailingConfig Emailing { get; set; } = new EmailingConfig();
    }

    public class DatabaseConfig
    {
        public string ConnectionString
        {
            get { return DBEF.DatabaseConnectionString; }
            set { DBEF.DatabaseConnectionString = value; }
        }
    }


    public class ApplicationConfig
    {
        public string SiteUrl { get; set; }
        public string SiteLogoUrl { get; set; }
        public string SiteRedirectUrl { get; set; }
        public string SiteViewFileUrl { get; set; }
        public string EnvironmentCode { get; set; }
        public string AppLayout { get; set; }
        public string SystemsAdminEmail { get; set; }
        public string EmailFooter { get; set; }
        public string ApplicationName { get; set; }

        public CloudStorageAccount AzureStorageAccount { get; set; }
        public CloudBlobClient AzureCloudBlobClient { get; set; }
        public CloudBlobContainer AzureUploadsContainer { get; set; }

        public string AzureUploadsContainerDirectory { get; set; }
        public string AzureUploadsContainerUri { get; set; }
        public string AzureStorageConnectionString { get; set; }

        public string UploadsFolder { get; set; }
        public string MissingImagePath { get; set; }

        public string[] Tables => new[]
          {
                "ConfigGroup",
                "ConfigSetting",
                "Email",
                "EmailAttachment",
                "EmailTemplate",
                "File",
                "FileContent",
                "DVV",
                "Usuario",
                "Rol",
                "Familia",
                "Patente",
                "FamiliaPatente",
                "RolFamiliaPatente",
                "Bitacora"
            };
    }

    public class SupportConfig
    {
        public bool EnableLogging { get; set; }
        public string DaemonEmailList { get; set; }
        public string LogLevel { get; set; }
        public string FileSaveMode { get; set; }
    }

    public class SecurityConfig
    {
        public int ExpirationUserActivationUrl { get; set; }
        public int ExpirationPasswordResetUrl { get; set; }
        public string UserActivationUrl { get; set; }
        public string PasswordRestablishmentUrl { get; set; }
        public string PasswordResetUrl { get; set; }
        public string LoginUrl { get; set; }
        public int MaxLoginAttempts { get; set; }
        public bool PasswordStrengthPolicy { get; set; }
        public int CookieExpirationTime { get; set; }
        public bool EnableRoleManagement { get; set; }
    }

    public class EmailingConfig
    {
        public bool EmailingEnabled { get; set; }
        public string FromAddress { get; set; }
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
        public bool SmtpUseDefaultCredentials { get; set; }
        public bool SmtpEnableSsl { get; set; }
        public string EmailDisplayName { get; set; }
    }
}
