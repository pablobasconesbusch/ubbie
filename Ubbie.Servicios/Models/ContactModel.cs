﻿namespace Ubbie.Servicios.Models
{
    public class ContactModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Reason { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public bool ConfirmTerms { get; set; }

        public string ToAddress { get; set; }
        public string CompanyName { get; set; }

        // From Contact when user is not logged
        public string CountryName { get; set; }
        public string HowYouMetUs { get; set; }
    }
}
