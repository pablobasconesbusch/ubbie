﻿using System.Collections.Generic;

namespace Ubbie.Servicios.Models
{
    public class Constants
    {

        public class EnvironmentCode
        {
            public const string DEV = "DEV";
            public const string STG = "STG";
            public const string PRD = "PRD";

            public static readonly Dictionary<string, string> VALUES = new Dictionary<string, string>
            {
                { DEV, DEV },
                { STG, STG },
                { PRD, PRD }
            };
        }

        public class LogLevelCode
        {
            public const string ERROR = "ERROR";
            public const string WARN = "WARN";
            public const string INFO = "INFO";
            public const string DEBUG = "DEBUG";

            public static readonly Dictionary<string, string> VALUES = new Dictionary<string, string>
            {
                { ERROR, ERROR },
                { WARN, WARN },
                { INFO, INFO },
                { DEBUG, DEBUG },
            };
        }

        public class LogEventCode
        {
            public const string APP = "APP";
            public const string TASK = "TASK";
            public const string EMAILING = "EMAILING";
            public const string INFO = "INFO";
        }

        public class EmailTemplateCode
        {
            public const string NewUser = "NewUser";
            public const string RestablishPassword = "RestablishPassword";
            public const string ContactEmail = "ContactEmail";
        }

        public class EmailStatusCode
        {
            public const string QUEUED = "QUEUED";
            public const string SENT = "SENT";
            public const string ERROR = "ERROR";

            public static readonly Dictionary<string, string> VALUES = new Dictionary<string, string>
            {
                { ERROR, ERROR },
                { QUEUED, QUEUED },
                { SENT, SENT }
            };
        }

        public class SelectListOptionCode
        {
            public const string SELECT = "Seleccione una opción";
            public const string SELECT_MULTI = "Seleccione múltiples opciones";
        }

        public class AppLayoutCode
        {
            public const string SIDEBAR = "SIDEBAR";
            public const string HORIZONTAL = "HORIZONTAL";

            public static readonly Dictionary<string, string> VALUES = new Dictionary<string, string>
            {
                { SIDEBAR, SIDEBAR },
                { HORIZONTAL, HORIZONTAL },
            };
        }

        public class EventCode
        {
            public const string APP = "APP";
            public const string JOB = "JOB";
            public const string EMAIL = "EMAIL";
        }

        public class RoleCode
        {
            public const string ADMIN = "ADMIN";
            public const string CONSORCIOADMIN = "CONSORCIOADMIN";
            public const string INQUILINO = "INQUILINO";
        }

        public class RoleCodeSystem
        {
            public const string ADMIN = "ADMIN";
        }

        public class ResourceCode
        {
            public const string DASHBOARD = "DASHBOARD";
            public const string USER = "USER";
            public const string ROLE = "ROLE";
            public const string BITACORA = "BITACORA";
            public const string BITACORA_HISTORICO = "BITACORA_HISTORICO";
            public const string NOTICIA = "NOTICIA";
            public const string RECLAMO = "RECLAMO";
            public const string SEGURIDAD = "SEGURIDAD";
            public const string CONSORCIO = "CONSORCIO";
            public const string UNIDAD = "UNIDAD";
            public const string GASTO = "GASTO";
            public const string EXPENSAS = "EXPENSAS";
            public const string BALANCE = "BALANCE";
        }

        public class PatentCode
        {
            public const string ADD = "ADD";
            public const string EDIT = "EDIT";
            public const string DELETE = "DELETE";
            public const string VIEW = "VIEW";
            public const string APPROVE = "APPROVE";
            public const string BACKUP = "BACKUP";
            public const string RESTORE = "RESTORE";
            public const string DIGITOS = "DIGITOS";
            public const string USUARIO_PATENTE = "USUARIO_PATENTE";
        }


        public class FileSaveMode
        {
            public const string DATABASE = "DATABASE";
            public const string AZURE = "AZURE";
            public const string FILESYSTEM = "FILESYSTEM";

            public static readonly Dictionary<string, string> VALUES = new Dictionary<string, string>
            {
                { DATABASE, DATABASE },
                { AZURE, AZURE },
                { FILESYSTEM, FILESYSTEM }
            };
        }

        public class BitacoraCriticidad
        {
            public const string BAJA = "BAJA";
            public const string MEDIA = "MEDIA";
            public const string ALTA = "ALTA";
            public const string CRITICA = "CRITICA";
        }

        public class ConsorcioVariables
        {
            public const int ActualConsorcioId = 1;
        }

        public class NotificacionCodigo
        {
            public const string NUEVO_RECLAMO = "Nuevo Reclamo";
            public const string RECLAMO_FINALIZADO = "Reclamo Finalizado";
        }
    }
}