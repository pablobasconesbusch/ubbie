﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Threading;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Aplicacion
{
    public class ConfigurationService : ServicioBase
    {
        public ConfigGroup GetConfigGroup(string code)
        {
            return DB
                .ConfigGroup
                .Include(x => x.ConfigSettings)
                .SingleOrDefault(x => x.Code == code);
        }


        public List<ConfigGroup> GetConfigGroupList()
        {
            return DB
                .ConfigGroup
                .Include(x => x.ConfigSettings)
                .ToList();
        }

        public List<ConfigSetting> GetConfigSettingList()
        {
            return DB.ConfigSetting.ToList();
        }

        public List<ConfigSetting> GetEditableSettingsList()
        {
            return DB.ConfigSetting.Where(x => x.IsEditable).ToList();
        }

        public void LoadConfig()
        {
            var configSettingList = GetConfigSettingList();

            foreach (var cs in configSettingList)
            {
                var type = typeof(Configuration);

                foreach (var groupProperty in type.GetProperties())
                {
                    if (groupProperty.Name != cs.ConfigGroupcode) continue;

                    var groupInstance = groupProperty.GetValue(null);

                    foreach (var settingProperty in groupInstance.GetType().GetProperties().Where(settingProperty => settingProperty.Name == cs.Key))
                    {
                        switch (settingProperty.PropertyType.Name.ToLower())
                        {
                            case "string":
                                settingProperty.SetValue(groupInstance, cs.Value, null);
                                break;
                            case "int32":
                                settingProperty.SetValue(groupInstance, cs.Value.ToInt(), null);
                                break;
                            case "datetime":
                                settingProperty.SetValue(groupInstance, cs.Value.ToDateTime(), null);
                                break;
                            case "boolean":
                                settingProperty.SetValue(groupInstance, cs.Value.ToBool(), null);
                                break;
                            default:
                                settingProperty.SetValue(groupInstance, cs.Value, null);
                                break;
                        }
                    }
                }
            }
        }

        public ServiceResponse UpdateConfigSettings(List<ConfigGroup> configGroups)
        {
            var sr = new ServiceResponse();

            foreach (var configGroup in configGroups)
            {
                var dbConfigGroup = DB.ConfigGroup.Single(x => x.Code == configGroup.Code);

                foreach (var configSetting in configGroup.ConfigSettings)
                {
                    dbConfigGroup.ConfigSettings.Single(x => x.Key == configSetting.Key).Value = configSetting.Value;
                }

            }

            DB.SaveChanges();
            new ConfigurationService().LoadConfig();
            return sr;
        }


        public static void SetupRefreshConfigTask()
        {
            var minute = 1000 * 60;
            var hour = minute * 60;

            PeriodicTaskFactory.Start(new ConfigurationService().LoadConfig, hour * 5);
        }

    }
}
