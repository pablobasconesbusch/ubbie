﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;

namespace Ubbie.Servicios.Aplicacion
{
    public class ServicioBitacora : ServicioBase
    {
        public List<Bitacora> GetListExport(DateTime fechaDesde, DateTime fechaHasta)
        {
            var nextDate = fechaHasta.AddDays(1);

            return DB
                .Bitacora
                .Include(x => x.Usuario)
                .Where(x => x.DateTime >= fechaDesde && x.DateTime < nextDate && x.UsuarioId.HasValue)
                .ToList();
        }

        public List<Bitacora> GetList()
        {
            return DB
                .Bitacora
                .Include(x => x.Usuario)
                .ToList();
        }

        public ServiceResponse GetSetupList(BitacoraParameters p)
        {
            var sr = new ServiceResponse();

            var hoy = Configuration.CurrentLocalTime.Date;
            var unMesAntes = hoy.AddMonths(-1);

            var fechaDesde = p.FechaDesde;
            var fechaHasta = p.FechaHasta.AddDays(1);

            var query = DB
                .Bitacora
                .Include(x => x.Usuario)
                .AsNoTracking()
                .Where(x => x.DateTime >= fechaDesde && x.DateTime < fechaHasta)
                .AsQueryable();

            if (p.IsHistory)
            {
                query = query.Where(x => x.DateTime < unMesAntes);
            }
            else
            {
                query = query.Where(x => x.DateTime >= unMesAntes);
            }



            if (!string.IsNullOrEmpty(p.Query))
                query = query.Where(x => x.DateTime.ToString("G").Contains(p.Query) || x.Usuario.Email.Contains(p.Query));

            if (p.UsuarioId != 0)
                query = query.Where(x => x.UsuarioId == p.UsuarioId);


            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        public static void Log(string detail, string criticidad)
        {
            var bitacora = new Entidades.Seguridad.Bitacora();
            bitacora.DateTime = DateTime.UtcNow;
            bitacora.Detalle = CustomEncrypt.Encrypt(detail);
            bitacora.IP = UsuarioActivo?.DireccionIP ?? "";
            bitacora.UsuarioId = UsuarioActivo?.Id;
            bitacora.Criticidad = criticidad;
            bitacora.DVH = "";

            Log(bitacora);
        }


        public static void Log(Entidades.Seguridad.Bitacora entity)
        {
            if (Configuration.Support.EnableLogging)
            {
                DB.Bitacora.Add(entity);
            }


            if (UsuarioActivo == null)
            {
                DB.SaveChanges();
            }
            else
            {
                DB.SaveChanges(UsuarioActivo.Id);
            }
        }

    }

    public class BitacoraParameters : PagerParameters
    {
        public int UsuarioId { get; set; }
        public string Criticidad { get; set; }

        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }

        public bool IsHistory { get; set; }
    }
}