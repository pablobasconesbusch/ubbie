﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Ubbie.Entidades.Seguridad;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Framework.Web;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;

namespace Ubbie.Servicios.Aplicacion
{
    public class EmailingService : ServicioBase
    {

        #region Config

        private static List<EmailTemplate> EmailTemplates { get; set; }
        public static string EmailTemplatesPath { get; set; }

        private const int EmailQueueInterval = 1000 * 60 * 60;

        #endregion

        public static Email Get(int entityId)
        {
            return DB
                .Email
                .SingleOrDefault(x => x.Id == entityId);
        }

        public static ServiceResponse GetList(EmailLogParameters p)
        {

            var sr = new ServiceResponse();

            p.FilterByDateFrom = p.FilterByDateFrom.AddDays(-1);
            p.FilterByDateTo = p.FilterByDateTo.AddDays(1);

            var query = DB
                .Email
                .Where(x => x.FechaCreacion >= p.FilterByDateFrom.Date && x.FechaCreacion <= p.FilterByDateTo.Date)
                .AsNoTracking()
                .AsQueryable();

            if (!string.IsNullOrEmpty(p.EmailStatusCode))
                query = query.Where(x => x.EmailStatusCode == p.EmailStatusCode);

            sr.ReturnValue = query.Count();

            sr.Data = query
                .OrderBy(p.SortField + " " + p.SortDirection)
                .Skip((p.PageIndex - 1) * p.PageSize)
                .Take(p.PageSize)
                .ToList();

            return sr;
        }

        public static ServiceResponse SendTestEmail(SmtpClient smtp, Email email)
        {
            MailMessage mail = null;

            var sr = new ServiceResponse();

            try
            {
                mail = new MailMessage
                {
                    IsBodyHtml = true,
                    Subject = email.Subject,
                    From = new MailAddress(Configuration.Emailing.FromAddress),
                    Body = email.Body,
                    Priority = MailPriority.Normal
                };

                foreach (var emailAddress in email.Recipients.Split(';'))
                {
                    mail.To.Add(emailAddress.Trim());
                }

                DB.Email.Add(email);

                smtp.Send(mail);
                email.EmailStatusCode = Constants.EmailStatusCode.SENT;
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
                email.Log = ex.ToString();
                email.EmailStatusCode = Constants.EmailStatusCode.ERROR;
                EventLogService.Error(Constants.EventCode.EMAIL, ex);
            }
            finally
            {
                mail?.Dispose();
                smtp?.Dispose();
            }

            DB.SaveChanges(UsuarioActivo?.Id ?? 0);

            return sr;
        }

        public static ServiceResponse ResendEmail(int emailId)
        {
            var sr = new ServiceResponse();

            var dbEmail = Get(emailId);

            var email = dbEmail.MapToEntity<Email>();

            email.Id = 0;
            email.EmailStatusCode = Constants.EmailStatusCode.QUEUED;
            email.Log = null;

            DB.Email.Add(email);
            DB.SaveChanges(UsuarioActivo?.Id ?? 0);

            var t = new Task(() => SendQueuedEmail(email));
            t.Start();


            return sr;
        }

        public static ServiceResponse DeliverEmail(string emailTemplateCode, List<string> recipients, Dictionary<string, string> fields, List<string> attachmentsUrl = null, string sender = null)
        {
            var sr = new ServiceResponse();

            if (!Configuration.Emailing.EmailingEnabled)
            {
                sr.AddError("El servicio de correo esta deshabilitado");
                EventLogService.Info(Constants.EventCode.EMAIL, "El servicio de correo esta deshabilitado");
                return sr;
            }

            var emailTemplate =
                EmailTemplates
                .SingleOrDefault(x => x.Code == emailTemplateCode);

            if (emailTemplate == null)
            {
                sr.AddError("Se quiso enviar un correo pero el template del mismo no fue encontrado en la tabla EmailTemplate");
                EventLogService.Info(Constants.EventCode.EMAIL, "Se quiso enviar un correo pero el template del mismo no fue encontrado en la tabla EmailTemplate");
                return sr;
            }

            string layoutHtml;
            using (var layoutFile = new StreamReader($"{EmailTemplatesPath}\\{emailTemplate.Layout}.html", Encoding.GetEncoding("ISO-8859-1")))
            {
                layoutHtml = layoutFile.ReadToEnd();
            }

            string templateHtml;
            using (var templateFile = new StreamReader($"{EmailTemplatesPath}\\{emailTemplateCode}.html", Encoding.GetEncoding("ISO-8859-1")))
            {
                templateHtml = templateFile.ReadToEnd();
            }

            var emailHtml = layoutHtml
                .Replace("[site-url]", Configuration.Application.SiteUrl)
                .Replace("[site-logo-url]", Configuration.Application.SiteLogoUrl)
                .Replace("[site-footer]", Configuration.Application.EmailFooter)
                .Replace("[body]", templateHtml);

            var email = new Email
            {
                Subject = MergeFields(emailTemplate.Subject, fields),
                Body = MergeFields(emailHtml, fields),
                Sender = sender ?? Configuration.Emailing.FromAddress,
                Recipients = string.Join(";", recipients),
                EmailStatusCode = Constants.EmailStatusCode.QUEUED
            };

            if (attachmentsUrl != null)
            {
                foreach (var attachment in attachmentsUrl)
                {
                    var emailAttachment = new EmailAttachment();
                    emailAttachment.Url = attachment;

                    email.EmailAttachments.Add(emailAttachment);
                }
            }

            DB.Email.Add(email);
            DB.SaveChanges(UsuarioActivo?.Id ?? 0);

            var t = new Task(() => SendQueuedEmail(email));
            t.Start();

            return sr;
        }

        public static void FillEmailTemplates()
        {
            EmailTemplates = DB.EmailTemplate.ToList();
        }

        private static void SendQueuedEmail(Email email)
        {
            MailMessage mail = null;
            SmtpClient smtpClient = null;

            try
            {
                smtpClient = new SmtpClient
                {
                    Host = Configuration.Emailing.SmtpServer,
                    Port = Configuration.Emailing.SmtpPort,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Configuration.Emailing.SmtpUseDefaultCredentials,
                    EnableSsl = Configuration.Emailing.SmtpEnableSsl,
                    Credentials = new NetworkCredential(Configuration.Emailing.SmtpUsername, Configuration.Emailing.SmtpPassword),
                    ServicePoint = { MaxIdleTime = 0 }
                };

                mail = new MailMessage
                {
                    IsBodyHtml = true,
                    Subject = email.Subject,
                    From = new MailAddress(email.Sender),
                    Body = email.Body,
                    Priority = email.Important ? MailPriority.High : MailPriority.Normal
                };

                foreach (var emailAddress in email.Recipients.Split(';'))
                {
                    mail.To.Add(emailAddress.Trim());
                }

                foreach (var emailAttach in email.EmailAttachments)
                {
                    using (var wc = new WebClient())
                    {
                        wc.DownloadFile(emailAttach.Url, emailAttach.FileName);
                        mail.Attachments.Add(new Attachment(emailAttach.FileName));
                    }
                }

                DB.Email.Attach(email);

                smtpClient.Send(mail);
                email.EmailStatusCode = Constants.EmailStatusCode.SENT;
            }
            catch (Exception ex)
            {
                email.Log = ex.ToString();
                email.EmailStatusCode = Constants.EmailStatusCode.ERROR;
                EventLogService.Error(Constants.EventCode.EMAIL, ex);
            }
            finally
            {
                mail?.Dispose();
                smtpClient?.Dispose();
            }

            DB.SaveChanges();
        }

        private static string MergeFields(string value, Dictionary<string, string> fields)
        {
            foreach (var field in fields)
            {
                value = value.Replace("[" + field.Key + "]", field.Value);
            }

            return value;
        }


        #region Queue Manager

        public static readonly object MThreadLock = new object();
        public delegate void ProcessQueueDelegate();
        public static Timer QueueTimer { get; set; }
        public static bool ProcessingQueue { get; set; }

        public static void StartQueueManager()
        {
            QueueTimer = new Timer { Interval = EmailQueueInterval };
            QueueTimer.Elapsed += QueueTimer_Elapsed;
            QueueTimer.Start();
        }

        private static void QueueTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            TriggerProcessQueue();
        }

        public static void TriggerProcessQueue()
        {
            ProcessQueueDelegate starter = ProcessQueue;
            starter.BeginInvoke(null, null);
        }

        public static void ProcessQueue()
        {
            lock (MThreadLock)
            {
                ProcessingQueue = true;

                var yesterday = DateTime.UtcNow.AddDays(-1);
                var yesterdayEmails = DB
                    .Email
                    .Where(x => x.EmailStatusCode == Constants.EmailStatusCode.ERROR && x.FechaCreacion > yesterday)
                    .ToList();

                foreach (var email in yesterdayEmails)
                {
                    SendQueuedEmail(email);
                }

                ProcessingQueue = false;
            }
        }

        #endregion

        #region Emails

        public static ServiceResponse NewUser(Usuario user)
        {
            const string template = Constants.EmailTemplateCode.NewUser;

            var activationLink = Configuration.Security.UserActivationUrl + HttpHelper.SafeUrlEncode(CustomEncrypt.Encrypt($"{user.Id}@{DateTime.Now.ToString(new CultureInfo("en-US"))}"));
            var recipients = new List<string> { user.Email };

            return DeliverEmail(template, recipients, new Dictionary<string, string>
            {
                {"user-fullname", $"{CustomEncrypt.Decrypt(user.Nombre)} {CustomEncrypt.Decrypt(user.Apellido)}"},
                {"activate-account-url", activationLink}
            });
        }

        public static ServiceResponse RestablishPassword(Usuario user)
        {
            const string template = Constants.EmailTemplateCode.RestablishPassword;

            var keyEncrypted = CustomEncrypt.Encrypt($"{user.Id}@{DateTime.UtcNow.ToString(new CultureInfo("en-US"))}");
            var restablishPasswordUrl = $"{Configuration.Security.PasswordRestablishmentUrl}{HttpHelper.SafeUrlDecode(keyEncrypted)}";

            var recipients = new List<string> { user.Email };

            return DeliverEmail(template, recipients, new Dictionary<string, string>
            {
                {"user-fullname", user.NombreCompleto},
                {"restablish-password-url", restablishPasswordUrl}
            });
        }

        public static ServiceResponse SendContactMessage(ContactModel entity)
        {
            const string template = Constants.EmailTemplateCode.ContactEmail;

            var recipients = Configuration.Support.DaemonEmailList.Split(";").ToList();

            return DeliverEmail(template, recipients, new Dictionary<string, string>
            {
                { "name", entity.Name },
                { "message", entity.Message }
            }, null, entity.Email);
        }

        #endregion

    }


    public class EmailLogParameters : PagerParameters
    {
        public DateTime FilterByDateFrom { get; set; }
        public DateTime FilterByDateTo { get; set; }

        public string EmailStatusCode { get; set; }

    }
}
