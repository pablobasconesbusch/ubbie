﻿using System.Collections.Generic;

namespace Ubbie.Servicios.Inheritable
{
    public interface IQueryServiceBase<V, K>
    {
        V Get(K key);
        List<V> GetList();
    }
}