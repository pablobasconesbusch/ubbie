﻿using Ubbie.Servicios.Base;

namespace Ubbie.Servicios.Inheritable
{
    public interface ISetupServiceBase<T>
    {
        ServiceResponse GetSetupList(PagerParameters p);

        ServiceResponse SaveEntity(T entity);
        ServiceResponse DeleteEntity(T entity);

        ServiceResponse ValidateSaveEntity(T entity);
        ServiceResponse ValidateDeleteEntity(T entity);
    }
}