﻿using System.Web.Services;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Areas.Rol.Models;

namespace Ubbie.WebSite
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Helper : System.Web.Services.WebService
    {

        [WebMethod]
        public ServiceResponse GuardarFamilia(RolInfoModel model)
        {
            var entidad = model.ToEntity();
            var sr = new ServicioRol().SaveEntity(entidad);

            if (sr.Status)
                CacheManager.ObtenerUsuarioActivo(true);

            return sr;
        }

        [WebMethod]
        public ServiceResponse EliminarFamilia(RolInfoModel model)
        {
            var service = new ServicioRol();
            var sr = service.DeleteEntity(model.ToEntity());
            return sr;
        }
    }
}
