﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Unidad.Models
{
    public class UnidadModel : MapperModel<UnidadModel, Entidades.Negocio.Unidad, int>
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Name")]
        [RegularExpression("^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_UseLetterOnly")]
        public string Nombre { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Rooms")]
        [RegularExpression("^[0-9]+$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_JustNumbers))]
        public int? Ambientes { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Meters")]
        [RegularExpression("^[0-9]+$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_JustNumbers))]
        public double? Metros { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_Owner")]
        public int PropietarioId { get; set; }
        public SelectList Propietarios { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_Consorcio")]
        public int ConsorcioId { get; set; }
        public SelectList Consorcios { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_UnitType")]
        public int TipoUnidadId { get; set; }
        public SelectList TipoUnidades { get; set; }


        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Nombre;
            if (IsInitialized)
                return;

            HeaderTitle = $"{Translation.Label_New} {Translation.Label_Consorcio}";

            Propietarios = new ServicioUsuario().GetUsersByRole(Constants.RoleCode.INQUILINO).ToSelectList(x => x.Id, x => $"{CustomEncrypt.Decrypt(x.Nombre)} {CustomEncrypt.Decrypt(x.Apellido)}");
            Consorcios = new ServicioConsorcio().GetList().ToSelectList(x => x.Id, x => x.Nombre);
            TipoUnidades = new ServicioUnidad().GetTipoUnidades().ToSelectList(x => x.Id, x => x.Nombre);
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Unidad entity)
        {

        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Unidad entity)
        {

        }


        public UnidadModel() { }

        public UnidadModel(int id) : base(id, x => new ServicioUnidad().Get(id))
        {

        }

        public UnidadModel(Entidades.Negocio.Unidad entity) : base(entity)
        {

        }
    }
}
