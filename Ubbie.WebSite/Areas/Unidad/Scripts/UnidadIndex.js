﻿$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Unidad.List,
                params: function () {
                    return {

                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: 'Nombre', data: 'Nombre' },
                    { title: 'Metros Cuadrados', data: 'Metros' },
                    { title: 'Propietario', data: 'Propietario' }
                ],
                order: [[0, "asc"]]
            });
        },
    });

    ASApp.InfoView.init({
        mainTab: {
            onInit: function (tab) {
                Unidad.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Unidad.Delete
                }
            }
        },
        tabs: []
    });
});

var Unidad = {

    controls: {

    },

    c: {},

    init: function (tab) {
        var b = Unidad;

        b.initControls(tab);
        b.initPlugins(tab);
        b.initHandlers(tab);

    },

    initControls: function (tab) {
        var b = Unidad;
        b.c = $.extend({}, b.controls);

    },

    initPlugins: function (tab) {
        var b = Unidad;

    },

    initHandlers: function (tab) {
        var b = Unidad;

    }
}

