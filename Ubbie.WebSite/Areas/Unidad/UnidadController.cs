﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Unidad.Models;

namespace Ubbie.WebSite.Areas.Unidad
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.UNIDAD)]
    [ViewsPath("~/Areas/Unidad/Views")]
    public class UnidadController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Info(int entityId)
        {
            var model = entityId == 0 ? new UnidadModel() : new UnidadModel(entityId);
            model.InitializateData();
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            if (p.SortField == "Propietario") p.SortField = "Propietario.Nombre";

            var service = new ServicioUnidad();

            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Negocio.Unidad>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    x.Nombre,
                    x.Metros,
                    Propietario = $"{CustomEncrypt.Decrypt(x.Propietario.Nombre)} {CustomEncrypt.Decrypt(x.Propietario.Apellido)}"
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.UNIDAD, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.UNIDAD, Constants.PatentCode.EDIT)]
        public JsonResult Save(UnidadModel model)
        {
            var service = new ServicioUnidad();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);
            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.UNIDAD, Constants.PatentCode.DELETE)]
        public JsonResult Delete(UnidadModel model)
        {
            var service = new ServicioUnidad();
            var entity = model.ToEntity();
            var sr = service.DeleteEntity(entity);
            return Json(sr);
        }
    }
}
