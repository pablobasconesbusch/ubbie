﻿$(function() {

    $('[data-export-usuarios]').on('click', function () {
        document.location = URLs.Exportar.XMLExportUsuarios;

    });

    $('[data-export-reclamos]').on('click', function () {
        document.location = URLs.Exportar.XMLExportReclamos;
    });

});