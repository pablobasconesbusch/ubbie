﻿using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using System.Xml.Serialization;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Exportar
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.BITACORA)]
    [ViewsPath("~/Areas/Exportar/Views")]
    public class ExportarController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult XMLExportUsuarios()
        {
            var usuarios = new ServicioUsuario().GetListForExport();
            usuarios.ForEach(x =>
            {
                x.Nombre = CustomEncrypt.Decrypt(x.Nombre);
                x.Apellido = CustomEncrypt.Decrypt(x.Apellido);
            });
            var serializer = new XmlSerializer(typeof(List<Entidades.Seguridad.Usuario>));

            var fileStream = new StreamWriter(@"C:\UbbieXML\reporteUsuarios.xml");
            serializer.Serialize(fileStream, usuarios);

            fileStream.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\UbbieXML\reporteUsuarios.xml");
            var fileName = "ReporteUsuarios.xml";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpGet]
        public ActionResult XMLExportReclamos()
        {
            var reclamos = new ServicioReclamo().GetListForExport();
            reclamos.ForEach(x =>
            {
                x.Usuario.Nombre = CustomEncrypt.Decrypt(x.Usuario.Nombre);
                x.Usuario.Apellido = CustomEncrypt.Decrypt(x.Usuario.Apellido);
            });
            var serializer = new XmlSerializer(typeof(List<Entidades.Negocio.Reclamo>));

            var fileStream = new StreamWriter(@"C:\UbbieXML\reporteReclamos.xml");
            serializer.Serialize(fileStream, reclamos);

            fileStream.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\UbbieXML\reporteReclamos.xml");
            var fileName = "ReporteReclamos.xml";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

    }
}
