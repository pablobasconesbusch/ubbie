﻿

$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Rol.List,
                params: function () {
                    return {
                        Parameters: {
                        }
                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: codigoLabel, data: 'Codigo', width: '10%' },
                    { title: nombreLabel, data: 'Nombre' }
                ],
                order: [[0, "asc"]]
            });
        },
        onInit: function () {

        }
    });

    ASApp.InfoView.init({
        entity: {
            entityId: ''
        },

        mainTab: {
            onInit: function (tab) {
                RolInfo.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Rol.Delete,
                    codigo: ASApp.InfoView.entity.entityId
                }
            }
        }
    });
});
