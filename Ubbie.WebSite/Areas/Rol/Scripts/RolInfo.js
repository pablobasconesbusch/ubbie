﻿

var RolInfo = {

    tab: {},

    controls: {
        dataResource: '[data-resource]',
        dataResourceCode: '[data-resource-code]',
        dataPatentCode: '[data-patent-code]',


        DefaultResourceCode: '[name="DefaultCodigoFamilia"]'
    },

    c: {},

    init: function (tab) {
        b.tab = tab;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {

        $.extend(b.c, b.controls);

    },

    initPlugins: function () {
    },

    initHandlers: function () {

        b.tab.form.find(b.controls.dataResourceCode).on('change', function () {
            var $this = $(this);

            if (!$(b.controls.dataResourceCode).filter(':checked').length) {

                ASApp.Notify.error({
                    message: 'You must select at least one resource'
                });
                $this.prop('checked', !$this.is(':checked'));
                return;
            }


            var parent = $this.closest(b.controls.dataResource);

            parent.find(b.controls.dataPatentCode).prop('checked', $this.prop('checked'));
            parent.find(b.controls.DefaultResourceCode).prop('disabled', !$this.prop('checked'));

            if (!$this.is(':checked') && parent.find(b.controls.DefaultResourceCode).is(':checked')) {

                var dataResourceCodes = $(b.controls.dataResourceCode).filter(':checked').first();
                var resourceParent = dataResourceCodes.closest(b.controls.dataResource);

                resourceParent.find(b.controls.DefaultResourceCode).prop('checked', true);
            }
        });

        b.tab.form.find(b.controls.dataPatentCode).on('change', function () {
            var $this = $(this);

            var parent = $this.closest(b.controls.dataResource);
            var patents = parent.find(b.controls.dataPatentCode);
            var checkedPatents = patents.filter(':checked');

            if (checkedPatents.length) {
                parent.find(b.controls.dataResourceCode).prop('checked', true);
                parent.find(b.controls.DefaultResourceCode).prop('disabled', false);
            }
        });

    }
}

var b = RolInfo;