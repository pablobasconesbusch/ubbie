﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Rol.Models
{
    public class RolInfoModel : MapperModel<RolInfoModel, Entidades.Seguridad.Rol, string>
    {
        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Code")]
        public string Codigo { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Name")]
        public string Nombre { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_DefaultResourceCode")]
        public string DefaultCodigoFamilia { get; set; }

        public List<RolInfoResourceModel> ResourceList { get; set; }


        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Nombre;
            if (IsInitialized)
                return;
            HeaderTitle = $"{Translation.Label_New} {Translation.Label_Role}";

            ResourceList = new ServicioRol().GetResourceList().Select(x =>
                new RolInfoResourceModel
                {
                    Key = x.Codigo,
                    Name = x.Descripcion,
                    Selected = true,
                    ResourcePatentList = x.FamiliaPatentes.Select(y =>
                        new RolInfoResourcePatentModel
                        {
                            Key = y.Patente.Codigo,
                            Name = y.Patente.Descripcion,
                            Selected = true
                        }).ToList()
                }).ToList();

            DefaultCodigoFamilia = ResourceList.First().Key;
        }

        protected override void ExtraMapFromEntity(Entidades.Seguridad.Rol entity)
        {
            ResourceList.ForEach(x => x.Selected = false);
            ResourceList.SelectMany(x => x.ResourcePatentList).ToList().ForEach(x => x.Selected = false);

            ResourceList.Where(x => entity.RolFamilias.Select(y => y.CodigoFamilia).Contains(x.Key)).ToList().ForEach(x =>
            {
                x.Selected = true;

                var resource = entity.RolFamilias.Single(y => y.CodigoFamilia == x.Key);
                x.ResourcePatentList.Where(y => resource.RolFamiliaPatentes.Select(z => z.CodigoPatente).Contains(y.Key)).ToList().ForEach(y => y.Selected = true);
            });
        }

        protected override void ExtraMapToEntity(Entidades.Seguridad.Rol entity)
        {
            entity.RolFamilias = ResourceList.Where(x => x.Selected).Select(x => new RolFamilia
            {
                CodigoRol = Codigo,
                CodigoFamilia = x.Key,
                RolFamiliaPatentes = x.ResourcePatentList?.Where(y => y.Selected).Select(y => new RolFamiliaPatente
                {
                    CodigoRol = Codigo,
                    CodigoFamilia = x.Key,
                    CodigoPatente = y.Key
                }).ToList() ?? new List<RolFamiliaPatente>()
            }).ToList();

        }

        public RolInfoModel()
        {

        }

        public RolInfoModel(string id) : base(id, x => new ServicioRol().Get(x))
        {

        }

        public RolInfoModel(Entidades.Seguridad.Rol entity) : base(entity)
        {

        }
    }
}