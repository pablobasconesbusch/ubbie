﻿using System.Collections.Generic;
using Ubbie.WebSite.Areas.Rol.Models;
using Ubbie.WebSite.Shared.Models;

namespace Ubbie.WebSite.Areas.Rol.Models
{
    public class RolInfoResourceModel : CheckEntityModel<string>
    {
        public List<RolInfoResourcePatentModel> ResourcePatentList { get; set; }
    }
}
