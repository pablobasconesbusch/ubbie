﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Rol.Models;

namespace Ubbie.WebSite.Areas.Rol
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.ROLE)]
    [ViewsPath("~/Areas/Rol/Views")]
    public class RolController : BaseController
    {
        public ActionResult Index()
        {
            if (!Configuration.Security.EnableRoleManagement)
                return HttpNotFound();

            return View();
        }

        public ActionResult Info(string entityId)
        {
            RolInfoModel model;

            if (string.IsNullOrEmpty(entityId))
                model = new RolInfoModel();
            else
                model = new RolInfoModel(entityId);

            model.InitializateData();
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            var service = new ServicioRol();
            var sr = service.GetSetupList(p);

            if (!sr.Status) return Json(sr);

            sr.Data = ((List<Entidades.Seguridad.Rol>)sr.Data)
                .Select(x => new
                {
                    x.Codigo,
                    x.Nombre
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.ROLE, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.ROLE, Constants.PatentCode.EDIT)]
        public JsonResult Save(RolInfoModel model)
        {
            var service = new ServicioRol();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);

            if (!sr.Status)
                return Json(sr);

            CacheManager.ObtenerUsuarioActivo(true);
            sr.Reload = true;

            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.ROLE, Constants.PatentCode.DELETE)]
        public JsonResult Delete(RolInfoModel model)
        {
            var service = new ServicioRol();
            var entity = new Entidades.Seguridad.Rol { Codigo = model.Codigo };
            var sr = service.DeleteEntity(entity);

            return Json(sr);
        }
    }
}