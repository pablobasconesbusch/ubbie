﻿using System.Web.Mvc;
using Ubbie.Framework.Extensiones;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Error.Models;

namespace Ubbie.WebSite.Areas.Error
{
    [ViewsPath("~/Areas/Error/Views")]
    public class ErrorController : BaseController
    {
        // GET: Error
        public ActionResult Index(ErrorModel model)
        {
            if (model.IsNull())
                return RedirectToAction("Index", "Home");

            Response.StatusCode = 404;
            return View();
        }

        // GET: Error/PageNotFound
        public ActionResult PageNotFound()
        {
            Response.StatusCode = 404;
            return View();
        }
    }
}