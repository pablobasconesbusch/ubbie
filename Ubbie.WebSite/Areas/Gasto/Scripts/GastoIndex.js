﻿$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Gasto.List,
                params: function () {
                    return {

                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: 'Fecha', data: 'Fecha' },
                    { title: 'Descripción', data: 'Descripcion' },
                    { title: 'Monto', data: 'Monto' }
                ],
                order: [[0, "desc"]]
            });
        },
    });

    ASApp.InfoView.init({
        mainTab: {
            onInit: function (tab) {
                Gasto.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Gasto.Delete
                }
            }
        },
        tabs: []
    });
});

var Gasto = {

    controls: {

    },

    c: {},

    init: function (tab) {
        var b = Gasto;

        b.initControls(tab);
        b.initPlugins(tab);
        b.initHandlers(tab);

    },

    initControls: function (tab) {
        var b = Gasto;
        b.c = $.extend({}, b.controls);

    },

    initPlugins: function (tab) {
        var b = Gasto;

        ASApp.Plugins.BootstrapDatepicker.init(tab.targetContent);
        ASApp.Plugins.AutoNumeric.init(tab.targetContent);

    },

    initHandlers: function (tab) {
        var b = Gasto;

    }
}

