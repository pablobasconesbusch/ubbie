﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Gasto.Models;

namespace Ubbie.WebSite.Areas.Gasto
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.GASTO)]
    [ViewsPath("~/Areas/Gasto/Views")]
    public class GastoController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Info(int entityId)
        {
            var model = entityId == 0 ? new GastoModel() : new GastoModel(entityId);
            model.InitializateData();
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            var service = new ServicioGasto();

            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Negocio.Gasto>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    Fecha = x.Fecha.ToString("d"),
                    x.Descripcion,
                    Monto = x.Monto.ToString("C"),
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.GASTO, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.GASTO, Constants.PatentCode.EDIT)]
        public JsonResult Save(GastoModel model)
        {
            var service = new ServicioGasto();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);
            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.GASTO, Constants.PatentCode.DELETE)]
        public JsonResult Delete(GastoModel model)
        {
            var service = new ServicioGasto();
            var entity = model.ToEntity();
            var sr = service.DeleteEntity(entity);
            return Json(sr);
        }

    }
}
