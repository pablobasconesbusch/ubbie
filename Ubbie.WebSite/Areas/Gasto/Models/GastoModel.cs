﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Gasto.Models
{
    public class GastoModel : MapperModel<GastoModel, Entidades.Negocio.Gasto, int>
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Description")]
        public string Descripcion { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_InvoiceNumber")]
        public string NroComprobante { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Amount")]
        public decimal Monto { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Date")]
        public string Fecha { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_Consorcio")]
        public int ConsorcioId { get; set; }
        public SelectList Consorcios { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_SpendingType")]
        public int TipoGastoId { get; set; }
        public SelectList TipoGastos { get; set; }


        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Descripcion;
            if (IsInitialized)
                return;

            HeaderTitle = $"{Translation.Label_New} {Translation.Label_Spending}";

            Consorcios = new ServicioConsorcio().GetList().ToSelectList(x => x.Id, x => x.Nombre);
            TipoGastos = new ServicioGasto().GetTipoGastos().ToSelectList(x => x.Id, x => x.Nombre);
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Gasto entity)
        {
            Fecha = entity.Fecha.ToString("d");
        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Gasto entity)
        {
            if (!string.IsNullOrEmpty(Fecha))
            {
                DateTime fecha;
                var conversion = DateTime.TryParse(Fecha, out fecha);

                if (!conversion)
                    throw new Exception($"{nameof(Fecha)} conversion error");

                entity.Fecha = fecha;
            }
        }

        public GastoModel() { }

        public GastoModel(int id) : base(id, x => new ServicioGasto().Get(id))
        {

        }

        public GastoModel(Entidades.Negocio.Gasto entity) : base(entity)
        {

        }
    }
}
