﻿



var UsuarioInfo = {

    tab: {},

    controls: {
    },

    c: {},

    init: function (tab) {
        b.tab = tab;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {

        $.extend(b.c, b.controls);

    },

    initPlugins: function () {
        ASApp.Plugins.BootstrapDatepicker.init();
    },

    initHandlers: function () {

    }
}

var b = UsuarioInfo;
