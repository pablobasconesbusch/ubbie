﻿


$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Usuario.List,
                params: function () {
                    return {

                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: nombreLabel, data: 'Nombre' },
                    { title: apellidoLabel, data: 'Apellido' },
                    { title: 'Email', data: 'Email' },
                    { title: rolLabel, data: 'RolNombre' }
                ],
                order: [[0, "asc"]]
            });
        },
        onInit: function () {

        }
    });

    ASApp.InfoView.init({
        mainTab: {
            onInit: function (tab) {
                UsuarioInfo.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Usuario.Delete
                }
            }
        },
        tabs: []
    });
});
