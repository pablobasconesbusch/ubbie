﻿$(function () {

    $('.js-btn-collapse').on('click', function () {
        var $this = $(this);

        var icon = $this.find('i');
        icon.toggleClass('fa-angle-down fa-angle-up');
    });

    $('.js-users').treeview({
        levels: 1,
        expandIcon: 'fa fa-plus',
        collapseIcon: 'fa fa-minus',
        data: $('.js-users').data('users')
    });
});