﻿using System.Collections.Generic;
using System.Linq;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Seguridad;

namespace Ubbie.WebSite.Areas.Usuario.Models
{
    public class UsuarioPatenteIndexModel
    {
        public List<UsuarioPatenteModel> UsuarioModels { get; set; }

        public List<Entidades.Seguridad.Usuario> Usuarios { get; set; }

        public List<object> UsuarioData { get; set; } = new List<object>();

        public UsuarioPatenteIndexModel()
        {
            var usuarios = new ServicioUsuario().GetUserPatentList();
            usuarios.ForEach(x =>
            {
                x.Nombre = CustomEncrypt.Decrypt(x.Nombre);
                x.Apellido = CustomEncrypt.Decrypt(x.Apellido);
            });

            UsuarioModels = usuarios.Select(x => new UsuarioPatenteModel
            {
                NombreCompleto = $"{x.Nombre} {x.Apellido}",
                RolFamilias = x.Rol.RolFamilias,
                RoleName = x.Rol.Nombre
            }).ToList();

            UsuarioModels.ForEach(x =>
            {
                var username = x.NombreCompleto;
                var icon = "icon-user";

                var roleNodes = new List<object>();
                var roleName = $"R: {x.RoleName}";

                var familyNodes = new List<object>();

                x.RolFamilias.ForEach(y =>
                {
                    var familyName = $"F: {y.Familia.Descripcion}";
                    var patentNode = new List<object>();

                    y.RolFamiliaPatentes.ForEach(z =>
                    {
                        var patente = $"P: {z.Patente.Descripcion}";
                        patentNode.Add(new { text = patente });
                    });

                    if (patentNode.Any())
                        familyNodes.Add(new { text = familyName, nodes = patentNode });
                    else
                        familyNodes.Add(new { text = familyName });
                });
                roleNodes.Add(new { text = roleName, nodes = familyNodes });
                UsuarioData.Add(new { text = username, icon, nodes = roleNodes });
            });
        }
    }
}
