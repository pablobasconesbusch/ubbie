﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Usuario.Models
{
    public class UsuarioModel : MapperModel<UsuarioModel, Entidades.Seguridad.Usuario, int>
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_FirstName")]
        [RegularExpression("^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_UseLetterOnly")]
        public string Nombre { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_LastName")]
        [RegularExpression("^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_UseLetterOnly")]
        public string Apellido { get; set; }

        [ASRequired, DataType(DataType.EmailAddress), Display(ResourceType = typeof(Translation), Name = "Label_Email")]
        [EmailAddress(ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_EmailFormatNotValid))]
        public string Email { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_DNI")]
        [RegularExpression("^[0-9]+$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_JustNumbers))]
        public long? DNI { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Phone")]
        public string Telefono { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_BirthDate")]
        public string FechaNacimiento { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Profile")]
        public string CodigoRol { get; set; }
        public string NombreRol { get; set; }
        public SelectList RolList { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_DefaultResourceCode")]
        public string DefaultCodigoRol { get; set; }
        public SelectList DefaultResourceList { get; set; }

        [ASDisplay(nameof(Translation.Label_IsActive))]
        [Display(ResourceType = typeof(Translation), Name = "Label_IsActive")]
        public bool Activo { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_HasToResetPassword")]
        public bool DebeResetearPassword { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_ProfilePicture")]
        public int? ImagenPerfilId { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_IsSuperUser")]
        public bool EsSuperUsuario { get; set; }


        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = $"{Nombre} {Apellido}";
            if (IsInitialized)
                return;

            HeaderTitle = $"{Translation.Label_New} {Translation.Label_User}";
            RolList = new ServicioRol().GetList().ToSelectList(x => x.Codigo, x => x.Nombre);
        }

        protected override void ExtraMapFromEntity(Entidades.Seguridad.Usuario entity)
        {
            Nombre = CustomEncrypt.Decrypt(entity.Nombre);
            Apellido = CustomEncrypt.Decrypt(entity.Apellido);

            DefaultResourceList = new ServicioUsuario().GetAvailableResources(Id).OrderBy(x => x.Descripcion).ToSelectList(x => x.Codigo, x => x.Descripcion);
            DefaultCodigoRol = entity.DefaultCodigoFamilia;

            NombreRol = entity.Rol.Nombre;

            if (entity.FechaNacimiento.HasValue)
            {
                FechaNacimiento = entity.FechaNacimiento.Value.ToString("d");
            }
        }

        protected override void ExtraMapToEntity(Entidades.Seguridad.Usuario entity)
        {
            if (!string.IsNullOrEmpty(FechaNacimiento))
            {
                DateTime fechaNacimiento;
                var conversion = DateTime.TryParse(FechaNacimiento, out fechaNacimiento);

                if (!conversion)
                    throw new Exception($"{nameof(FechaNacimiento)} conversion error");

                entity.FechaNacimiento = fechaNacimiento;
            }

            entity.DefaultCodigoFamilia = DefaultCodigoRol;

            entity.Nombre = CustomEncrypt.Encrypt(Nombre);
            entity.Apellido = CustomEncrypt.Encrypt(Apellido);
        }

        public UsuarioModel()
        {
        }


        public UsuarioModel(int id) : base(id, x => new ServicioUsuario().Get(x))
        {

        }

        public UsuarioModel(Entidades.Seguridad.Usuario entity) : base(entity)
        {

        }
    }
}