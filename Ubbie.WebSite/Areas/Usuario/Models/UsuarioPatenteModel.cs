﻿using System.Collections.Generic;
using Ubbie.Entidades.Seguridad;

namespace Ubbie.WebSite.Areas.Usuario.Models
{
    public class UsuarioPatenteModel
    {
        public string NombreCompleto { get; set; }
        public string RoleName { get; set; }
        public List<RolFamilia> RolFamilias { get; set; } = new List<RolFamilia>();
    }
}
