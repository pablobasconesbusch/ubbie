﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Usuario.Models;

namespace Ubbie.WebSite.Areas.Usuario
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.USER)]
    [ViewsPath("~/Areas/Usuario/Views")]
    public class UsuarioController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Info(int entityId)
        {
            UsuarioModel model;

            if (entityId == 0)
                model = new UsuarioModel();
            else
                model = new UsuarioModel(entityId);

            model.InitializateData();
            return PartialView(model);
        }

        public ActionResult PatenteIndex()
        {
            var model = new UsuarioPatenteIndexModel();
            return View(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            if (p.SortField == "RolName") p.SortField = "Rol.Name";

            var service = new ServicioUsuario();
            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Seguridad.Usuario>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    Nombre = CustomEncrypt.Decrypt(x.Nombre),
                    Apellido = CustomEncrypt.Decrypt(x.Apellido),
                    x.Email,
                    RolNombre = x.Rol.Nombre
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.USER, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.USER, Constants.PatentCode.EDIT)]
        public JsonResult Save(UsuarioModel model)
        {
            var service = new ServicioUsuario();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);

            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.USER, Constants.PatentCode.DELETE)]
        public JsonResult Delete(UsuarioModel model)
        {
            var service = new ServicioUsuario();
            var entity = model.ToEntity();
            var sr = service.DeleteEntity(entity);

            return Json(sr);
        }

        [HttpPost]
        public JsonResult Find(string query)
        {
            var users = new ServicioUsuario().GetList(query);
            return Json(new
            {
                suggestions = users.Select(x => new
                {
                    data = x.Id,
                    value = $"{x.Id} {x.NombreCompleto} ({x.Email}) | {x.CodigoRol} ({x.Rol.Nombre})"
                })
            });
        }
    }
}