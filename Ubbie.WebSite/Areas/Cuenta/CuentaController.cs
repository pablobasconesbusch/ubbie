﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autenticacion;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Cuenta.Models;

namespace Ubbie.WebSite.Areas.Cuenta
{
    [ViewsPath("~/Areas/Cuenta/Views")]
    public class CuentaController : BaseController
    {
        #region Login
        public ActionResult Login(string returnUrl)
        {
            if (UsuarioActivo != null)
                return RedirectToAction("Index", "Home");

            var model = new LoginModel();
            model.ReturnUrl = returnUrl;

            if (HttpContext.Request.Cookies["RememberMe"] != null)
            {
                var rememberMeCookie = HttpContext.Request.Cookies["RememberMe"];
                model.Email = CustomEncrypt.Decrypt(rememberMeCookie["Email"]);
                model.Password = CustomEncrypt.Decrypt(rememberMeCookie["Password"]);
                model.RememberMe = true;
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult PromptLogin(string email)
        {
            var model = new LoginModel();
            model.Email = email;
            model.IsLogged = true;

            new AuthService().Logout();

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult Login(LoginModel model)
        {
            if (model.RecalcularDigitos)
            {
                ServicioBitacora.Log("Se recalcularon los dígitos verificadores", Constants.BitacoraCriticidad.ALTA);
                ServicioDigitos.RecalcularDigitosVerificadores();
            }

            var sr = ServicioSeguridad.Login(model.Email, model.Password);
            if (!sr.Status)
                return Json(sr);

            SetRememberMeCookie(model.Email, model.Password, model.RememberMe);

            new AuthService().Login((Entidades.Seguridad.Usuario)sr.Data, model.RememberMe);
            var user = (Entidades.Seguridad.Usuario)sr.Data;
            sr.Data = null;

            if (model.IsLogged)
                return Json(sr);

            if (!string.IsNullOrEmpty(model.ReturnUrl))
                sr.RedirectUrl = model.ReturnUrl;
            else if (!string.IsNullOrEmpty(user.DefaultCodigoFamilia))
                sr.RedirectUrl = Url.Action(user.DefaultFamilia.Action, user.DefaultFamilia.Controller);
            else
                sr.RedirectUrl = Url.Action(user.Rol.DefaultFamilia.Action, user.Rol.DefaultFamilia.Controller);

            return Json(sr);

        }

        public ActionResult FakeLogin(int usuarioId = 0)
        {
            if (AppInfo.EnvironmentCode == Constants.EnvironmentCode.PRD)
                return RedirectToAction("Login");

            if (usuarioId == 0)
            {
                var model = new FakeLoginModel();
                return View(model);
            }

            if (UsuarioActivo != null)
                new AuthService().Logout();

            var sr = new AuthService().Login(usuarioId, false);
            var user = (Entidades.Seguridad.Usuario)sr.Data;

            if (!string.IsNullOrEmpty(user.DefaultCodigoFamilia))
                return RedirectToAction(user.DefaultFamilia.Action, user.DefaultFamilia.Controller);

            var firstResourceAvailable = user.Rol.RolFamilias.Select(x => x.Familia).First();
            return RedirectToAction(firstResourceAvailable.Action, firstResourceAvailable.Controller);
        }

        public ActionResult Logout()
        {
            new AuthService().Logout();
            return RedirectToAction("FrontPage", "Home");
        }

        #endregion

        #region Activation and Restablishment

        public ActionResult Activate(string key)
        {
            if (string.IsNullOrEmpty(key))
                return RedirectToAction("Index", "Error");

            if (UsuarioActivo != null)
            {
                new AuthService().Logout();
                return RedirectToAction("Activate", new { key });
            }

            var sr = ServicioSeguridad.DecryptUserToActivate(key, true);
            if (!sr.Status)
                return RedirectToAction("Index", "Error");

            var user = (Entidades.Seguridad.Usuario)sr.Data;

            var model = new ActivationModel();
            model.ValidationToken = key;
            model.UsuarioId = user.Id;
            model.Email = user.Email;
            model.EstaActivandoCuenta = true;
            model.EstaRestableciendoPassword = false;

            return View(model);
        }

        public ActionResult RestablishPassword(string key)
        {
            if (string.IsNullOrEmpty(key))
                return RedirectToAction("Index", "Home");

            if (UsuarioActivo != null)
                new AuthService().Logout();

            var model = new ActivationModel();

            var sr = ServicioSeguridad.DecryptUserToActivate(key, false);
            if (!sr.Status)
                return RedirectToAction("Index", "Error");

            var user = (Entidades.Seguridad.Usuario)sr.Data;

            model.ValidationToken = key;
            model.UsuarioId = user.Id;
            model.Email = user.Email;
            model.EstaActivandoCuenta = false;
            model.EstaRestableciendoPassword = true;

            return View("CuentaActivate", model);
        }

        [HttpPost]
        public JsonResult Activate(ActivationModel model)
        {
            var sr = ServicioSeguridad.DecryptUserToActivate(model.ValidationToken, model.EstaActivandoCuenta);
            if (!sr.Status)
                return Json(sr);

            var user = (Entidades.Seguridad.Usuario)sr.Data;
            sr = new ServicioUsuario().ActivateUser(user, model.Password, model.EstaActivandoCuenta);
            if (!sr.Status)
                return Json(sr);

            new AuthService().Login(user, false);

            if (model.EstaRestableciendoPassword)
                sr.MessageText = string.Format(Translation.Account_PasswordRestalishmentSuccessful, user.NombreCompleto);
            else
                sr.MessageText = string.Format(Translation.Account_ActivationSuccessful, user.NombreCompleto);


            sr.RedirectUrl = Url.Action("Index", "Home");
            return Json(sr);
        }


        [HttpPost]
        public JsonResult SendRestablishPasswordEmail(string restablishmentEmail)
        {
            var sr = new ServicioUsuario().RestablishPassword(restablishmentEmail);
            if (!sr.Status)
                return Json(sr);

            sr.MessageText = Translation.Account_PasswordRestablishmentEmailSent;
            return Json(sr);
        }

        #endregion

        #region Others

        private void SetRememberMeCookie(string email, string password, bool rememberMe)
        {
            var cookieExists = HttpContext.Request.Cookies["RememberMe"] != null;

            if (!rememberMe && cookieExists)
            {
                var myCookie = new HttpCookie("RememberMe") { Expires = DateTime.Now.AddYears(-1) };
                Response.Cookies.Add(myCookie);
            }

            if (!rememberMe)
                return;

            var rememberMeCookie = new HttpCookie("RememberMe")
            {
                ["Email"] = CustomEncrypt.Encrypt(email),
                ["Password"] = CustomEncrypt.Encrypt(password),
                Expires = DateTime.MaxValue
            };

            if (cookieExists)
                Response.Cookies.Set(rememberMeCookie);
            else
                Response.Cookies.Add(rememberMeCookie);
        }

        public JsonResult DigitosVerificadores()
        {
            ServicioDigitos.RecalcularDigitosVerificadores();

            var sr = new ServiceResponse();
            sr.MessageText = Translation.Text_DVHSuccess;

            return Json(sr);
        }

        #endregion


        [Authorize]
        public ViewResult MyProfile()
        {
            var user = new MyProfileModel(UsuarioActivo.Id);
            return View(user);
        }

        [Authorize]
        [HttpPost]
        public JsonResult SaveMyProfile(MyProfileModel model)
        {
            var entity = model.ToEntity();
            var sr = new ServicioUsuario().SaveProfile(entity);

            if (!sr.Status)
                return Json(sr);

            sr.Reload = true;
            sr.MessageText = Translation.Account_ProfileUpdated;
            CacheManager.ObtenerUsuarioActivo(true);
            return Json(sr);
        }

    }
}