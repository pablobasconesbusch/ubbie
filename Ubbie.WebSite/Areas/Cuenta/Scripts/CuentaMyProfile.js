﻿
var AccountMyProfile = {

    c: {},

    init: function () {
        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {
        $.extend(b.c, b.controls);
    },

    initPlugins: function () {
    },

    initHandlers: function () {
        ASApp.Utils.ajaxForm('form');
    },
}

var b = AccountMyProfile;


$(function () {
    AccountMyProfile.init();
});
