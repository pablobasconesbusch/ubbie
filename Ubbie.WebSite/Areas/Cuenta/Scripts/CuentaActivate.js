﻿
var CuentaActivate = {

    controls: {
        frmActivate: '#frmActivate',
    },

    c: {},

    init: function () {
        var b = CuentaActivate;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {
        $.extend(b.c, b.controls);
    },

    initPlugins: function () {
    },

    initHandlers: function () {
        ASApp.Utils.ajaxForm(b.controls.frmActivate);
    },
}

var b = CuentaActivate;


$(function () {
    CuentaActivate.init();
});
