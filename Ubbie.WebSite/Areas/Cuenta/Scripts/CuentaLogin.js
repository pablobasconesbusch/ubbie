﻿
var AccountLogin = {

    controls: {
        frmLogin: '#frmLogin',
        frmRestablishPassword: '#frmRestablishPassword',

        Email: '#Email',
        RestablishmentEmail: '#RestablishmentEmail',
    },

    c: {},

    init: function () {
        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {

        $.extend(b.c, b.controls);

        b.c.Email = $(b.controls.Email);
        b.c.RestablishmentEmail = $(b.controls.RestablishmentEmail);
        b.c.frmRestablishPassword = $(b.controls.frmRestablishPassword);
    },

    initPlugins: function () {
    },

    initHandlers: function () {
        ASApp.Utils.ajaxForm(b.controls.frmLogin, {
            showSuccessMessage: false
        });

        ASApp.Utils.ajaxForm(b.controls.frmRestablishPassword);

        $(b.controls.frmLogin).find('button[type="submit"]').prop('disabled', false);
    },
}

var b = AccountLogin;
$(function () {
    AccountLogin.init();
});
