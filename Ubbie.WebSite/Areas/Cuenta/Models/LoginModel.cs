﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Recursos;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Cuenta.Models
{
    public class LoginModel
    {
        [Display(ResourceType = typeof(Translation), Name = "Label_Email"), ASRequired]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_EmailFormatNotValid")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Email"), ASRequired]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_EmailFormatNotValid")]
        public string RestablishmentEmail { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Password"), ASRequired]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_RememberMe")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }

        public bool IsLogged { get; set; }
        public bool RecalcularDigitos { get; set; }

    }
}