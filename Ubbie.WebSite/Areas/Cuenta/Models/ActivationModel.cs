﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Recursos;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Cuenta.Models
{
    public class ActivationModel
    {
        public int UsuarioId { get; set; }
        public string ValidationToken { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Email"), ASRequired]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_EmailFormatNotValid")]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Password"), ASRequired]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_ConfirmYourPassword"), ASRequired]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Account_PasswordsDoNotMatch")]
        public string ConfirmPassword { get; set; }

        public bool EstaActivandoCuenta { get; set; }
        public bool EstaRestableciendoPassword { get; set; }
    }
}