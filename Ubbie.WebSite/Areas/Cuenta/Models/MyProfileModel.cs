﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Usuario.Models;

namespace Ubbie.WebSite.Areas.Cuenta.Models
{
    public class MyProfileModel : MapperModel<UsuarioModel, Entidades.Seguridad.Usuario, int>
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_FirstName")]
        public string Nombre { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_LastName")]
        public string Apellido { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Email"), DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_EmailFormatNotValid))]
        public string Email { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_ProfilePicture")]
        public int? ImagenPerfilId { get; set; }


        [Display(ResourceType = typeof(Translation), Name = "Label_Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_ConfirmYourPassword")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Account_PasswordsDoNotMatch")]
        public string ConfirmPassword { get; set; }



        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(Entidades.Seguridad.Usuario entity)
        {
            Nombre = CustomEncrypt.Decrypt(entity.Nombre);
            Apellido = CustomEncrypt.Decrypt(entity.Apellido);
            Password = "";
        }

        protected override void ExtraMapToEntity(Entidades.Seguridad.Usuario entity)
        {
            entity.Nombre = CustomEncrypt.Encrypt(Nombre);
            entity.Apellido = CustomEncrypt.Encrypt(Apellido);
        }

        public MyProfileModel()
        {
        }


        public MyProfileModel(int id) : base(id, x => new ServicioUsuario().Get(x))
        {

        }

        public MyProfileModel(Entidades.Seguridad.Usuario entity) : base(entity)
        {

        }
    }
}
