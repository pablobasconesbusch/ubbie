﻿using System.Collections.Generic;
using System.Linq;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Areas.Usuario.Models;

namespace Ubbie.WebSite.Areas.Cuenta.Models
{
    public class FakeLoginModel
    {
        public List<UsuarioModel> Usuarios { get; set; }

        public FakeLoginModel()
        {
            Usuarios = new ServicioUsuario().GetFakeLogins().Select(x => new UsuarioModel(x)).ToList();
        }
    }
}
