﻿var ExpensaIndex = {

    controls: {
        frmLiquidar: '#frmLiquidar'
    },

    c: {},

    init: function () {
        var b = ExpensaIndex;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
    },

    initControls: function () {
        var b = ExpensaIndex;

        b.c = $.extend({}, b.controls);

        b.c.frmLiquidar = $(b.c.frmLiquidar);
    },

    initPlugins: function () {
        var b = ExpensaIndex;

    },

    initHandlers: function () {
        var b = ExpensaIndex;

        ASApp.Utils.ajaxForm(b.c.frmLiquidar, {
            onSuccess: function (sr) {
                ASApp.Ajax.partial({
                    url: URLs.Expensa.Info,
                    data: sr.Data,
                    partialWrapper: '#modalWrapper',
                    onSuccess: function () {
                        $('#modalExpensaInfo').modal();
                        $("#printBody").printThis();

                        $('.js-print').click(function() {
                            $("#printBody").printThis();
                        });
                    }
                });
            }
        });
    }

}


$(function () {
    ExpensaIndex.init();
});