﻿using System.Collections.Generic;
using Ubbie.Servicios.Negocio;

namespace Ubbie.WebSite.Areas.Expensa.Models
{
    public class ExpensaInfoModel
    {
        public int ImagenId { get; set; }

        public string DescripcionConsorcio { get; set; }

        public List<Entidades.Negocio.Gasto> GastosMensuales { get; set; }


        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string CodigoPostal { get; set; }
        public string Nro { get; set; }
        public string CUIT { get; set; }

        public ExpensaInfoModel()
        {
            Email = "ubbie@yopmail.com";
            Telefono = "+54 11 6793 4342";

            var consorcio = new ServicioConsorcio().Get(1);

            Direccion = consorcio.Direccion;
            Nro = consorcio.Numero;
            CUIT = consorcio.CUIT;
            CodigoPostal = consorcio.CodigoPostal.ToString();
        }
    }
}
