﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Expensa.Models;

namespace Ubbie.WebSite.Areas.Expensa
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.EXPENSAS)]
    [ViewsPath("~/Areas/Expensa/Views")]
    public class ExpensaController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult LiquidarExpensa()
        {
            var sr = new LiquidarExpensas().LiquidarExpensaDelMes();
            return Json(sr);
        }

        [HttpPost]
        public JsonResult Info(List<Entidades.Negocio.Gasto> gastos)
        {
            var model = new ExpensaInfoModel();
            model.GastosMensuales = gastos;

            return PartialView(model);
        }
    }
}
