﻿using System.Web.Mvc;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Balance.Models;

namespace Ubbie.WebSite.Areas.Balance
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.BALANCE)]
    [ViewsPath("~/Areas/Balance/Views")]
    public class BalanceController : BaseController
    {
        public ActionResult MisPagos()
        {
            var model = new MisPagosModel(UsuarioActivo.Id);
            return View("MisPagos", model);
        }

        public ActionResult PagarExpensas()
        {
            var model = new MisPagosModel(UsuarioActivo.Id);
            return View("PagarExpensas", model);
        }

        [HttpPost]
        public JsonResult EfectuarPago(int pagoExpensaId)
        {
            var sr = new ServicioExpensas().EfectuarPago(pagoExpensaId);
            return Json(sr);
        }
    }
}
