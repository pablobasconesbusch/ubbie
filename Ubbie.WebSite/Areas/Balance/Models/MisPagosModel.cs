﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;

namespace Ubbie.WebSite.Areas.Balance.Models
{
    public class MisPagosModel
    {
        public decimal SumaHaber { get; set; }
        public decimal SumaDebe { get; set; }

        public List<GrillaDTO> GrilaDto { get; set; } = new List<GrillaDTO>();

        public MisPagosModel() { }

        public MisPagosModel(int usuarioId)
        {
            var today = Configuration.CurrentLocalTime.Date;

            var servicio = new ServicioExpensas();

            var expensasSinPagar = servicio.GetExpensasSinPagar(usuarioId);
            var expensasPagas = servicio.GetExpensasPagas(usuarioId);

            SumaHaber = expensasSinPagar.Sum(x => x.Monto);
            SumaDebe = expensasPagas.Sum(x => x.Monto);


            GrilaDto = new List<GrillaDTO>();

            for (var i = 0; i < 6; i++)
            {
                var haber = "-";
                var haberId = 0;
                var debe = "-";
                var pagado = false;
                try
                {
                    haber = expensasSinPagar[i].Monto.ToString("C");
                    haberId = expensasSinPagar[i].Id;
                    pagado = expensasSinPagar[i].Pagado;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                try
                {
                    debe = expensasPagas[i].Monto.ToString("C");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                var grillaDto = new GrillaDTO
                {
                    Mes = today.AddMonths(-i).ToString("MMMM"),
                    Haber = haber,
                    HaberId = haberId,
                    Debe = debe,
                    Pagado = pagado
                };

                GrilaDto.Add(grillaDto);
            }
        }
    }

    public class GrillaDTO
    {
        public string Mes { get; set; }
        public int HaberId { get; set; }
        public string Haber { get; set; }
        public string Debe { get; set; }

        public decimal TotalHaber { get; set; }

        public bool Pagado { get; set; }
    }
}
