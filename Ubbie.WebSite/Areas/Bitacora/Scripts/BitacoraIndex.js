﻿var Bitacora = {
    grid: {},

    controls: {
        grid: '#setupGrid',

        dataDateFilters: '[data-date-filters]',
        fechaDesde: '#FechaDesde',
        fechaHasta: '#FechaHasta',
        userId: '[data-userid]',

        btnSearch: '[data-daterange-search]',
        xmlExport: '[data-export-xml]'
    },

    c: {},

    init: function () {
        b.initControls();
        b.initPlugins();
        b.initHandlers();

        b.initGrid();
    },

    initControls: function () {
        b.c = $.extend({}, b.controls);

        b.c.dataDateFilters = $(b.c.dataDateFilters);
        b.c.fechaDesde = $(b.c.fechaDesde);
        b.c.fechaHasta = $(b.c.fechaHasta);
        b.c.userId = $(b.c.userId);
        
        b.c.btnSearch = $(b.c.btnSearch);
        b.c.xmlExport = $(b.c.xmlExport);
    },

    initPlugins: function () {

        b.c.dataDateFilters.datepicker({
            inputs: [b.c.fechaDesde, b.c.fechaHasta],
            format: ASApp.settings.DateFormat,
            autoclose: true,
            language: ASApp.settings.LanguageTwoLetter
        });

    },

    initHandlers: function () {

        b.c.userId.change(function () {
            b.grid.ajax.reload();
        });

        b.c.btnSearch.on('click', function () {
            b.grid.ajax.reload();
        });

        b.c.xmlExport.on('click', function() {

            var fechaDesde = b.c.fechaDesde.val();
            var fechaHasta = b.c.fechaHasta.val();
            document.location = URLs.Bitacora.XMLExport + '?fechaDesde=' + fechaDesde + '&fechaHasta=' + fechaHasta;
        });

    },

    initGrid: function () {

        b.grid = ASApp.Plugins.DataTables.initGrid(b.controls.grid, {
            url: URLs.Bitacora.List,
            params: function () {
                return {
                    UsuarioId: b.c.userId.val(),
                    FechaDesde: b.c.fechaDesde.val(),
                    FechaHasta: b.c.fechaHasta.val(),
                    IsHistory: false
                };
            },
            columns: [
                { title: '#', data: 'Id' },
                { title: fechaLabel, data: 'DateTime' },
                { title: usuarioLabel, data: 'Email', orderable: false },
                { title: detalleLabel, data: 'Detalle', orderable: false },
                { title: criticidadLabel, data: 'Criticidad' }
            ],
            order: [[0, "desc"]],
            isSetupGrid: false
        });
    }
}

var b = Bitacora;

$(function () {
    Bitacora.init();
});