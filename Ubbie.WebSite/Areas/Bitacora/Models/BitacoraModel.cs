﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Bitacora.Models
{
    public class BitacoraModel : MapperModel<BitacoraModel, Entidades.Seguridad.Bitacora, int>
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_User")]
        public int? UsuarioId { get; set; }

        public string Detalle { get; set; }
        public string IP { get; set; }

        public SelectList Usuarios { get; set; }

        public override void InitializateData()
        {
            Usuarios = new ServicioUsuario().GetList().ToSelectList(x => x.Id, x => x.Email);
        }

        protected override void ExtraMapFromEntity(Entidades.Seguridad.Bitacora entity)
        {
            throw new NotImplementedException();
        }

        protected override void ExtraMapToEntity(Entidades.Seguridad.Bitacora entity)
        {
            throw new NotImplementedException();
        }
    }
}
