﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Linq;
using System.Xml.Serialization;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Bitacora.Models;

namespace Ubbie.WebSite.Areas.Bitacora
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.BITACORA)]
    [ViewsPath("~/Areas/Bitacora/Views")]
    public class BitacoraController : BaseController
    {
        public ActionResult Index()
        {
            var model = new BitacoraModel();
            model.InitializateData();
            return View(model);
        }

        public ActionResult HistoricoIndex()
        {
            var model = new BitacoraModel();
            model.InitializateData();
            return View(model);
        }

        public ActionResult HistoricoReadXML()
        {
            var model = new BitacoraModel();
            model.InitializateData();

            XDocument xmlDoc = XDocument.Load("~/Content/reportes/historico-bitacora.xml");
            var list = xmlDoc.Root.Elements("id")
                .Select(element => element.Value)
                .ToList();

            return View(model);
        }

        [HttpPost]
        public JsonResult List(BitacoraParameters p)
        {
            var service = new ServicioBitacora();
            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Seguridad.Bitacora>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    DateTime = x.DateTime.ToUserDateTime().ToString("dd/MM/yyyy HH:mm", new CultureInfo("es-AR")),
                    Email = x.Usuario?.Email ?? "-",
                    Detalle = CustomEncrypt.Decrypt(x.Detalle),
                    x.Criticidad
                });

            return Json(sr);
        }

        [HttpGet]
        public ActionResult XMLExport(string fechaDesde, string fechaHasta)
        {
            var fDesde = DateTime.Parse(fechaDesde);
            var fHasta = DateTime.Parse(fechaHasta);

            var bitacora = new ServicioBitacora().GetListExport(fDesde, fHasta);
            bitacora.ForEach(x =>
            {
                x.Detalle = CustomEncrypt.Decrypt(x.Detalle);
                x.Usuario.Nombre = CustomEncrypt.Decrypt(x.Usuario.Nombre);
                x.Usuario.Apellido = CustomEncrypt.Decrypt(x.Usuario.Apellido);
            });

            var serializer = new XmlSerializer(typeof(List<Ubbie.Entidades.Seguridad.Bitacora>));

            var fileStream = new StreamWriter(@"C:\UbbieXML\reporteBitacora.xml");
            serializer.Serialize(fileStream, bitacora);

            fileStream.Close();

            byte[] fileBytes = System.IO.File.ReadAllBytes(@"C:\UbbieXML\reporteBitacora.xml");
            var fileName = "ReporteBitacora.xml";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}