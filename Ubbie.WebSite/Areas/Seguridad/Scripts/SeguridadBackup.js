﻿
$(function () {

    $('#frmBackup').submit(function (e) {
        e.preventDefault();
        var $this = $(this);

        ASApp.Notify.confirmDelete({
            title: confirmBackup
        }, function() {
            var data = ASApp.Utils.formToObject($this);
            ASApp.Ajax.postForm($this, {
                data: data
            });
        });

    });

});
