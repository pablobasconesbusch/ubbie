﻿
$(function () {


    $('#frmRestore').submit(function (e) {
        e.preventDefault();
        var $this = $(this);

        ASApp.Notify.confirmDelete({
            title: confirmRestore
        }, function () {
            var data = ASApp.Utils.formToObject($this);
            ASApp.Ajax.postForm($this, {
                data: data
            });
        });

    });

});