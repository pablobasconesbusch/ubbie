﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autenticacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Seguridad
{
    [ViewsPath("~/Areas/Seguridad/Views")]
    public class SeguridadController : BaseController
    {
        public ActionResult Backup()
        {
            return View();
        }

        [HttpPost, ActionName("Backup")]
        public JsonResult BackupPost()
        {
            var servicio = new ServicioBD();

            var carpetaBackup = ConfigurationManager.AppSettings["CarpetaBackup"];
            var archivoBackup = $"{carpetaBackup}ubbie-{DateTime.Now.ToString("yyyy-MM-dd HHmm")}.bak";

            var sr = servicio.Backup(archivoBackup);

            if (!sr.Status)
                return Json(sr);

            sr.MessageTitle = "El backup se ha realizado correctamente";
            sr.MessageText = $"El archivo se encuentra en {archivoBackup}";

            return Json(sr);
        }

        public ActionResult Restore()
        {
            return View();
        }

        [HttpPost, ActionName("Restore")]
        public JsonResult RestorePost(string files)
        {
            var servicio = new ServicioBD();

            var sr = servicio.Restore(files);

            if (!sr.Status)
                return Json(sr);

            sr.MessageTitle = "El restore se ha realizado correctamente";
            sr.MessageText = "";
            new AuthService().Logout();
            sr.RedirectUrl = Url.Action("Login", "Cuenta");

            return Json(sr);
        }
    }
}
