﻿using System;
using System.Linq;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.Servicios.Seguridad;

namespace Ubbie.WebSite.Areas.Dashboard.Models
{
    public class DashboardModel
    {
        public int TotalUsuarios { get; set; }

        public decimal GastosMensuales { get; set; }

        public int TotalReclamos { get; set; }

        public DashboardModel()
        {
            TotalUsuarios = new ServicioUsuario().GetList().Count;

            var today = Configuration.CurrentLocalTime;
            var primerDiaDelMes = new DateTime(today.Year, today.Month, 1);
            var ultimoDiaDelMes = primerDiaDelMes.AddMonths(1);
            var gastosMensuales = new ServicioGasto().GastosMensuales(primerDiaDelMes, ultimoDiaDelMes);

            GastosMensuales = gastosMensuales.Sum(x => x.Monto);
            TotalReclamos = new ServicioReclamo().GetList().Count(x => x.Pendiente);
        }
    }
}
