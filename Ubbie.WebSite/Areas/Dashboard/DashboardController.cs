﻿using System.Web.Mvc;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Dashboard.Models;

namespace Ubbie.WebSite.Areas.Dashboard
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.DASHBOARD)]
    [ViewsPath("~/Areas/Dashboard/Views")]
    public class DashboardController : BaseController
    {

        public ViewResult Index()
        {
            var model = new DashboardModel();
            return View(model);
        }

    }
}
