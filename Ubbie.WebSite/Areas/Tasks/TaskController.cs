﻿using System.Web.Mvc;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.MVC;

namespace Ubbie.WebSite.Areas.Tasks
{
    public class TaskController : BaseController
    {
        [HttpGet]
        public string Run()
        {
            ServicioSeguridad.BitacoraAXML();
            return "Proceso Ejecutado";
        }


        //[HttpGet]
        //public string RunUsuario()
        //{
        //    new ServicioUsuario().Encrypt();
        //    return "Proceso Ejecutado";
        //}
    }
}
