﻿using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Negocio;

namespace Ubbie.WebSite.Areas.Notificacion.Models
{
    public class NotificacionModel : MapperModel<NotificacionModel, UsuarioNotificacion, int>
    {
        public int Id { get; set; }

        public string UserFullName { get; set; }
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string Fecha { get; set; }

        public override void InitializateData()
        {

        }

        protected override void ExtraMapFromEntity(UsuarioNotificacion entity)
        {
            UserFullName = $"{CustomEncrypt.Decrypt(entity.Usuario.Nombre)} {CustomEncrypt.Decrypt(entity.Usuario.Apellido)}";
            Fecha = entity.FechaEnvio.ToString("d");
        }

        protected override void ExtraMapToEntity(UsuarioNotificacion entity)
        {

        }

        public NotificacionModel() { }



        public NotificacionModel(int id) : base(id, x => new ServicioNotificacion().Get(x))
        {

        }

        public NotificacionModel(UsuarioNotificacion entity) : base(entity)
        {

        }
    }
}
