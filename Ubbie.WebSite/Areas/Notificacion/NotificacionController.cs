﻿using System.Web.Mvc;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Notificacion.Models;

namespace Ubbie.WebSite.Areas.Notificacion
{
    [Authorize]
    [ViewsPath("~/Areas/Notificacion/Views")]
    public class NotificacionController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewNotification(int entityId)
        {
            var model = new NotificacionModel(entityId);
            return PartialView("~/Shared/Views/NotificationModal.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetNotification(int? entityId)
        {
            if (entityId.HasValue)
            {
                var service = new ServicioNotificacion();
                service.ViewNotificacion(entityId.Value);
            }

            return PartialView("~/Shared/Views/NotificationDropdown.cshtml");
        }

        [HttpPost]
        public JsonResult ViewAll()
        {
            var service = new ServicioNotificacion();
            var sr = service.ViewAllNotificaciones(AppInfo.UsuarioActivo.Id);
            return Json(sr);
        }
    }
}
