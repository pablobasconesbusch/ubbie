﻿var ReclamoIndex = {
    controls: {
    },

    c: {},

    init: function () {
        var b = ReclamoIndex;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
        b.initGrid();
    },

    initControls: function () {
        var b = ReclamoIndex;

        b.c = $.extend({}, b.controls);

    },

    initPlugins: function () {
        var b = ReclamoIndex;

    },

    initHandlers: function () {
        var b = ReclamoIndex;

    },

    initGrid: function () {
        var b = ReclamoIndex;

        ASApp.GridView.init({
            initGrid: function (grid) {
                ASApp.Plugins.DataTables.initGrid(grid, {
                    url: URLs.Reclamo.List,
                    params: function () {
                        return {
                            IsAdminView: false
                        };
                    },
                    columns: [
                        { title: 'Fecha', data: 'FechaCreacion', width: '10%' },
                        { title: '#', data: 'Numero', width: '10%' },
                        {
                            title: 'Pendiente', data: 'Pendiente', className: 'dt-center', width: '5%', render(data) {
                                return ASApp.Utils.renderStateCheck(data);
                            }
                        },
                        { title: 'Solución', data: 'Solucion' }
                    ],
                    onSelect: function (item) {
                        if (item) {
                            ASApp.InfoView.entity = {
                                entityId: item.Id
                            }
                            ASApp.InfoView.getView(ASApp.InfoView.mainTab);
                        }
                    },
                    order: [[0, "desc"]]
                });
            },
            onInit: function () {

            }
        });

        ASApp.InfoView.init({

            mainTab: {
                onInit: function (tab) {

                },
                getSaveModel: function (data) {

                },
                getDeleteModel: function () {
                    return {
                        url: URLs.Reclamo.Delete
                    }
                },
                customFormValidation: function () {
                    return true;
                }
            },
            tabs: [

            ]
        });
    }

}

$(function () {
    ReclamoIndex.init();
});
