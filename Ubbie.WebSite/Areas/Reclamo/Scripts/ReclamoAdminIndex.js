﻿var ReclamoAdminIndex = {
    controls: {
        grid: '#setupGrid',
        btnSolved: '[data-solved]',
        btnUnsolved: '[data-unsolved]',
        btnDetails: '[data-details]',
        modalWrapper: '#modalWrapper',
        form: '#frmFinalizarReclamo',
        reclamoAdminModal: '#reclamoAdminModal',
        reclamoDetailsModal: '#reclamoDetailsModal'
    },

    c: {},

    init: function () {
        var b = ReclamoAdminIndex;

        b.initControls();
        b.initPlugins();
        b.initHandlers();
        b.initGrid();

    },

    initControls: function () {
        var b = ReclamoAdminIndex;

        b.c = $.extend({}, b.controls);

        b.c.grid = $(b.c.grid);
        b.c.btnSolved = $(b.c.btnSolved);
        b.c.btnUnsolved = $(b.c.btnUnsolved);
        b.c.reclamoAdminModal = $(b.c.reclamoAdminModal);
    },

    initPlugins: function () {
        var b = ReclamoAdminIndex;

    },

    initHandlers: function () {
        var b = ReclamoAdminIndex;

    },

    initGrid: function () {
        var b = ReclamoAdminIndex;

        ASApp.GridView.init({
            initGrid: function (grid) {
                ASApp.Plugins.DataTables.initGrid(grid, {
                    url: URLs.Reclamo.List,
                    params: function () {
                        return {
                            IsAdminView: true
                        };
                    },
                    columns: [
                        { title: 'Fecha', data: 'FechaCreacion', width: '10%' },
                        { title: 'Usuario', data: 'UsuarioNombreCompleto', width: '25%' },
                        {
                            title: 'Pendiente', data: 'Pendiente', className: 'dt-center', width: '5%', render(data) {
                                return ASApp.Utils.renderStateCheck(data);
                            }
                        },
                        {
                            title: 'Solucionado', data: 'Solucionado', className: 'dt-center', width: '5%', render(data) {
                                return ASApp.Utils.renderStateCheck(data);
                            }
                        },
                        { title: 'Solución', data: 'Solucion' },
                        {
                            title: 'Acciones', data: null, orderable: false, className: 'dt-center', width: '10%', render(data) {
                                var ret = '';

                                if (data.Pendiente && IsAdmin) {
                                    ret += '<div class="button-group">';
                                    ret += '<button type="button" class="btn btn-success btn-xs" data-solved data-reclamo-id="' + data.Id + '" title="Resuelto" data-toggle="tooltip"><i class="fa fa-check"></i></button>';
                                    ret += '<button type="button" class="btn btn-danger btn-xs" data-unsolved data-reclamo-id="' + data.Id + '" title="No resuelto" data-toggle="tooltip"><i class="fa fa-times"></i></button>';
                                }

                                ret += '<button type="button" class="btn btn-info btn-xs" data-details data-reclamo-id="' + data.Id + '" title="Ver detalles" data-toggle="tooltip"><i class="fa fa-eye"></i></button>';

                                ret += '</div>';
                                return ret;
                            }
                        }
                    ],
                    isSetupGrid: false,
                    initComplete: $.noop,
                    onSelect: $.noop,
                    drawCallback: function () {
                        $('[data-toggle="tooltip"]').tooltip('destroy');
                        $('[data-toggle="tooltip"]').tooltip({ container: 'body', trigger: 'hover' });
                    },
                    order: [[0, "desc"]]
                });
            },
            onInit: function () {
                $('body').on('click', b.controls.btnSolved, function () {
                    var $this = $(this);
                    b.initModal($this.data('reclamo-id'), true);
                });

                $('body').on('click', b.controls.btnUnsolved, function () {
                    var $this = $(this);

                    var model = {
                        Id: $this.data('reclamo-id'),
                        IsSolved: false
                    }

                    ASApp.Notify.alert({
                        title: '¿Seguro desea marcar el reclamo como "No resuelto"?'
                    }, function () {
                        b.finishReclamo(model);
                    });

                });

                $('body').on('click', b.controls.btnDetails, function () {
                    var $this = $(this);
                    b.reclamoDetails($this.data('reclamo-id'));
                });
            }
        });
    },

    initModal: function (reclamoId) {
        var b = ReclamoAdminIndex;

        ASApp.Ajax.partial({
            url: URLs.Reclamo.AdminInfo,
            data: { entityId: reclamoId },
            partialWrapper: b.controls.modalWrapper,
            scrollToPartial: false,
            onSuccess: function () {
                $(b.controls.reclamoAdminModal).modal('show');
                b.initModalHandlers();
            }
        });
    },

    initModalHandlers: function () {
        var b = ReclamoAdminIndex;
        ASApp.Plugins.JqueryValidationBootstrap.reAttach($(b.controls.form));

        $(b.controls.form).on('submit', function (e) {
            e.preventDefault();
            var $this = $(this);
            if ($this.valid()) {
                var data = ASApp.Utils.formToObject(b.controls.form);
                data.Solucionado = true;

                b.finishReclamo(data);
            }
        });

    },

    finishReclamo: function (data) {
        var b = ReclamoAdminIndex;

        ASApp.Ajax.post({
            url: URLs.Reclamo.FinalizarReclamo,
            data: { model: data },
            onSuccess: function () {
                $(b.controls.reclamoAdminModal).modal('hide');
                b.reloadGrid();
            }
        });
    },

    reloadGrid: function () {
        var b = ReclamoAdminIndex;

        b.c.grid.DataTable().ajax.reload();
    },

    reclamoDetails: function (reclamoId) {
        var b = ReclamoAdminIndex;

        ASApp.Ajax.partial({
            url: URLs.Reclamo.Details,
            data: { entityId: reclamoId },
            partialWrapper: b.controls.modalWrapper,
            scrollToPartial: false,
            onSuccess: function () {
                $(b.controls.reclamoDetailsModal).modal('show');
                $(b.controls.reclamoDetailsModal).on('shown.bs.modal', function () {
                    ASApp.Utils.resizeTextAreas();
                });
            }
        });
    }

}

$(function () {
    ReclamoAdminIndex.init();
});
