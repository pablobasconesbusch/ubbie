﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Reclamo.Models;

namespace Ubbie.WebSite.Areas.Reclamo
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.RECLAMO)]
    [ViewsPath("~/Areas/Reclamo/Views")]
    public class ReclamoController : BaseController
    {
        // GET: Reclamo/Reclamo
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Info(int entityId)
        {
            var model = entityId == 0 ? new ReclamoModel() : new ReclamoModel(entityId);
            model.InitializateData();

            return PartialView(model);
        }


        public ActionResult AdminIndex()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminInfo(int entityId)
        {
            var model = new ReclamoAdminModel { Id = entityId };
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Details(int entityId)
        {
            var model = entityId == 0 ? new ReclamoDetalleModel() : new ReclamoDetalleModel(entityId);
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            var service = new ServicioReclamo();

            if (p.SortField == "UsuarioNombreCompleto") p.SortField = "Usuario.Nombre";

            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Negocio.Reclamo>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    UsuarioNombreCompleto = $"{CustomEncrypt.Decrypt(x.Usuario.Nombre)} {CustomEncrypt.Decrypt(x.Usuario.Apellido)}",
                    FechaCreacion = x.FechaCreacion.ToString("d"),
                    x.Numero,
                    x.Pendiente,
                    x.Solucionado,
                    x.Solucion
                });

            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.RECLAMO, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.RECLAMO, Constants.PatentCode.EDIT)]
        public JsonResult Save(ReclamoModel model)
        {
            var entity = model.ToEntity();
            var sr = new ServicioReclamo().SaveEntity(entity);

            if (sr.Status)
                new GenerarNotificacion().NuevoReclamo((List<Entidades.Seguridad.Usuario>)sr.Data, sr.ReturnCode);

            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.RECLAMO, Constants.PatentCode.DELETE)]
        public JsonResult Delete(ReclamoModel model)
        {
            var entity = model.ToEntity();
            var sr = new ServicioReclamo().DeleteEntity(entity);
            return Json(sr);
        }

        [HttpPost]
        public JsonResult FinalizarReclamo(ReclamoAdminModel model)
        {
            var entity = new Entidades.Negocio.Reclamo
            {
                Id = model.Id,
                Solucion = model.Solucion,
                Solucionado = model.Solucionado
            };

            var sr = new ServicioReclamo().FinalizarReclamo(entity);

            if (sr.Status)
                new GenerarNotificacion().ReclamoFinalizado((Entidades.Seguridad.Usuario)sr.Data, sr.ReturnCode);

            return Json(sr);
        }

    }
}