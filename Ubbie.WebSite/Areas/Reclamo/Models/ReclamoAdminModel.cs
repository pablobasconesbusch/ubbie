﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Recursos;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Reclamo.Models
{
    public class ReclamoAdminModel
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Solution")]
        public string Solucion { get; set; }

        public bool Solucionado { get; set; }
    }
}
