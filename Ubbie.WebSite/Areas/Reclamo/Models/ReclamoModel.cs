﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Code;

namespace Ubbie.WebSite.Areas.Reclamo.Models
{
    public class ReclamoModel : MapperModel<ReclamoModel, Entidades.Negocio.Reclamo, int>
    {
        public int Id { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Number")]
        public string Numero { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Comments")]
        public string Comentario { get; set; }

        public bool Pendiente { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Solution")]
        public string Solucion { get; set; }

        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Translation.HeaderTitle_ComplaintNew;
            Numero = new ServicioReclamo().ObtenerNroReclamo();
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Reclamo entity)
        {
            HeaderTitle = string.Format(Translation.HeaderTitle_Complaint, entity.Numero);
        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Reclamo entity)
        {
            entity.UsuarioId = AppInfo.UsuarioActivo.Id;
        }

        public ReclamoModel()
        {

        }

        public ReclamoModel(int id) : base(id, x => new ServicioReclamo().Get(x))
        {

        }

        public ReclamoModel(Entidades.Negocio.Reclamo entity) : base(entity)
        {

        }
    }
}
