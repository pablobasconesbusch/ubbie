﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Reclamo.Models
{
    public class ReclamoDetalleModel : MapperModel<ReclamoDetalleModel, Entidades.Negocio.Reclamo, int>
    {
        public int Id { get; set; }

        public string Numero { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_User")]
        public string UsuarioNombreCompleto { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Solution")]
        public string Solucion { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_Comments")]
        public string Comentario { get; set; }

        public bool Pendiente { get; set; }
        public bool Solucionado { get; set; }

        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Reclamo entity)
        {
            UsuarioNombreCompleto = $"{CustomEncrypt.Decrypt(entity.Usuario.Nombre)} {CustomEncrypt.Decrypt(entity.Usuario.Apellido)}";
        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Reclamo entity)
        {

        }

        public ReclamoDetalleModel()
        {

        }

        public ReclamoDetalleModel(int id) : base(id, x => new ServicioReclamo().Get(x))
        {

        }

        public ReclamoDetalleModel(Entidades.Negocio.Reclamo entity) : base(entity)
        {

        }
    }
}
