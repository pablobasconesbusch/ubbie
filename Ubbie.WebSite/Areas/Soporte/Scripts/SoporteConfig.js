﻿
var SoporteConfig = {

    grid: {},

    controls: {
        tab: '#tabConfig',
        form: '[data-form]',

        btnRefreshConfig: '[data-btn-refresh-config]'
    },

    c: {},

    init: function () {
        sc.initControls();
        sc.initPlugins();
        sc.initHandlers();
    },

    initControls: function () {
        $.extend(sc.c, sc.controls);

        sc.c.tab = $(sc.controls.tab);
        sc.c.btnRefreshConfig = $(sc.controls.btnRefreshConfig);
    },

    initPlugins: function () {
    },

    initHandlers: function () {
        ASApp.Utils.ajaxForm(sc.c.tab.find(sc.controls.form));

        sc.c.btnRefreshConfig.on('click', function () {
            ASApp.Ajax.post({
                url: URLs.Soporte.RefreshConfig
            });
        });

        $('input[type="checkbox"]').on('change', function () {
            var $this = $(this);
            var parent = $this.parent();
            var inputHidden = parent.find('input[type="hidden"]');
            var isChecked = $this.is(':checked');

            inputHidden.val(isChecked ? "True" : "False");
        });
    }
}

var sc = SoporteConfig;
$(function () {
    SoporteConfig.init();
});
