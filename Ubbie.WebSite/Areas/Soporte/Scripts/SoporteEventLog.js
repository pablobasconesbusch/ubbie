﻿
var SoporteEventLog = {
    grid: {},

    controls: {
        tab: '#tabEventLog',
        grid: '[data-grid]',

        btnViewEventLogDetails: '[data-btn-view-event-log-details]',

        dataDateFilters: '[data-date-filters]',
        FilterByDateFrom: '#FilterByDateFrom',
        FilterByDateTo: '#FilterByDateTo',
        FilterByLogLevel: '#FilterByLogLevel',
        FilterByUsuarioIds: '#FilterByUsuarioIds',

        btnRefreshGrid: '[data-btn-refresh-grid]'
    },

    c: {},

    init: function () {

        sel.initControls();
        sel.initPlugins();
        sel.initHandlers();

        sel.initGrid();
    },

    initControls: function () {
        $.extend(sel.c, sel.controls);

        sel.c.tab = $(sel.controls.tab);

        sel.c.dataDateFilters = sel.c.tab.find(sel.controls.dataDateFilters);
        sel.c.FilterByDateFrom = sel.c.tab.find(sel.controls.FilterByDateFrom);
        sel.c.FilterByDateTo = sel.c.tab.find(sel.controls.FilterByDateTo);
        sel.c.FilterByLogLevel = sel.c.tab.find(sel.controls.FilterByLogLevel);
        sel.c.FilterByUsuarioIds = sel.c.tab.find(sel.controls.FilterByUsuarioIds);

        sel.c.btnRefreshGrid = sel.c.tab.find(sel.controls.btnRefreshGrid);
    },

    initPlugins: function () {

        sel.c.dataDateFilters.datepicker({
            inputs: [sel.c.FilterByDateFrom, sel.c.FilterByDateTo],
            format: ASApp.settings.dateFormat,
            autoclose: true,
            language: ASApp.settings.languageTwoLetter
        });

    },

    initHandlers: function () {
        ASApp.Utils.ajaxForm(sel.controls.frmConfig);

        $('body').on('click', sel.controls.btnViewEventLogDetails, function () {
            var $this = $(this);
            var eventLogId = $this.data('event-log-id');
            sel.getEventLogDetailView(eventLogId);
        });

        [sel.c.FilterByDateFrom, sel.c.FilterByDateTo, sel.c.FilterByLogLevel, sel.c.FilterByUsuarioIds].forEach(function (e) {
            e.on('change', function () {
                sel.grid.ajax.reload();
            });
        });

        sel.c.btnRefreshGrid.on('click', function () {
            sel.grid.ajax.reload();
        });

    },


    initGrid: function () {
        sel.grid = ASApp.Plugins.DataTables.initGrid(sel.c.tab.find(sel.controls.grid), {
            url: URLs.Soporte.EventLogList,
            params: function () {
                return {
                    FilterByDateFrom: sel.c.FilterByDateFrom.val(),
                    FilterByDateTo: sel.c.FilterByDateTo.val(),
                    FilterByLogLevel: sel.c.FilterByLogLevel.val(),
                    FilterByUsuarioIds: sel.c.FilterByUsuarioIds.val()
                };
            },
            columnDefs: [{
                className: 'dt-center',
                targets: '_all'
            }],
            columns: [
                { title: 'Date', width: '5%', data: 'Date' },
                { title: 'LogLevel', width: '5%', data: 'LogLevel' },
                { title: 'EventCode', width: '5%', data: 'EventCode' },
                {
                    title: 'ServerException', width: '20%', data: 'ServerException', render: function (data, e, r) {

                        return '<span data-bs-tooltip data-style="max-width: 500px; word-wrap: break-word;" data-placement="left" data-container="body" title="' + data + '">' + data.substring(0, 100) + '</span>';
                    }
                },
                { title: 'Usuario', width: '10%', data: 'Usuario' },
                { title: 'Path', width: '10%', data: 'Path' },
                {
                    title: 'Actions', width: '5%', data: null, render: function (data) {
                        return '<button type="button" class="btn btn-xs btn-info" title="Ver detalles" data-btn-view-event-log-details data-event-log-id=' + data.Id + '><i class="fa fa-eye"></i></button>';
                    }
                },
            ],
            order: [[0, "desc"]],
            isSetupGrid: false,
            drawCallback: function () {
                ASApp.Plugins.BootstrapTooltip.init();
            }
        });
    },

    getEventLogDetailView: function (eventLogId) {
        ASApp.Ajax.popUp({
            url: URLs.Soporte.EventLogInfo,
            data: { entityId: eventLogId }
        });
    }
}

var sel = SoporteEventLog;
$(function () {
    SoporteEventLog.init();
});
