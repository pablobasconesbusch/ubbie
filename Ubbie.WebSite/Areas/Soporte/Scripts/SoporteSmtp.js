﻿
var SoporteSmtp = {

    grid: {},


    controls: {
        tab: '#tabSmtp',
        grid: '[data-grid]',

        btnViewEmailDetails: '[data-btn-view-email-details]',
        btnResendEmail: '[data-btn-resend-email]',


        dataDateFilters: '[data-date-filters]',
        FilterByDateFrom: '#FilterByDateFrom',
        FilterByDateTo: '#FilterByDateTo',
        EmailStatusCode: '#EmailStatusCode',

        form: '[data-form]',

        btnRefreshGrid: '[data-btn-refresh-grid]'
    },

    c: {},

    init: function () {
        ss.initControls();
        ss.initPlugins();
        ss.initHandlers();

        ss.initGrid();
    },

    initControls: function () {
        $.extend(ss.c, ss.controls);

        ss.c.tab = $(ss.controls.tab);

        ss.c.dataDateFilters = ss.c.tab.find(ss.controls.dataDateFilters);
        ss.c.FilterByDateFrom = ss.c.tab.find(ss.controls.FilterByDateFrom);
        ss.c.FilterByDateTo = ss.c.tab.find(ss.controls.FilterByDateTo);
        ss.c.EmailStatusCode = ss.c.tab.find(ss.controls.EmailStatusCode);

        ss.c.btnRefreshGrid = ss.c.tab.find(ss.controls.btnRefreshGrid);
    },

    initPlugins: function () {

        ss.c.dataDateFilters.datepicker({
            inputs: [ss.c.FilterByDateFrom, ss.c.FilterByDateTo],
            format: ASApp.settings.dateFormat,
            autoclose: true,
            language: ASApp.settings.languageTwoLetter
        });

    },

    initHandlers: function () {

        ASApp.Utils.ajaxForm(ss.c.tab.find(ss.controls.form), {
            onSuccess: function () {
                ss.grid.ajax.reload();
            }
        });

        [ss.c.FilterByDateFrom, ss.c.FilterByDateTo, ss.c.EmailStatusCode].forEach(function (e) {
            e.on('change', function () {
                ss.grid.ajax.reload();
            });
        });

        ss.c.btnRefreshGrid.on('click', function () {
            ss.grid.ajax.reload();
        });

        $('body').on('click', ss.controls.btnViewEmailDetails, function () {
            var $this = $(this);
            var emailId = $this.data('email-id');
            ss.getEmailDetail(emailId);
        });

        $('body').on('click', ss.controls.btnResendEmail, function () {
            var $this = $(this);
            var emailId = $this.data('email-id');
            ss.resendEmail(emailId);
        });

    },

    initGrid: function () {
        ss.grid = ASApp.Plugins.DataTables.initGrid(ss.c.tab.find(sel.controls.grid), {
            url: URLs.Soporte.EmailList,
            params: function () {
                return {
                    FilterByDateFrom: ss.c.FilterByDateFrom.val(),
                    FilterByDateTo: ss.c.FilterByDateTo.val(),
                    EmailStatusCode: ss.c.EmailStatusCode.val()
                };
            },
            columnDefs: [{
                className: 'dt-center',
                targets: '_all'
            }],
            columns: [
                { title: 'Date', width: '20%', data: 'Date' },
                { title: 'Subject', width: '30%', data: 'Subject' },
                { title: 'Recipients', data: 'Recipients' },
                { title: 'StatusCode', data: 'EmailStatusCode' },
                {
                    title: 'Actions', data: null, render: function (data) {

                        var ret = '';

                        ret += '<div class="btn-group">';
                        ret += '<button type="button" class="btn btn-xs btn-warning" data-bs-tooltip title="Resend" data-btn-resend-email data-email-id=' + data.Id + '><i class="fa fa-repeat"></i></button>';
                        ret += '<button type="button" class="btn btn-xs btn-info" data-bs-tooltip title="Details" data-btn-view-email-details data-email-id=' + data.Id + '><i class="fa fa-eye"></i></button>';
                        ret += '</div>';

                        return ret;
                    }
                },
            ],
            order: [[0, "desc"]],
            isSetupGrid: false,
            drawCallback: function () {
                ASApp.Plugins.BootstrapTooltip.init();
            }
        });
    },

    getEmailDetail: function (emailId) {
        ASApp.Ajax.popUp({
            url: URLs.Soporte.EmailInfo,
            data: { entityId: emailId }
        });
    },

    resendEmail: function (emailId) {
        ASApp.Ajax.post({
            url: URLs.Soporte.ResendEmail,
            data: { emailId: emailId },
            onSuccess: function() {
                ss.grid.ajax.reload();
            }
        });
    },
}

var ss = SoporteSmtp;
$(function () {
    SoporteSmtp.init();
});
