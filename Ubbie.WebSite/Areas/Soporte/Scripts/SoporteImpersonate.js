﻿
var SoporteImpersonate = {

    grid: {},


    controls: {
        tab: '#tabImpersonate',
        grid: '[data-grid]',

        btnViewUsuarios: '[data-btn-view-users]',
        btnRefreshGrid: '[data-btn-refresh-grid]',

        ImpersonateQueryString: '#ImpersonateQueryString'
    },

    c: {},

    init: function () {
        si.initControls();
        si.initPlugins();
        si.initHandlers();

    },

    initControls: function () {
        $.extend(si.c, si.controls);

        si.c.tab = $(si.controls.tab);

        si.c.btnViewUsuarios = si.c.tab.find(si.controls.btnViewUsuarios);
        si.c.btnRefreshGrid = si.c.tab.find(si.controls.btnRefreshGrid);

        si.c.ImpersonateQueryString = si.c.tab.find(si.controls.ImpersonateQueryString);
    },

    initPlugins: function () {

        $(si.c.ImpersonateQueryString).autocomplete({
            serviceUrl: URLs.Usuario.Find,
            type: 'POST',
            deferRequestBy: 1000,
            minChars: 3,
            dataType: 'json',
            onSelect: function(suggestion) {
                window.location = URLs.Account.FakeLogin + '?usuarioId=' + suggestion.data;
            }
        });

    },

    initHandlers: function () {

        si.c.btnViewUsuarios.on('click', function () {
            si.initGrid();
        });

        si.c.btnRefreshGrid.on('click', function () {
            si.grid.ajax.reload();
        });

    },

    initGrid: function () {

        si.grid = ASApp.Plugins.DataTables.initGrid(si.c.tab.find(si.controls.grid), {
            url: URLs.Soporte.UsuariosList,
            params: function () {
                return {

                };
            },
            columnDefs: [{
                className: 'dt-center',
                targets: '_all'
            }],
            columns: [
                { title: 'Usuario', data: 'Usuario' },
                { title: 'Email', data: 'Email', width: '25%' },
                { title: 'Rol', width: '20%', data: 'Rol' },
                {
                    title: 'Acciones', width: '10%', data: null, render: function (r, e, data) {
                        return '<a href="' + URLs.Account.FakeLogin + '?usuarioId=' + data.Id + '" type="button" class="btn btn-info btn-xs" data-bs-tooltip title="Impersonate"><i class="fa fa-user"></i></button>';
                    }
                },

            ],
            order: [[0, "asc"]],
            isSetupGrid: false,
            initComplete: function () {
                si.c.btnViewUsuarios.remove();
                si.c.btnRefreshGrid.removeClass('hide');
            },
            drawCallback: function () {
                ASApp.Plugins.BootstrapTooltip.init();
            }
        });
    }
}

var si = SoporteImpersonate;
$(function () {
    SoporteImpersonate.init();
});
