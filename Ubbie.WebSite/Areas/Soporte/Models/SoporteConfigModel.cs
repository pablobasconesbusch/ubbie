﻿using System.Collections.Generic;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Mapping;
using Ubbie.Servicios.Aplicacion;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteConfigModel
    {
        public List<SoporteConfigGroupModel> ConfigGroupModel { get; set; }

        public SoporteConfigModel()
        {
            var config = new ConfigurationService().GetConfigGroupList();
            ConfigGroupModel = MappingHelper.MapEntityListToMapperModelList<ConfigGroup, SoporteConfigGroupModel>(config);
        }
    }
}
