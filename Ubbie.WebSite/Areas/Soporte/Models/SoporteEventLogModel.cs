﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ubbie.Framework.Extensiones;
using Ubbie.Recursos;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteEventLogModel
    {
        [ASDisplay(nameof(Translation.Label_DateFrom))]
        public DateTime FilterByDateFrom { get; set; }

        [ASDisplay(nameof(Translation.Label_DateTo))]
        public DateTime FilterByDateTo { get; set; }

        [ASDisplay(nameof(Translation.Label_LogLevel))]
        public string FilterByLogLevel { get; set; }
        public SelectList FilterByLogLevelList { get; set; }

        [ASDisplay(nameof(Translation.Label_User))]
        public List<int> FilterByUsuarioIds { get; set; }
        public MultiSelectList FilterByUsuariosList { get; set; }

        public SoporteEventLogModel()
        {
            FilterByDateFrom = DateTime.UtcNow.ToUserDateTime().AddMonths(-1);
            FilterByDateTo = DateTime.UtcNow.ToUserDateTime().AddMonths(1);

            FilterByLogLevelList = Constants.LogLevelCode.VALUES.ToSelectList(x => x.Key, x => x.Value);
            FilterByUsuariosList = new ServicioUsuario().GetList().ToSelectList(x => x.Id, x => x.NombreCompleto);
        }
    }
}
