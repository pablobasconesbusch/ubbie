﻿using System.Linq;
using Ubbie.Servicios.Models;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteIndexModel
    {
        public SoporteConfigModel SoporteConfigModel { get; set; }
        public SoporteEventLogModel SoporteEventLogModel { get; set; }
        public SoporteSmtpModel SoporteSmtpModel { get; set; }

        public SoporteIndexModel()
        {
            SoporteConfigModel = new SoporteConfigModel();
            SoporteEventLogModel = new SoporteEventLogModel();
            SoporteSmtpModel = new SoporteSmtpModel(SoporteConfigModel.ConfigGroupModel.Single(x => x.Code == nameof(Configuration.Emailing)).ConfigSettings);
        }

    }
}
