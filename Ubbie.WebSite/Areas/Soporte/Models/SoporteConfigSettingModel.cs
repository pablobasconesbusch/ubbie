﻿using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Mapping;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteConfigSettingModel : MapperModel<SoporteConfigSettingModel, ConfigSetting, string>
    {

        public int Id { get; set; }
        public string ConfigGroupCode { get; set; }

        public string Key { get; set; }
        public string Description { get; set; }

        [ASRequired]
        public string Value { get; set; }
        public string Type { get; set; }

        public bool IsEditable { get; set; }

        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(ConfigSetting entity)
        {
            entity.Value = entity.Value.Trim();
        }

        protected override void ExtraMapToEntity(ConfigSetting entity)
        {
        }
    }
}
