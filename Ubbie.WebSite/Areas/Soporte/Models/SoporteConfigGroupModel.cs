﻿using System.Collections.Generic;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Mapping;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteConfigGroupModel : MapperModel<SoporteConfigGroupModel, ConfigGroup, string>
    {

        public string Code { get; set; }
        public string Description { get; set; }

        public List<SoporteConfigSettingModel> ConfigSettings { get; set; }




        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(ConfigGroup entity)
        {
            ConfigSettings = MappingHelper.MapEntityListToMapperModelList<ConfigSetting, SoporteConfigSettingModel>(entity.ConfigSettings);
        }

        protected override void ExtraMapToEntity(ConfigGroup entity)
        {
            entity.ConfigSettings = MappingHelper.MapMapperModelListToEntityList<SoporteConfigSettingModel, ConfigSetting>(ConfigSettings);
        }
    }
}
