﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Extensiones;
using Ubbie.Recursos;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Soporte.Models
{
    public class SoporteSmtpModel
    {
        // Email Test
        [ASRequired]
        public string SmtpHost { get; set; }

        public int SmtpPort { get; set; }

        [ASRequired]
        public string SmtpUsuarioname { get; set; }

        [ASRequired]
        public string SmtpPassword { get; set; }


        public bool SmtpUsuarioDefaultCredentials { get; set; }
        public bool SmtpEnableSSL { get; set; }

        [ASRequired]
        public string EmailTo { get; set; }

        [ASRequired]
        public string EmailSubject { get; set; }

        [ASRequired]
        public string EmailBody { get; set; }



        // Grid
        [ASDisplay(nameof(Translation.Label_DateFrom))]
        public DateTime FilterByDateFrom { get; set; }

        [ASDisplay(nameof(Translation.Label_DateTo))]
        public DateTime FilterByDateTo { get; set; }

        [ASDisplay(nameof(Translation.Label_EmailStatus))]
        public string EmailStatusCode { get; set; }
        public SelectList EmailStatusList { get; set; }



        public SoporteSmtpModel()
        {

        }

        public SoporteSmtpModel(List<SoporteConfigSettingModel> model)
        {
            FilterByDateFrom = DateTime.UtcNow.ToUserDateTime().AddMonths(-1);
            FilterByDateTo = DateTime.UtcNow.ToUserDateTime().AddMonths(1);
            EmailStatusList = Constants.EmailStatusCode.VALUES.ToSelectList(x => x.Key, x => x.Value);


            SmtpHost = model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpServer)).Value;
            SmtpPort = int.Parse(model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpPort)).Value);
            SmtpUsuarioname = model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpUsername)).Value;
            SmtpPassword = model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpPassword)).Value;
            SmtpUsuarioDefaultCredentials = model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpUseDefaultCredentials)).Value == "True";
            SmtpEnableSSL = model.Single(x => x.Key == nameof(Configuration.Emailing.SmtpEnableSsl)).Value == "True";

        }

    }
}