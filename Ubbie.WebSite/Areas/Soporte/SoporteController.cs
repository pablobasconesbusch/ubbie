﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web.Mvc;
using Ubbie.Entidades.Soporte;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.Servicios.Soporte;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Soporte.Models;

namespace Ubbie.WebSite.Areas.Soporte
{
    [Authorize]
    [ViewsPath("~/Areas/Soporte/Views")]
    public class SoporteController : BaseController
    {
        public ActionResult Index()
        {
            if (!UsuarioActivo.EsSuperUsuario)
            {
                return HttpNotFound();
            }

            var model = new SoporteIndexModel();
            return View(model);
        }

        #region Config

        [HttpPost]
        public JsonResult SaveConfig(SoporteConfigModel model)
        {
            var entities = model.ConfigGroupModel.Select(x => x.ToEntity()).ToList();
            var service = new ConfigurationService();

            var sr = service.UpdateConfigSettings(entities);

            if (!sr.Status)
                return Json(sr);

            sr.Reload = true;
            return Json(sr);
        }


        [HttpPost]
        public JsonResult RefreshConfig()
        {
            var sr = new ServiceResponse();
            new ConfigurationService().LoadConfig();

            sr.MessageText = "Configuration settings reloaded";
            sr.Reload = true;
            sr.RedirectTime = 1000;
            return Json(sr);
        }

        #endregion

        #region EventLog

        public JsonResult EventLogList(EventLogParameters p)
        {
            if (p.SortField == "Usuario") p.SortField = "Usuario.Nombre";

            var service = new EventLogService();
            var sr = service.GetList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<EventLog>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    Date = x.Date.ToString("g"),
                    x.EventCode,
                    x.LogLevel,
                    x.ServerException,
                    x.RequestType,
                    x.UserAgent,
                    x.QueryString,
                    x.Path,
                    Usuario = x.User?.NombreCompleto ?? "-"
                });

            return Json(sr);
        }

        public ActionResult EventLogInfo(int entityId)
        {
            var model = new EventLogModel(entityId);
            return PartialView("_SoporteEventLogInfo", model);
        }


        #endregion


        #region SMTP

        public JsonResult EmailList(EmailLogParameters p)
        {
            if (p.SortField == "Date") p.SortField = "FechaCreacion";

            var sr = EmailingService.GetList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Email>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    Date = x.FechaCreacion.ToString("g", Thread.CurrentThread.CurrentCulture),
                    x.Subject,
                    x.Body,
                    x.Recipients,
                    x.EmailStatusCode
                });

            return Json(sr);
        }

        [HttpPost]
        public ActionResult EmailInfo(int entityId)
        {
            var email = EmailingService.Get(entityId);
            return PartialView("_SoporteEmailInfo", email);

        }

        [HttpPost]
        public JsonResult SendTestEmail(SoporteSmtpModel model)
        {
            var smtp = new SmtpClient
            {
                Host = model.SmtpHost,
                Port = model.SmtpPort,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = model.SmtpUsuarioDefaultCredentials,
                EnableSsl = model.SmtpEnableSSL,
                Credentials = new NetworkCredential(model.SmtpUsuarioname, model.SmtpPassword)
            };


            var email = new Email
            {
                Subject = model.EmailSubject,
                Body = model.EmailBody,
                Sender = Configuration.Emailing.FromAddress,
                Recipients = model.EmailTo,
                EmailStatusCode = Constants.EmailStatusCode.QUEUED
            };

            var sr = EmailingService.SendTestEmail(smtp, email);

            if (!sr.Status)
                return Json(sr);

            sr.MessageText = "Email sent.";
            return Json(sr);
        }

        [HttpPost]
        public JsonResult ResendEmail(int emailId)
        {
            var sr = EmailingService.ResendEmail(emailId);

            if (!sr.Status)
                return Json(sr);

            sr.MessageText = "Email sent.";
            return Json(sr);
        }

        #endregion

        #region Impersonate

        [HttpPost]
        public JsonResult UsuariosList(PagerParameters p)
        {
            if (p.SortField == "Usuario") p.SortField = "Nombre";

            var sr = new ServicioUsuario().GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Seguridad.Usuario>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    Usuario = x.NombreCompleto,
                    x.Email,
                    Rol = $"{x.CodigoRol} - ({x.Rol.Nombre})"
                });

            return Json(sr);
        }

        #endregion
    }
}
