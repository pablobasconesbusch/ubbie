﻿$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Noticia.List,
                params: function () {
                    return {

                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: 'Fecha', data: 'Fecha' },
                    { title: 'Título', data: 'Titulo' },
                    { title: 'Categoría', data: 'Nombre' },
                    {
                        title: 'Imagen', data: 'Imagen', orderable: false, render(data) {
                            return ASApp.Utils.renderImageColumn(data, 150);
                        }
                    }
                ],
                order: [[0, "desc"]]
            });
        },
    });

    ASApp.InfoView.init({
        mainTab: {
            onInit: function (tab) {
                Noticia.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Noticia.Delete
                }
            }
        },
        tabs: []
    });
});

var Noticia = {

    controls: {

    },

    c: {},

    init: function (tab) {
        var b = Noticia;

        b.initControls(tab);
        b.initPlugins(tab);
        b.initHandlers(tab);

    },

    initControls: function (tab) {
        var b = Noticia;
        b.c = $.extend({}, b.controls);

    },

    initPlugins: function (tab) {
        var b = Noticia;

        ASApp.Plugins.BootstrapDatepicker.init(tab.targetContent);
    },

    initHandlers: function (tab) {
        var b = Noticia;

    }
}

