﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Noticia.Models
{
    public class NoticiaModel : MapperModel<NoticiaModel, Entidades.Negocio.Noticia, int>
    {
        public int Id { get; set; }

        [ASRequiredDropdown, Display(ResourceType = typeof(Translation), Name = "Label_Category")]
        public int NoticiaCategoriaId { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Date")]
        public string Fecha { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Title")]
        public string Titulo { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_SmallImage")]
        public string ImagenId { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_MainImage")]
        public string ImagenHDId { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Body")]
        public string Cuerpo { get; set; }

        public SelectList NoticiaCategorias { get; set; }

        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Titulo;
            if (IsInitialized)
                return;

            HeaderTitle = "Nueva noticia";
            NoticiaCategorias = new ServicioNoticia().GetCategorias().ToSelectList(x => x.Id, x => x.Nombre);
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Noticia entity)
        {
            HeaderTitle = entity.Titulo;
            Fecha = entity.Fecha.ToString("d");
            ImagenId = entity.ImagenId.ToString();
            ImagenHDId = entity.ImagenHDId.ToString();
        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Noticia entity)
        {
            if (!string.IsNullOrEmpty(Fecha))
            {
                DateTime fecha;
                var conversion = DateTime.TryParse(Fecha, out fecha);

                if (!conversion)
                    throw new Exception($"{nameof(Fecha)} conversion error");

                entity.Fecha = fecha;
            }
            entity.ImagenId = ImagenId.ToInt();
            entity.ImagenHDId = ImagenHDId.ToInt();
        }

        public NoticiaModel() { }

        public NoticiaModel(int id) : base(id, x => new ServicioNoticia().Get(id))
        {

        }

        public NoticiaModel(Entidades.Negocio.Noticia entity) : base(entity)
        {

        }
    }
}
