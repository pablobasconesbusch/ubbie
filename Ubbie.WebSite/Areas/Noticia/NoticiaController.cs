﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Noticia.Models;

namespace Ubbie.WebSite.Areas.Noticia
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.NOTICIA)]
    [ViewsPath("~/Areas/Noticia/Views")]
    public class NoticiaController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Info(int entityId)
        {
            var model = entityId == 0 ? new NoticiaModel() : new NoticiaModel(entityId);
            model.InitializateData();

            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            var service = new ServicioNoticia();

            if (p.SortField == "Nombre") p.SortField = "NoticiaCategoria.Nombre";

            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Negocio.Noticia>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    x.Titulo,
                    x.NoticiaCategoria.Nombre,
                    Fecha = x.Fecha.ToString("d"),
                    Imagen = CustomEncrypt.Encrypt(x.ImagenId.ToString())
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.NOTICIA, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.NOTICIA, Constants.PatentCode.EDIT)]
        public JsonResult Save(NoticiaModel model)
        {
            var service = new ServicioNoticia();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);

            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.NOTICIA, Constants.PatentCode.DELETE)]
        public JsonResult Delete(NoticiaModel model)
        {
            var service = new ServicioNoticia();
            var entity = model.ToEntity();
            var sr = service.DeleteEntity(entity);

            return Json(sr);
        }
    }
}