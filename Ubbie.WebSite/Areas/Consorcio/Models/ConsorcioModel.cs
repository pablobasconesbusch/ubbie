﻿using System.ComponentModel.DataAnnotations;
using Ubbie.Framework.Mapping;
using Ubbie.Recursos;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Consorcio.Models
{
    public class ConsorcioModel : MapperModel<ConsorcioModel, Entidades.Negocio.Consorcio, int>
    {
        public int Id { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Name")]
        [RegularExpression("^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_UseLetterOnly")]
        public string Nombre { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Address")]
        [RegularExpression("^[ñA-Za-z _]*[ñA-Za-z][ñA-Za-z _]*$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = "Validation_UseLetterOnly")]
        public string Direccion { get; set; }

        [ASRequired, Display(ResourceType = typeof(Translation), Name = "Label_Number")]
        [RegularExpression("^[0-9]+$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_JustNumbers))]
        public string Numero { get; set; }

        [ASRequired, AsCuit, Display(ResourceType = typeof(Translation), Name = "Label_CUIT")]
        [RegularExpression("^[0-9]{2}-[0-9]{8}-[0-9]$", ErrorMessageResourceType = typeof(Translation), ErrorMessageResourceName = nameof(Translation.Validation_CUIT))]
        public string CUIT { get; set; }

        [Display(ResourceType = typeof(Translation), Name = "Label_PostalCode")]
        public int CodigoPostal { get; set; }

        public string HeaderTitle { get; set; }

        public override void InitializateData()
        {
            HeaderTitle = Nombre;
            if (IsInitialized)
                return;

            HeaderTitle = $"{Translation.Label_New} {Translation.Label_Consorcio}";
        }

        protected override void ExtraMapFromEntity(Entidades.Negocio.Consorcio entity)
        {

        }

        protected override void ExtraMapToEntity(Entidades.Negocio.Consorcio entity)
        {

        }

        public ConsorcioModel() { }

        public ConsorcioModel(int id) : base(id, x => new ServicioConsorcio().Get(id))
        {

        }

        public ConsorcioModel(Entidades.Negocio.Consorcio entity) : base(entity)
        {

        }
    }
}
