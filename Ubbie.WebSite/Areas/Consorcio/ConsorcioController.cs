﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Areas.Consorcio.Models;

namespace Ubbie.WebSite.Areas.Consorcio
{
    [Authorize]
    [ResourceAuthorize(Constants.ResourceCode.CONSORCIO)]
    [ViewsPath("~/Areas/Consorcio/Views")]
    public class ConsorcioController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Info(int entityId)
        {
            var model = entityId == 0 ? new ConsorcioModel() : new ConsorcioModel(entityId);
            model.InitializateData();
            return PartialView(model);
        }

        [HttpPost]
        public JsonResult List(PagerParameters p)
        {
            var service = new ServicioConsorcio();

            var sr = service.GetSetupList(p);

            if (!sr.Status)
                return Json(sr);

            sr.Data = ((List<Entidades.Negocio.Consorcio>)sr.Data)
                .Select(x => new
                {
                    x.Id,
                    x.Nombre,
                    x.Direccion,
                    x.CUIT
                });

            return Json(sr);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ResourcePatentAuthorize(Constants.ResourceCode.CONSORCIO, Constants.PatentCode.ADD)]
        [ResourcePatentAuthorize(Constants.ResourceCode.CONSORCIO, Constants.PatentCode.EDIT)]
        public JsonResult Save(ConsorcioModel model)
        {
            var service = new ServicioConsorcio();
            var entity = model.ToEntity();
            var sr = service.SaveEntity(entity);
            return Json(sr);
        }

        [HttpPost]
        [ResourcePatentAuthorize(Constants.ResourceCode.CONSORCIO, Constants.PatentCode.DELETE)]
        public JsonResult Delete(ConsorcioModel model)
        {
            var service = new ServicioConsorcio();
            var entity = model.ToEntity();
            var sr = service.DeleteEntity(entity);
            return Json(sr);
        }
    }
}
