﻿$(function () {
    ASApp.GridView.init({
        initGrid: function (grid) {
            ASApp.Plugins.DataTables.initGrid(grid, {
                url: URLs.Consorcio.List,
                params: function () {
                    return {

                    };
                },
                columnDefs: [{
                    className: 'dt-center',
                    targets: '_all'
                }],
                columns: [
                    { title: 'Nombre', data: 'Nombre' },
                    { title: 'Dirección', data: 'Direccion' },
                    { title: 'CUIT', data: 'CUIT' }
                ],
                order: [[0, "desc"]]
            });
        },
    });

    ASApp.InfoView.init({
        mainTab: {
            onInit: function (tab) {
                Consorcio.init(tab);
            },
            getSaveModel: function (data) {

            },
            getDeleteModel: function () {
                return {
                    url: URLs.Consorcio.Delete
                }
            }
        },
        tabs: []
    });
});

var Consorcio = {

    controls: {

    },

    c: {},

    init: function (tab) {
        var b = Consorcio;

        b.initControls(tab);
        b.initPlugins(tab);
        b.initHandlers(tab);

    },

    initControls: function (tab) {
        var b = Consorcio;
        b.c = $.extend({}, b.controls);

    },

    initPlugins: function (tab) {
        var b = Consorcio;

    },

    initHandlers: function (tab) {
        var b = Consorcio;

    }
}

