﻿using System.Linq;
using System.Web.Mvc;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Areas.Home
{
    [ViewsPath("~/Areas/Home/Views")]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (AppInfo.UsuarioActivo == null)
                return View("~/Areas/Home/Views/FrontPage.cshtml");


            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.DASHBOARD))
                return RedirectToAction("Index", "Dashboard");


            var user = new ServicioUsuario().Get(UsuarioActivo.Id);
            if (!string.IsNullOrEmpty(user.DefaultCodigoFamilia))
                return RedirectToAction(user.DefaultFamilia.Action, user.DefaultFamilia.Controller);

            var firstResourceAvailable = user.Rol.RolFamilias.Select(x => x.Familia).First();
            return RedirectToAction(firstResourceAvailable.Action, firstResourceAvailable.Controller);
        }

        public ActionResult FrontPage()
        {
            return View("~/Areas/Home/Views/FrontPage.cshtml");
        }

        [HttpPost]
        public JsonResult ContactMe(ContactModel model)
        {
            var sr = EmailingService.SendContactMessage(model);
            return Json(sr);
        }

    }
}