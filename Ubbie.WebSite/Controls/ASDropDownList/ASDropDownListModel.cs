﻿using Ubbie.WebSite.Controls.ASDropDownList.BootstrapSelect;
using Ubbie.WebSite.Controls.BaseModels;

namespace Ubbie.WebSite.Controls.ASDropDownList
{
    public class ASDropDownListModel : ASBaseControlModel
    {
        public bool IsMultiple { get; set; }
        public bool HasSearch { get; set; }
        public DropDownListStyleEnum DownListStyle { get; set; } = DropDownListStyleEnum.BOOTSTRAPSELECT;
        public BootstrapSelectModel BootstrapSelectModel { get; set; }

    }


    public enum DropDownListStyleEnum
    {
        NOSTYLE,
        SELECT2,
        BOOTSTRAPSELECT
    }
}
