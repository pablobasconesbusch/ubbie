﻿namespace Ubbie.WebSite.Controls.ASDropDownList.BootstrapSelect
{
    public class BootstrapSelectModel
    {
        public bool ShowArrow { get; set; }
        public bool ShowActionsBox { get; set; }
    }
}
