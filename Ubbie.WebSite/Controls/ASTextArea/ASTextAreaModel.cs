﻿using Ubbie.WebSite.Controls.BaseModels;

namespace Ubbie.WebSite.Controls.ASTextArea
{
    public class ASTextAreaModel : ASBaseControlModel
    {
        public int Rows { get; set; } = 3;
    }
}
