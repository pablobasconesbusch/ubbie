﻿using Ubbie.Recursos;

namespace Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonCancel
{
    public class ASInfoButtonCancelModel : ASInfoButtonModel
    {

        public ASInfoButtonCancelModel()
        {
            Text = Translation.Label_Cancel;
            Classes = "btn btn-default";
            Type = "button";
            Attributes = "data-btn-cancel";
        }
    }
}
