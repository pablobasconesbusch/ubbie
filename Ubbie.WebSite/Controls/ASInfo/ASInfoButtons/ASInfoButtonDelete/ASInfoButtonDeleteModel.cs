﻿using Ubbie.Recursos;

namespace Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonDelete
{
    public class ASInfoButtonDeleteModel : ASInfoButtonModel
    {

        public ASInfoButtonDeleteModel()
        {
            Text = Translation.Label_Delete;
            Classes = "btn btn-danger";
            Type = "button";
            Attributes = "data-btn-delete";
        }
    }
}
