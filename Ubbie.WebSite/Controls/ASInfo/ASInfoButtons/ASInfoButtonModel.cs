﻿
namespace Ubbie.WebSite.Controls.ASInfo.ASInfoButtons
{
    public class ASInfoButtonModel
    {
        public string Text { get; set; }
        public string Classes { get; set; }
        public string Attributes { get; set; }
        public string Type { get; set; }
    }
}
