﻿using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonCancel;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonDelete;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonSave;

namespace Ubbie.WebSite.Controls.ASInfo.ASInfoButtons
{
    public class ASInfoButtonsModel
    {
        public bool RenderASInfoButtonSave { get; set; }
        public bool RenderASInfoButtonDelete { get; set; }
        public bool RenderASInfoButtonCancel { get; set; } = true;

        public ASInfoButtonSaveModel AsInfoButtonSaveModel { get; set; } = new ASInfoButtonSaveModel();
        public ASInfoButtonDeleteModel AsInfoButtonDeleteModel { get; set; } = new ASInfoButtonDeleteModel();
        public ASInfoButtonCancelModel ASInfoButtonCancelModel { get; set; } = new ASInfoButtonCancelModel();

        public bool RenderAsGroup { get; set; } = true;
    }
}
