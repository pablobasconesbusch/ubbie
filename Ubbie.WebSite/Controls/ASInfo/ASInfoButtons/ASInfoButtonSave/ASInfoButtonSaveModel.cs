﻿using Ubbie.Recursos;

namespace Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonSave
{
    public class ASInfoButtonSaveModel : ASInfoButtonModel
    {

        public ASInfoButtonSaveModel()
        {
            Text = Translation.Label_Save;
            Classes = "btn btn-success";
            Type = "submit";
        }
    }
}
