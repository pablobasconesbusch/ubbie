﻿
namespace Ubbie.WebSite.Controls.ASGrid.ASGridTabs
{
    public class ASGridTabsItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Class { get; set; }
        public string Url { get; set; }


        public string DivId => "div" + GetId();

        public ASGridTabsItem(string title, string id, string url)
        {
            Title = title;
            Id = id;
            Url = url;
        }

        private string GetId()
        {
            var id = Id;

            id = id.Replace(" ", "");
            id = id.Replace("&", "");
            id = id.Replace("+", "");
            id = id.Replace("/", "");
            id = id.Replace("-", "");
            id = id.Replace("-", "");
            id = id.Replace("(", "");
            id = id.Replace(")", "");
            id = id.ToLower();

            return id;
        }
    }
}