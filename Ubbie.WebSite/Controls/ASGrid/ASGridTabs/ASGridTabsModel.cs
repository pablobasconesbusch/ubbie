﻿using System.Collections.Generic;

namespace Ubbie.WebSite.Controls.ASGrid.ASGridTabs
{
    public class ASGridTabsModel
    {
        public bool RenderOnePartial { get; set; }
        public ASGridTabsItem MainTab { get; set; }
        public List<ASGridTabsItem> Tabs { get; set; }

        public bool ShowInfoBox { get; set; } = false;
    }
}
