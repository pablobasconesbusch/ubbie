﻿using Ubbie.Recursos;
using Ubbie.WebSite.Controls.BaseModels;

namespace Ubbie.WebSite.Controls.ASGrid.ASGridQuery
{
    public class ASGridQueryModel : ASBaseControlModel
    {

        public string ControlSize { get; set; } = ControlSizesCode.XLARGE;

        public ASGridQueryModel()
        {
            Id = "GridQuery";
            ControlHTMLAttributes.AddAttribute("placeholder", Translation.Query_InsertValueToSearch);
            ControlHTMLAttributes.AddAttribute("class", "form-control");
            ControlHTMLAttributes.AddAttribute("data-typewatch", "");
        }

    }

    public class ControlSizesCode
    {
        public const string MINI = "mini";
        public const string XSMALL = "xsmall";
        public const string SMALL = "small";
        public const string MEDIUM = "medium";
        public const string LARGE = "large";
        public const string XLARGE = "xlarge";
    }

}
