﻿using Ubbie.WebSite.Controls.ASGrid.ASGridActions;
using Ubbie.WebSite.Controls.ASGrid.ASGridTitle;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools;

namespace Ubbie.WebSite.Controls.ASGrid.ASGridTable
{
    public class ASGridTableModel
    {
        public string Id { get; set; } = "setupGrid";

        public ASGridTitleModel AsGridTitleModel { get; set; } = new ASGridTitleModel();
        public ASGridActionsModel AsGridActionsModel { get; set; } = new ASGridActionsModel();
        public ASGridToolsModel AsGridToolsModel { get; set; } = new ASGridToolsModel();
    }
}
