﻿namespace Ubbie.WebSite.Controls.ASGrid.ASGridTitle
{
    public class ASGridTitleModel
    {
        public string Title { get; set; } = "Poneme un título en ASGridTable()";
        public string Icon { get; set; }
        public bool RenderIcon { get; set; }

    }
}
