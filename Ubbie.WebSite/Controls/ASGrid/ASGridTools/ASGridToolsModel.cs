﻿using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonCollapse;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonNew;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonReloadGrid;

namespace Ubbie.WebSite.Controls.ASGrid.ASGridTools
{
    public class ASGridToolsModel
    {
        public bool RenderButtonNew { get; set; } = true;
        public bool RenderButtonCollapse { get; set; } = true;
        public bool RenderButtonReloadGrid { get; set; } = true;


        public ASGridButtonNewModel AsGridButtonNew { get; set; } = new ASGridButtonNewModel();
        public ASGridButtonCollapseModel ASGridButtonCollapseModel { get; set; } = new ASGridButtonCollapseModel();
        public ASGridButtonReloadGridModel AsGridButtonReloadGridModel { get; set; } = new ASGridButtonReloadGridModel();

    }
}
