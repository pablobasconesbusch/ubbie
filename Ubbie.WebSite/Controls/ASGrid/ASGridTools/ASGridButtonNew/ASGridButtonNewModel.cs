﻿namespace Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonNew
{
    public class ASGridButtonNewModel
    {
        public string Id { get; set; } = "btnNew";
        public string Classes { get; set; } = "btn btn-primary btn-sm";
    }
}
