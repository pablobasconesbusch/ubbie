﻿namespace Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonCollapse
{
    public class ASGridButtonCollapseModel
    {
        public string Id { get; set; } = "btnCollapseGrid";
        public string Classes { get; set; } = "collapse";
    }
}
