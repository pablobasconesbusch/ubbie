﻿namespace Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonReloadGrid
{
    public class ASGridButtonReloadGridModel
    {
        public string Id { get; set; } = "btnReloadGrid";
        public string Classes { get; set; } = "reload";
    }
}
