﻿
namespace Ubbie.WebSite.Controls.ASSpinner
{
    public class ASSpinnerModel
    {
        public string SpinnerType { get; set; } = ASSpinnerType.Atebits;
    }


    // See https://github.com/jlong/css-spinners
    public class ASSpinnerType 
    {
        public const string Spinner = "spinner";
        public const string Throbber = "throbber";
        public const string Refreshing = "refreshing";
        public const string Heartbeat = "heartbeat";
        public const string Gauge = "gauge";
        public const string ThreeQuarters = "three-quarters";
        public const string Wobblebar = "wobblebar";
        public const string Atebits = "atebits";
        public const string Whirly = "whirly";
        public const string Flower = "flower";
        public const string Dots = "fots";
        public const string Circles = "circles";
        public const string Plus = "plus";
        public const string Ball = "ball";
        public const string Hexdots = "hexdots";
        public const string InnerCircles = "innerCircles";
        public const string Pong = "pong";
        public const string Pulse = "pulse";
    }
}
