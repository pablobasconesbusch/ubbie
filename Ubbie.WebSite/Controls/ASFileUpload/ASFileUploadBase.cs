﻿using Ubbie.Recursos;
using Ubbie.Servicios.Models;

namespace Ubbie.WebSite.Controls.ASFileUpload
{
    public class ASFileUploadBase
    {
        public string PropertyName { get; set; } = "DummyAvatarUpload";

        public bool HasLabel { get; set; } = true;
        public string LabelText { get; set; } = Translation.Label_Image;

        public bool HasRequiredIndicator => IsRequired;
        public bool IsRequired { get; set; }
        public string ValidationMessage { get; set; }

        public string HtmlAttributes { get; set; }
        public string Class { get; set; }

        public bool IsInsideForm { get; set; } = true;
        public string FileSystemSaveFolder { get; set; } = "temp";
        public string FileSaveMode { get; set; } = Configuration.Support.FileSaveMode;

        // Plugin properties
        public string AcceptedFiles { get; set; } = "image/*";
        public int ThumbnailWidth { get; set; } = 500;
        public int ThumbnailHeight { get; set; } = 500;
        public int MaxFileSize { get; set; } = 2;

        public bool CanBeRemoved { get; set; } = true;
    }

}
