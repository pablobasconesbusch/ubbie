﻿
using Ubbie.Servicios.Models;

namespace Ubbie.WebSite.Controls.ASSession
{
    public class ASSessionModel
    {
        public int Timeout => Configuration.Security.CookieExpirationTime;
        public string Markup => $"<script>ASApp.Session.timeOut = {Timeout * 1000}</script>";
    }
}
