﻿
namespace Ubbie.WebSite.Controls.ASSettings
{
    public class ASSettingsModel
    {
        public string Language { get; set; }
        public string LanguageTwoLetter { get; set; }

        public string DataTablesLanguageLocation { get; set; }

        public string DateFormat { get; set; }

        public string NotifySuccessMessage { get; set; }
        public string NotifyErrorMessage { get; set; }
        public string NotifyCancelText { get; set; }
        public string NotifyConfirmText { get; set; }
        public string NotifyYesText { get; set; }
        public string NotifyNoText { get; set; }
        public string NotifyConfirmDeleteMessage { get; set; }

        public string PluginBootstrapSelectSelectAllText { get; set; }
        public string PluginBootstrapSelectDeselectAllText { get; set; }
        public string PluginBootstrapSelectNoneSelectedText { get; set; }
    }
}
