﻿using System.Collections.Generic;

namespace Ubbie.WebSite.Controls.BaseModels
{
    public class ASBaseControlModel
    {
        public string Id { get; set; }
        public string Value { get; set; }

        public bool IsFormControl { get; set; } = true;
        public bool IsReadOnly { get; set; }

        public bool HasLabel { get; set; } = true;
        public bool HasValidationMessage { get; set; }
        public bool HasRequiredIndicator { get; set; }
        public string LabelText { get; set; }
        public string Type { get; set; } = ASBaseControlType.REGULAR;

        public string LabelColumnSize { get; set; } = "col-md-6";
        public string ControlColumnSize { get; set; } = "col-md-18";

        public string Placeholder { get; set; }

        public IDictionary<string, object> LabelHTMLAttributes { get; set; } = new Dictionary<string, object>();
        public IDictionary<string, object> ControlHTMLAttributes { get; set; } = new Dictionary<string, object>();
    }

    public class ASBaseControlType
    {
        public const string REGULAR = "REGULAR";
        public const string HORIZONTAL = "HORIZONTAL";
    }
}
