﻿using System.Collections.Generic;

namespace Ubbie.WebSite.Controls.BaseModels
{
    public abstract class ASCheckBoxRadioButtonModel
    {
        public ASCheckBoxRadioButtonStyleEnum CheckBoxRadioButtonStyle { get; set; } = ASCheckBoxRadioButtonStyleEnum.METRONIC;
        public IcheckModel Icheck { get; set; }
        public MetronicModel Metronic { get; set; }

        public string ControlId { get; set; } = "chkDummy";
        public string Label { get; set; } = "Put me a description";

        public bool IsFormControl { get; set; } = true;
        public bool HasLabel { get; set; } = true;

        public IDictionary<string, object> LabelHTMLAttributes { get; set; } = new Dictionary<string, object>();
        public IDictionary<string, object> ControlHTMLAttributes { get; set; } = new Dictionary<string, object>();

    }


    public class IcheckModel
    {
        public string IcheckStyle { get; set; } = IcheckStyleCode.SQUARE;
        public string IcheckColor { get; set; } = IcheckColorCode.BLUE;
        public string IcheckLineLabel { get; set; }
    }

    public class MetronicModel
    {
        public string BorderStyle { get; set; } = MetronicMaterialDesignCheckBoxRadioButtonBorderCode.DARK;

    }

    public enum ASCheckBoxRadioButtonStyleEnum
    {
        NOSTYLE,
        METRONIC,
        MATERIAL,
        ICHECK
    }


    public class IcheckStyleCode
    {
        public const string MINIMAL = "_minimal";
        public const string SQUARE = "_square";
        public const string FLAT = "_flat";
        public const string LINE = "_line";
    }

    public class IcheckColorCode
    {
        public const string DEFAULT = "";
        public const string RED = "-red";
        public const string GREEN = "-green";
        public const string BLUE = "-blue";
        public const string AERO = "-aero";
        public const string GREY = "-grey";
        public const string ORANGE = "-orange";
        public const string PINK = "-pink";
        public const string PURPLE = "-purple";
    }

    public class MetronicMaterialDesignCheckBoxRadioButtonBorderCode
    {
        public const string DARK = "";
        public const string LIGHT = "md-checkbox--light md-radio--light";
    }

}
