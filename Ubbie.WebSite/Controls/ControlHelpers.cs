﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Ubbie.Recursos;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;
using Ubbie.WebSite.Controls.ASCheckBox;
using Ubbie.WebSite.Controls.ASDropDownList;
using Ubbie.WebSite.Controls.ASDropDownList.BootstrapSelect;
using Ubbie.WebSite.Controls.ASFileUpload;
using Ubbie.WebSite.Controls.ASGrid.ASGridActions;
using Ubbie.WebSite.Controls.ASGrid.ASGridQuery;
using Ubbie.WebSite.Controls.ASGrid.ASGridTable;
using Ubbie.WebSite.Controls.ASGrid.ASGridTabs;
using Ubbie.WebSite.Controls.ASGrid.ASGridTitle;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonCollapse;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonNew;
using Ubbie.WebSite.Controls.ASGrid.ASGridTools.ASGridButtonReloadGrid;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonCancel;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonDelete;
using Ubbie.WebSite.Controls.ASInfo.ASInfoButtons.ASInfoButtonSave;
using Ubbie.WebSite.Controls.ASPassword;
using Ubbie.WebSite.Controls.ASRadioButton;
using Ubbie.WebSite.Controls.ASSession;
using Ubbie.WebSite.Controls.ASSettings;
using Ubbie.WebSite.Controls.ASSpinner;
using Ubbie.WebSite.Controls.ASTable;
using Ubbie.WebSite.Controls.ASTextArea;
using Ubbie.WebSite.Controls.ASTextBox;
using Ubbie.WebSite.Controls.BaseModels;
using Ubbie.WebSite.Shared.Models;

namespace Ubbie.WebSite.Controls
{
    public static class ControlHelpers
    {
        // AS Controls
        #region ASPassword

        public static MvcHtmlString ASPassword<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, AsPasswordControlModel model = null)
        {
            var html = new StringBuilder();

            if (model == null)
                model = new AsPasswordControlModel();

            var memberExpression = expression.Body as MemberExpression;
            var hasASRequiredAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequired), false).Length > 0;


            // Label
            if (model.HasLabel)
            {
                if (hasASRequiredAttribute || model.HasRequiredIndicator)
                    model.LabelHTMLAttributes.AddAttribute("class", "required-indicator");

                html.Append(helper.LabelFor(expression, model.LabelHTMLAttributes));
            }

            // Input
            if (model.IsFormControl)
                model.ControlHTMLAttributes.AddAttribute("class", "form-control");

            if (model.IsReadOnly)
                model.ControlHTMLAttributes.AddAttribute("readonly", "readonly");

            html.Append(helper.PasswordFor(expression, model.ControlHTMLAttributes));

            // Validation Message
            if (hasASRequiredAttribute || model.HasValidationMessage)
                html.Append(helper.ValidationMessageFor(expression));

            return MvcHtmlString.Create(html.ToString());
        }

        #endregion

        #region ASTextBox
        public static MvcHtmlString ASTextBox<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, ASTextBoxModel model = null)
        {
            var html = new StringBuilder();

            if (model == null)
                model = new ASTextBoxModel();

            var memberExpression = expression.Body as MemberExpression;
            var hasASRequiredAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequired), false).Any();

            // Label
            if (model.HasLabel)
            {
                if (model.Type == ASBaseControlType.HORIZONTAL)
                    model.LabelHTMLAttributes.AddAttribute("class", $"control-label {model.LabelColumnSize}");

                if (hasASRequiredAttribute || model.HasRequiredIndicator)
                    model.LabelHTMLAttributes.AddAttribute("class", "required-indicator");

                if (string.IsNullOrEmpty(model.LabelText))
                    html.Append(helper.LabelFor(expression, model.LabelHTMLAttributes));
                else
                    html.Append(helper.LabelFor(expression, model.LabelText, model.LabelHTMLAttributes));

            }

            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append($"<div class=\"{model.ControlColumnSize}\">");

            // Input
            if (model.IsFormControl)
                model.ControlHTMLAttributes.AddAttribute("class", "form-control");

            if (model.IsReadOnly)
                model.ControlHTMLAttributes.AddAttribute("readonly", "readonly");

            if (!string.IsNullOrEmpty(model.Placeholder))
                model.ControlHTMLAttributes.AddAttribute("placeholder", model.Placeholder);

            html.Append(helper.TextBoxFor(expression, model.ControlHTMLAttributes));

            // Validation Message
            if (hasASRequiredAttribute || model.HasValidationMessage)
                html.Append(helper.ValidationMessageFor(expression));

            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append("</div>");

            return MvcHtmlString.Create(html.ToString());
        }
        #endregion

        #region ASDropDownList
        public static MvcHtmlString ASDropDownList<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> selectList, ASDropDownListModel model = null)
        {
            var html = new StringBuilder();

            if (model == null)
                model = new ASDropDownListModel();


            var memberExpression = expression.Body as MemberExpression;
            var hasASRequiredAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequired), false).Length > 0;
            var hasASRequiredDropdownAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequiredDropdown), false).Length > 0;

            var isMultiselect = memberExpression.Type.IsGenericType && memberExpression.Type.GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));

            // Label
            if (model.HasLabel)
            {
                if (model.Type == ASBaseControlType.HORIZONTAL)
                    model.LabelHTMLAttributes.AddAttribute("class", $"control-label {model.LabelColumnSize}");

                if (hasASRequiredAttribute || hasASRequiredDropdownAttribute || model.HasRequiredIndicator)
                    model.LabelHTMLAttributes.AddAttribute("class", "required-indicator");

                html.Append(helper.LabelFor(expression, model.LabelHTMLAttributes));
            }


            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append($"<div class=\"{model.ControlColumnSize}\">");

            // DropDown
            if (model.IsFormControl)
                model.ControlHTMLAttributes.AddAttribute("class", "form-control");

            if (model.DownListStyle == DropDownListStyleEnum.BOOTSTRAPSELECT)
            {
                if (model.BootstrapSelectModel == null)
                    model.BootstrapSelectModel = new BootstrapSelectModel();

                model.ControlHTMLAttributes.AddAttribute("data-bootstrap-select", "");

                if (model.HasSearch)
                    model.ControlHTMLAttributes.AddAttribute("data-live-search", "true");

                if (model.BootstrapSelectModel.ShowActionsBox)
                    model.ControlHTMLAttributes.Add("data-actions-box", "true");

            }
            else if (model.DownListStyle == DropDownListStyleEnum.SELECT2)
                model.ControlHTMLAttributes.AddAttribute("data-select-2", "");

            MvcHtmlString dropdown;

            if (isMultiselect || model.IsMultiple)
            {
                model.ControlHTMLAttributes.AddAttribute("data-placeholder", Constants.SelectListOptionCode.SELECT_MULTI);
                dropdown = helper.ListBoxFor(expression, selectList, model.ControlHTMLAttributes);
            }
            else
                dropdown = helper.DropDownListFor(expression, selectList, Constants.SelectListOptionCode.SELECT, model.ControlHTMLAttributes);

            var dropdownFixed = Regex.Replace(dropdown.ToString(), @"(?<=data-val-required=).+?(?=\s)", "\"" + Translation.Validation_RequiredFieldClientSide + "\"");
            html.Append(dropdownFixed);

            // Validation Message
            if (hasASRequiredDropdownAttribute || hasASRequiredAttribute || model.HasValidationMessage)
                html.Append(helper.ValidationMessageFor(expression));

            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append("</div>");

            return MvcHtmlString.Create(html.ToString());
        }

        #endregion

        #region ASTextArea
        public static MvcHtmlString ASTextArea<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, ASTextAreaModel model = null)
        {
            var html = new StringBuilder();

            if (model == null)
                model = new ASTextAreaModel();

            var memberExpression = expression.Body as MemberExpression;
            var hasASRequiredAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequired), false).Length > 0;

            // Label
            if (model.HasLabel)
            {
                if (model.Type == ASBaseControlType.HORIZONTAL)
                    model.LabelHTMLAttributes.AddAttribute("class", $"control-label {model.LabelColumnSize}");

                if (hasASRequiredAttribute || model.HasRequiredIndicator)
                    model.LabelHTMLAttributes.AddAttribute("class", "required-indicator");

                if (string.IsNullOrEmpty(model.LabelText))
                    html.Append(helper.LabelFor(expression, model.LabelHTMLAttributes));
                else
                    html.Append(helper.LabelFor(expression, model.LabelText, model.LabelHTMLAttributes));
            }

            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append($"<div class=\"{model.ControlColumnSize}\">");

            // Input
            if (model.IsFormControl)
                model.ControlHTMLAttributes.AddAttribute("class", "form-control");

            if (model.IsReadOnly)
                model.ControlHTMLAttributes.AddAttribute("readonly", "readonly");

            model.ControlHTMLAttributes.AddAttribute("rows", model.Rows.ToString());

            if (!string.IsNullOrEmpty(model.Placeholder))
                model.ControlHTMLAttributes.AddAttribute("placeholder", model.Placeholder);

            html.Append(helper.TextAreaFor(expression, model.ControlHTMLAttributes));

            // Validation Message
            if (hasASRequiredAttribute || model.HasValidationMessage)
                html.Append(helper.ValidationMessageFor(expression));

            if (model.Type == ASBaseControlType.HORIZONTAL)
                html.Append("</div>");

            return MvcHtmlString.Create(html.ToString());
        }
        #endregion
        #region ASCheckBox
        public static MvcHtmlString ASCheckBox<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, ASCheckBoxModel model = null)
        {
            if (model == null)
                model = new ASCheckBoxModel();

            return ASCheckBoxRadioButton(helper, expression, null, model);
        }
        #endregion

        #region ASRadioButton
        public static MvcHtmlString ASRadioButton<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, object value, ASRadioButtonModel model = null)
        {
            if (model == null)
                model = new ASRadioButtonModel();

            return ASCheckBoxRadioButton(helper, expression, value, model);
        }
        #endregion

        #region ASCheckBoxRadioButton
        private static MvcHtmlString ASCheckBoxRadioButton<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, object value, ASCheckBoxRadioButtonModel model = null)
        {
            var html = new StringBuilder();

            string modelType;
            string controlClass;

            if (model is ASCheckBoxModel)
            {
                modelType = "checkbox";
                controlClass = "check";
            }
            else
            {
                modelType = "radio";
                controlClass = "radiobtn";
            }

            // Icheck
            if (model.CheckBoxRadioButtonStyle == ASCheckBoxRadioButtonStyleEnum.ICHECK)
            {
                if (model.Icheck == null)
                    model.Icheck = new IcheckModel();

                model.ControlHTMLAttributes = model.ControlHTMLAttributes.AddAttribute("class", "icheck");
                model.ControlHTMLAttributes = model.ControlHTMLAttributes.AddAttribute($"data-{modelType}", $"i{modelType}{model.Icheck.IcheckStyle}{model.Icheck.IcheckColor}");

                if (model.Icheck.IcheckStyle == IcheckStyleCode.LINE)
                    model.ControlHTMLAttributes.AddAttribute("data-label", model.Icheck.IcheckLineLabel);

                if (model is ASCheckBoxModel)
                {
                    if (expression == null)
                        html.Append(helper.CheckBox(model.ControlId, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.CheckBoxFor(expression, model.ControlHTMLAttributes));
                }
                else
                {
                    if (expression == null)
                        html.Append(helper.RadioButton(model.ControlId, value, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.RadioButtonFor(expression, value, model.ControlHTMLAttributes));
                }
            }

            // Material
            else if (model.CheckBoxRadioButtonStyle == ASCheckBoxRadioButtonStyleEnum.MATERIAL)
            {
                if (model.Metronic == null)
                    model.Metronic = new MetronicModel();

                model.ControlHTMLAttributes = model.ControlHTMLAttributes.AddAttribute("class", $"md-{controlClass}");

                if (model.IsFormControl)
                    html.AppendLine("<label style=\"display: block\">&nbsp;</label>");

                html.AppendLine($"<div class=\"md-{modelType} {model.Metronic.BorderStyle}\">");

                if (model is ASCheckBoxModel)
                {
                    if (expression == null)
                        html.Append(helper.CheckBox(model.ControlId, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.CheckBoxFor(expression, model.ControlHTMLAttributes));
                }
                else
                {
                    if (expression == null)
                        html.Append(helper.RadioButton(model.ControlId, value, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.RadioButtonFor(expression, value, model.ControlHTMLAttributes));
                }


                string expressionString;

                if (expression == null)
                    expressionString = model.ControlId;
                else
                {
                    var lambdaExpressionName = ExpressionHelper.GetExpressionText(expression);
                    expressionString = TagBuilder.CreateSanitizedId(lambdaExpressionName);
                }

                html.AppendLine($"<label for=\"{expressionString}\">");
                html.AppendLine("<span class=\"inc\"></span>");
                html.AppendLine("<span class=\"check\"></span>");
                html.AppendLine("<span class=\"box\"></span>");


                if (model.HasLabel)
                {
                    if (expression == null)
                        html.Append(model.Label);
                    else
                        html.Append(helper.DisplayNameFor(expression));
                }

                html.AppendLine("</label>");
                html.AppendLine("</div>");
            }

            // Metronic
            else if (model.CheckBoxRadioButtonStyle == ASCheckBoxRadioButtonStyleEnum.METRONIC)
            {
                html.AppendLine($"<label class=\"mt-{modelType} mt-{modelType}-outline\">");

                if (model is ASCheckBoxModel)
                {
                    if (expression == null)
                        html.Append(helper.CheckBox(model.ControlId, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.CheckBoxFor(expression, model.ControlHTMLAttributes));
                }
                else
                {
                    if (expression == null)
                        html.Append(helper.RadioButton(model.ControlId, value, model.ControlHTMLAttributes));
                    else
                        html.Append(helper.RadioButtonFor(expression, value, model.ControlHTMLAttributes));
                }

                if (model.HasLabel)
                {
                    if (expression == null)
                        html.Append(model.Label);
                    else
                        html.Append(helper.DisplayNameFor(expression));
                }


                html.AppendLine("<span></span>");
                html.AppendLine("</label>");
            }

            return MvcHtmlString.Create(html.ToString());
        }
        #endregion


        // AS Components
        #region ASFileUpload

        public static MvcHtmlString ASFileUpload(this HtmlHelper helper, ASFileUploadModel model = null)
        {
            if (model == null)
                model = new ASFileUploadModel();

            model.ValidationMessage = string.Format(Translation.Validation_RequiredField, model.LabelText);

            return helper.Partial("~/Controls/ASFileUpload/ASFileUpload.cshtml", model);
        }

        public static MvcHtmlString ASFileUpload<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, ASFileUploadModel model = null)
        {
            if (model == null)
                model = new ASFileUploadModel();

            var memberExpression = expression.Body as MemberExpression;
            var hasASRequiredAttribute = memberExpression.Member.GetCustomAttributes(typeof(ASRequired), false).Length > 0;

            model.LabelText = HttpUtility.HtmlDecode(helper.DisplayNameFor(expression).ToString());
            model.PropertyName = ExpressionHelper.GetExpressionText(expression);

            if (hasASRequiredAttribute)
            {
                model.IsRequired = true;
                model.ValidationMessage = string.Format(Translation.Validation_RequiredField, model.LabelText);
            }

            var tValue = expression.Compile().Invoke(helper.ViewData.Model);

            if (tValue != null && Convert.ToInt32(tValue) != 0)
            {
                var value = Convert.ToInt32(tValue);
                model.File = new FileModel(value);
            }

            return helper.Partial("~/Controls/ASFileUpload/ASFileUpload.cshtml", model);
        }

        #endregion

        #region ASMultipleFileUpload

        public static MvcHtmlString ASMultipleFileUpload(this HtmlHelper helper, ASMultipleFileUpload.ASMultipleFileUpload model = null)
        {
            if (model == null)
                model = new ASMultipleFileUpload.ASMultipleFileUpload();

            if (model.FileIds.Any())
            {
                model.Files = model.FileIds.Select(x => new FileModel(x)).ToList();
            }

            return helper.Partial("~/Controls/ASMultipleFileUpload/ASMultipleFileUpload.cshtml", model);
        }

        public static MvcHtmlString ASMultipleFileUpload<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, ASMultipleFileUpload.ASMultipleFileUpload model = null)
        {
            if (model == null)
                model = new ASMultipleFileUpload.ASMultipleFileUpload();

            model.LabelText = HttpUtility.HtmlDecode(helper.DisplayNameFor(expression).ToString());
            model.PropertyName = ExpressionHelper.GetExpressionText(expression);

            var tValue = expression.Compile().Invoke(helper.ViewData.Model);

            if (tValue != null)
            {
                var values = (IList<int>)tValue;
                model.Files = values.Select(x => new FileModel(x)).ToList();
            }

            return helper.Partial("~/Controls/ASMultipleFileUpload/ASMultipleFileUpload.cshtml", model);
        }

        #endregion

        #region ASSetupHeader

        public static MvcHtmlString ASSetupHeader(this HtmlHelper helper, object model)
        {
            return helper.Partial("~/Shared/Views/_PortletHeaderRibbon.cshtml", model);
        }

        #endregion

        #region ASSpinner

        public static MvcHtmlString ASSpinner(this HtmlHelper helper)
        {
            var model = new ASSpinnerModel();
            return helper.Partial("~/Controls/ASSpinner/ASSpinner.cshtml", model);
        }

        #endregion

        #region ASSession

        public static MvcHtmlString ASSession(this HtmlHelper helper)
        {
            var model = new ASSessionModel();
            return helper.Partial("~/Controls/ASSession/ASSession.cshtml", model);
        }

        #endregion

        #region ASSettings

        public static MvcHtmlString ASSettings(this HtmlHelper helper)
        {
            var model = new ASSettingsModel();

            model.Language = Thread.CurrentThread.CurrentCulture.Name;
            model.LanguageTwoLetter = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;

            model.DataTablesLanguageLocation = new UrlHelper(HttpContext.Current.Request.RequestContext).Content($"~/Content/vendor/datatables/languages/{model.LanguageTwoLetter}.json");

            if (model.LanguageTwoLetter == "es")
                model.DateFormat = "dd/mm/yyyy";
            else
                model.DateFormat = "mm/dd/yyyy";


            model.NotifySuccessMessage = Translation.Notify_SuccessMessage;
            model.NotifyErrorMessage = Translation.Notify_ErrorMessage;
            model.NotifyCancelText = Translation.Notify_CancelText;
            model.NotifyConfirmText = Translation.Notify_ConfirmText;
            model.NotifyYesText = Translation.Notify_YesText;
            model.NotifyNoText = Translation.Notify_NoText;
            model.NotifyConfirmDeleteMessage = Translation.Notify_ConfirmDeleteMessage;

            model.PluginBootstrapSelectSelectAllText = Translation.Plugin_BootstrapSelect_SelectAllText;
            model.PluginBootstrapSelectDeselectAllText = Translation.Plugin_BootstrapSelect_DeselectAllText;
            model.PluginBootstrapSelectNoneSelectedText = Translation.Plugin_BootstrapSelect_NoneSelectedText;

            return helper.Partial("~/Controls/ASSettings/ASSettings.cshtml", model);
        }

        #endregion

        // AS Grid
        #region ASGridTabs
        public static MvcHtmlString ASGridTabs(this HtmlHelper helper, ASGridTabsItem maintab, bool renderOnePartial = true, params ASGridTabsItem[] tabs)
        {

            var model = new ASGridTabsModel
            {
                MainTab = maintab,
                RenderOnePartial = renderOnePartial,
                Tabs = tabs.ToList()
            };
            return helper.ASGridTabs(model);
        }

        public static MvcHtmlString ASGridTabs(this HtmlHelper helper, ASGridTabsModel model)
        {
            return helper.Partial("~/Controls/ASGrid/ASGridTabs/ASGridTabs.cshtml", model);
        }
        #endregion

        #region ASGridQuery
        public static MvcHtmlString ASGridQuery(this HtmlHelper helper, ASGridQueryModel model = null)
        {
            if (model == null)
                model = new ASGridQueryModel();

            return helper.Partial("~/Controls/ASGrid/ASGridQuery/ASGridQuery.cshtml", model);
        }
        #endregion

        #region ASGridTools
        public static MvcHtmlString ASGridTools(this HtmlHelper helper, ASGridToolsModel model = null)
        {
            if (model == null)
                model = new ASGridToolsModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTools/ASGridTools.cshtml", model);
        }
        #endregion

        #region ASGridActions
        public static MvcHtmlString ASGridActions(this HtmlHelper helper, ASGridActionsModel model = null)
        {
            if (model == null)
                model = new ASGridActionsModel();

            return helper.Partial("~/Controls/ASGrid/ASGridActions/ASGridActions.cshtml", model);
        }
        #endregion

        #region ASGridTitle
        public static MvcHtmlString ASGridTitle(this HtmlHelper helper, ASGridTitleModel model = null)
        {
            if (model == null)
                model = new ASGridTitleModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTitle/ASGridTitle.cshtml", model);
        }

        #endregion

        #region ASGridButtonNew
        public static MvcHtmlString ASGridButtonNew(this HtmlHelper helper, ASGridButtonNewModel model = null)
        {
            if (model == null)
                model = new ASGridButtonNewModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTools/ASGridButtonNew/ASGridButtonNew.cshtml", model);
        }
        #endregion

        #region ASGridButtonCollapse
        public static MvcHtmlString ASGridButtonCollapse(this HtmlHelper helper, ASGridButtonCollapseModel model = null)
        {
            if (model == null)
                model = new ASGridButtonCollapseModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTools/ASGridButtonCollapse/ASGridButtonCollapse.cshtml", model);
        }
        #endregion

        #region ASGridButtonReloadGrid
        public static MvcHtmlString ASGridButtonReloadGrid(this HtmlHelper helper, ASGridButtonReloadGridModel model = null)
        {
            if (model == null)
                model = new ASGridButtonReloadGridModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTools/ASGridButtonReloadGrid/ASGridButtonReloadGrid.cshtml", model);
        }
        #endregion

        #region ASGridTable

        public static MvcHtmlString ASTable(this HtmlHelper helper, ASTableModel model = null)
        {
            if (model == null)
                model = new ASTableModel();

            return helper.Partial("~/Controls/ASTable/ASTable.cshtml", model);
        }

        public static MvcHtmlString ASGridTable(this HtmlHelper helper, ASGridTableModel model = null)
        {
            if (model == null)
                model = new ASGridTableModel();

            return helper.Partial("~/Controls/ASGrid/ASGridTable/ASGridTable.cshtml", model);
        }

        public static MvcHtmlString ASGridTable(this HtmlHelper helper, string asGridTitle, ASGridTableModel model = null)
        {
            if (model == null)
                model = new ASGridTableModel();

            model.AsGridTitleModel.Title = asGridTitle;

            return helper.Partial("~/Controls/ASGridTable/ASGridTable.cshtml", model);
        }

        public static MvcHtmlString ASGridTable(this HtmlHelper helper, string asGridTitle, string icon, ASGridTableModel model = null)
        {
            if (model == null)
                model = new ASGridTableModel();

            model.AsGridTitleModel.Title = asGridTitle;
            model.AsGridTitleModel.RenderIcon = true;
            model.AsGridTitleModel.Icon = icon;

            return helper.Partial("~/Controls/ASGridTable/ASGridTable.cshtml", model);
        }

        public static MvcHtmlString ASGridTable(this HtmlHelper helper, string asGridTitle, string icon, string resourceCode, ASGridTableModel model = null)
        {
            if (model == null)
                model = new ASGridTableModel();

            model.AsGridTitleModel.Title = asGridTitle;
            model.AsGridTitleModel.RenderIcon = true;
            model.AsGridTitleModel.Icon = icon;

            return helper.Partial("~/Controls/ASGrid/ASGridTable/ASGridTable.cshtml", model);
        }

        #endregion


        // AS Info
        public static MvcHtmlString ASInfoButtons(this HtmlHelper helper, string resource, bool isNewEntity, ASInfoButtonsModel model = null)
        {
            if (model == null)
                model = new ASInfoButtonsModel();

            if (isNewEntity)
                model.RenderASInfoButtonSave = PermissionsHelper.HasResourcePatentsPermissions(resource, Constants.PatentCode.ADD);
            else
                model.RenderASInfoButtonSave = PermissionsHelper.HasResourcePatentsPermissions(resource, Constants.PatentCode.EDIT);

            if (!isNewEntity)
                model.RenderASInfoButtonDelete = PermissionsHelper.HasResourcePatentsPermissions(resource, Constants.PatentCode.DELETE);

            return helper.Partial("~/Controls/ASInfo/ASInfoButtons/ASInfoButtons.cshtml", model);
        }

        public static MvcHtmlString ASInfoButtonSave(this HtmlHelper helper, ASInfoButtonSaveModel model = null)
        {
            if (model == null)
                model = new ASInfoButtonSaveModel();

            return helper.Partial("~/Controls/ASInfo/ASInfoButtons/ASInfoButton.cshtml", model);
        }

        public static MvcHtmlString ASInfoButtonDelete(this HtmlHelper helper, ASInfoButtonDeleteModel model = null)
        {
            if (model == null)
                model = new ASInfoButtonDeleteModel();

            return helper.Partial("~/Controls/ASInfo/ASInfoButtons/ASInfoButton.cshtml", model);
        }

        public static MvcHtmlString ASInfoButtonCancel(this HtmlHelper helper, ASInfoButtonCancelModel model = null)
        {
            if (model == null)
                model = new ASInfoButtonCancelModel();

            return helper.Partial("~/Controls/ASInfo/ASInfoButtons/ASInfoButton.cshtml", model);
        }

        public static IDictionary<string, object> AddAttribute(this IDictionary<string, object> htmlAttributes, string name, string value)
        {
            if (htmlAttributes.ContainsKey(name))
                htmlAttributes[name] += " " + value;
            else
                htmlAttributes.Add(name, value);

            return htmlAttributes;
        }


    }
}