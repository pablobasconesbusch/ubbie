﻿using Ubbie.WebSite.Controls.ASFileUpload;
using System.Collections.Generic;
using Ubbie.WebSite.Shared.Models;

namespace Ubbie.WebSite.Controls.ASMultipleFileUpload
{
    public class ASMultipleFileUpload : ASFileUploadBase
    {
        public List<int> FileIds { get; set; } = new List<int>();
        public List<FileModel> Files { get; set; } = new List<FileModel>();
    }

}
