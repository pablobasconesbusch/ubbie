﻿using MimeTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Shared.Models;

namespace Ubbie.WebSite.Shared.Controllers
{
    public class FileController : BaseController
    {
        [HttpPost]
        public JsonResult Upload(IEnumerable<HttpPostedFileBase> files)
        {
            var postedFile = Request
                .Files
                .Cast<string>()
                .Select(x => Request.Files[x])
                .ToArray()
                .Single();

            var saveMode = Request.Headers["fileSaveMode"];
            var fileSystemSaveFolder = Request.Headers["fileSystemSaveFolder"];

            var file = new Entidades.Soporte.File();
            file.FileName = postedFile.FileName;
            file.Extension = MimeTypeMap.GetExtension(postedFile.ContentType);
            file.MimeType = MimeTypeMap.GetMimeType(file.Extension);

            ServiceResponse sr;

            if (saveMode == Constants.FileSaveMode.DATABASE)
            {
                sr = UploadDatabase(file, postedFile);
            }
            else if (saveMode == Constants.FileSaveMode.AZURE)
            {
                sr = UploadAzure(file, postedFile);
            }
            else
            {
                sr = UploadFileSystem(file, postedFile, fileSystemSaveFolder);
            }

            var response = new FileInputResponseModel();
            if (sr.Status)
            {
                response.FileId = file.Id;
            }
            else
            {
                response.Error = "Error uploading the file";
                sr.Errors.ForEach(x => response.Error = x.ErrorMessage);
            }

            return Json(response);
        }

        private static ServiceResponse UploadDatabase(Entidades.Soporte.File file, HttpPostedFileBase postedFile)
        {
            file.Mode = Constants.FileSaveMode.DATABASE;
            using (var binaryReader = new BinaryReader(postedFile.InputStream))
            {
                file.FileContent = new FileContent
                {
                    RawData = binaryReader.ReadBytes(postedFile.ContentLength)
                };
            }
            return new FileService().SaveEntity(file);
        }

        private static ServiceResponse UploadAzure(Entidades.Soporte.File file, HttpPostedFileBase postedFile)
        {
            file.Guid = $"{Guid.NewGuid()}{file.Extension}";
            file.Mode = Constants.FileSaveMode.AZURE;

            var blobDocument = Configuration.Application.AzureUploadsContainer.GetBlockBlobReference(file.Guid);
            blobDocument.Properties.ContentType = file.MimeType;

            var sr = new FileService().SaveEntity(file);
            if (sr.Status)
            {
                using (var br = new BinaryReader(postedFile.InputStream))
                {
                    blobDocument.UploadFromStream(br.BaseStream);
                }
                file.Id = sr.ReturnValue;
            }
            return sr;
        }

        private static ServiceResponse UploadFileSystem(Entidades.Soporte.File file, HttpPostedFileBase postedFile, string fileSystemSaveFolder)
        {
            var uploadsFolder = Configuration.Application.UploadsFolder;
            Directory.CreateDirectory($"{uploadsFolder}/{fileSystemSaveFolder}");
            file.Mode = Constants.FileSaveMode.FILESYSTEM;

            do
            {
                file.Guid = $"{fileSystemSaveFolder}/{Guid.NewGuid()}{file.Extension}";
            } while (System.IO.File.Exists($"{Configuration.Application.UploadsFolder}/{file.Guid}"));

            var sr = new FileService().SaveEntity(file);
            if (sr.Status)
            {
                try
                {
                    using (var br = new BinaryReader(postedFile.InputStream))
                    {
                        using (var fileStream = System.IO.File.Create($"{uploadsFolder}/{file.Guid}"))
                        {
                            fileStream.Write(br.ReadBytes(postedFile.ContentLength), 0, postedFile.ContentLength);
                        }
                    }
                    file.Id = sr.ReturnValue;
                }
                catch (Exception ex)
                {
                    sr.AddError(ex);
                }
            }

            return sr;
        }

        [HttpPost]
        public JsonResult Delete()
        {
            var fileId = Request["key"].ToInt();
            new FileService().DeleteEntity(fileId);
            return Json(new { });
        }



        [OutputCache(Duration = int.MaxValue, VaryByParam = "key", Location = OutputCacheLocation.ServerAndClient)]
        public new FileContentResult View(string key)
        {
            var file = ProcessFile(key);
            return File(file.FileContent.RawData, file.MimeType);
        }

        [OutputCache(Duration = int.MaxValue, VaryByParam = "key", Location = OutputCacheLocation.ServerAndClient)]
        public FileContentResult Download(string key)
        {
            var file = ProcessFile(key);
            return File(file.FileContent.RawData, MediaTypeNames.Application.Octet);
        }

        private FileModel ProcessFile(string key)
        {
            var fileId = CustomEncrypt.Decrypt(key).ToInt();
            if (fileId == 0)
                return null;

            var file = new Models.FileModel(fileId);
            if (file.Mode == Constants.FileSaveMode.AZURE)
            {
                if (string.IsNullOrEmpty(file.Guid))
                    return null;

                var blob = Configuration.Application.AzureUploadsContainer.GetBlockBlobReference(file.Guid);
                if (!blob.Exists())
                    return null;

                using (var ms = new MemoryStream())
                {
                    blob.DownloadToStream(ms);
                    file.FileContent = new FileContentModel
                    {
                        RawData = ms.ToArray()
                    };
                }
            }
            else if (file.Mode == Constants.FileSaveMode.FILESYSTEM)
            {
                if (string.IsNullOrEmpty(file.Guid))
                    return null;

                var pathExists = new FileInfo(file.Path).Exists;
                file.FileContent = new FileContentModel
                {
                    RawData = pathExists ? System.IO.File.ReadAllBytes(file.Path) : System.IO.File.ReadAllBytes(Configuration.Application.MissingImagePath)
                };

            }
            Response.AppendHeader("Content-Disposition", "inline; filename=" + file.FileName);
            return file;
        }


    }
}