﻿using System.Reflection;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;

namespace Ubbie.WebSite.Shared.Controllers
{
    public class GlobalController : BaseController
    {
        public string URLs()
        {
            return UrlGenerator.GenerateUrls(Assembly.GetExecutingAssembly());
        }
    }
}