﻿namespace Ubbie.WebSite.Shared.Models
{
	public class Select2Result
	{
	    public string id { get; set; }
	    public string text { get; set; }
	}
}