﻿using System.IO;
using Ubbie.Framework.Mapping;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;
using File = Ubbie.Entidades.Soporte.File;

namespace Ubbie.WebSite.Shared.Models
{
    public class FileModel : MapperModel<FileModel, File, int>
    {
        public int Id { get; set; }

        public string FileName { get; set; }
        public string Extension { get; set; }

        public int? FileContentId { get; set; }
        public FileContentModel FileContent { get; set; }

        public string MimeType { get; set; }
        public string Folder { get; set; }
        public string Guid { get; set; }
        public string Mode { get; set; }

        public string Key => CustomEncrypt.Encrypt(Id);
        public string Path => $"{Configuration.Application.UploadsFolder}/{Folder}/{Guid}";





        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(File entity)
        {
            if (entity.Mode == Constants.FileSaveMode.DATABASE)
            {
                FileContent = new FileContentModel(entity.FileContent);
            }
            else if (entity.Mode == Constants.FileSaveMode.AZURE)
            {
                var blob = Configuration.Application.AzureUploadsContainer.GetBlockBlobReference(entity.Guid);

                using (var ms = new MemoryStream())
                {
                    blob.DownloadToStream(ms);
                    FileContent = new FileContentModel
                    {
                        Length = (int)ms.Length
                    };
                }
            }
            else if (entity.Mode == Constants.FileSaveMode.FILESYSTEM)
            {
                FileContent = new FileContentModel
                {
                    Length = new FileInfo(Path).Length
                };
            }
        }

        protected override void ExtraMapToEntity(File entity)
        {
        }

        public FileModel(int entityId) : base(entityId, x => new FileService().Get(entityId))
        {

        }

    }
}
