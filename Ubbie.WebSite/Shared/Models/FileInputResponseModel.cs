﻿namespace Ubbie.WebSite.Shared.Models
{
	public class FileInputResponseModel
    {
		public int FileId{ get; set; }
		public string Error { get; set; }
	}
}