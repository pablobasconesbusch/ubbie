﻿namespace Ubbie.WebSite.Shared.Models
{
	public class ValidationSuccessModel
	{
		public string Message { get; set; }
	    public string Icon { get; set; } = "fa fa-check";
	    public string Size { get; set; }
	}
}