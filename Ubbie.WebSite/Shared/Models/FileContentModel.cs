﻿
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Mapping;

namespace Ubbie.WebSite.Shared.Models
{
    public class FileContentModel : MapperModel<FileContentModel, FileContent, int>
    {
        public int Id { get; set; }
        public byte[] RawData { get; set; }
        public long Length { get; set; }

        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(FileContent entity)
        {
            Length = RawData.Length;
        }

        protected override void ExtraMapToEntity(FileContent entity)
        {
        }

        public FileContentModel()
        {

        }

        public FileContentModel(FileContent entity) : base(entity)
        {

        }
    }
}
