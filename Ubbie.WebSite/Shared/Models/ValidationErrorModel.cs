﻿using System.Collections.Generic;
using System.Web.Mvc;
using Ubbie.Servicios.Base;

namespace Ubbie.WebSite.Shared.Models
{
    public class ValidationErrorModel
    {
        public List<ServiceError> Errors = new List<ServiceError>();
        public string Title;

        public ValidationErrorModel()
        {

        }

        public ValidationErrorModel(string errorMessage)
        {
            Errors.Add(new ServiceError(errorMessage));
            Title = "Error Summary";
        }

        public ValidationErrorModel(ModelStateDictionary m)
        {
            if (!m.IsValid)
            {
                Title = "Error Summary";

                foreach (var modelState in m.Values)
                {
                    foreach (var error in modelState.Errors)
                        Errors.Add(new ServiceError(error.ErrorMessage));
                }
            }
        }

        public ValidationErrorModel(ServiceResponse sr)
        {
            if (!sr.Status)
            {
                Title = "Error Summary";

                foreach (var se in sr.Errors)
                    Errors.Add(new ServiceError(se.ErrorMessage));
            }
        }
    }
}