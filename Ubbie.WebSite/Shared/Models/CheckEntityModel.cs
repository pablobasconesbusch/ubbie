﻿namespace Ubbie.WebSite.Shared.Models
{
    public class CheckEntityModel<K>
    {
        public K Key { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}