﻿using System.Collections.Generic;
using Ubbie.Entidades.Seguridad;

namespace Ubbie.WebSite.Shared.Layouts.SidebarLayout.Notifications
{
    public class NotificationsModel
    {
        public List<UsuarioNotificacion> Notificationes { get; set; }
    }
}
