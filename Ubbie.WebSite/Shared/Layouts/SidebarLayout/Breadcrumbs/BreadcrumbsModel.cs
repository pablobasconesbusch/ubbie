﻿using System.Collections.Generic;

namespace Ubbie.WebSite.Shared.Layouts.SidebarLayout.Breadcrumbs
{
    public class BreadcrumbsModel
    {
        public List<BreadcrumbItemModel> Breadcrumbs { get; set; }
        public string LastBreadcrumb { get; set; }
    }
}
