﻿namespace Ubbie.WebSite.Shared.Layouts.SidebarLayout.Breadcrumbs
{
    public class BreadcrumbItemModel
    {
        public string Url { get; set; }
        public string Name { get; set; }

        public BreadcrumbItemModel() { }

        public BreadcrumbItemModel(string name, string url)
        {
            Name = name;
            Url = url;
        }
    }
}
