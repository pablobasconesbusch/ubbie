﻿using System.Collections.Generic;
using System.Linq;

namespace Ubbie.WebSite.Shared.Layouts.SidebarLayout.Menu
{
    public class MenuItemModel
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public bool HasChildren => Children.Any();
        public List<MenuItemModel> Children { get; set; } = new List<MenuItemModel>();



        public MenuItemModel()
        {
        }

        public MenuItemModel(string text, string url) : this(text, url, "")
        {
        }


        public MenuItemModel(string text, string url, string icon)
        {
            Text = text;
            Url = url;
            Icon = icon;
        }
    }
}
