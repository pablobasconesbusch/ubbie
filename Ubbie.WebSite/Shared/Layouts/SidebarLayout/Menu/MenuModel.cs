﻿using System.Collections.Generic;

namespace Ubbie.WebSite.Shared.Layouts.SidebarLayout.Menu
{
    public class MenuModel
    {
        public List<MenuItemModel> MenuItems { get; set; }
    }
}
