﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Ubbie.Framework.Seguridad;
using Ubbie.Recursos;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Shared.Layouts.SidebarLayout.Breadcrumbs;
using Ubbie.WebSite.Shared.Layouts.SidebarLayout.Header;
using Ubbie.WebSite.Shared.Layouts.SidebarLayout.Menu;

namespace Ubbie.WebSite.Shared.Layouts
{
    public static class LayoutHelper
    {
        public static MvcHtmlString RenderHeader(this HtmlHelper helper)
        {
            var model = new HeaderModel();
            return helper.Partial("~/Shared/Layouts/SidebarLayout/Header/Header.cshtml", model);
        }

        public static MvcHtmlString RenderMenu(this HtmlHelper helper)
        {
            var activeUsuario = AppInfo.UsuarioActivo;

            var model = new MenuModel();
            model.MenuItems = new List<MenuItemModel>();

            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.DASHBOARD))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Statistics, urlHelper.Action("Index", "Dashboard"), "icon-clock"));


            if (PermissionsHelper.HasRolPermissions(Constants.RoleCode.INQUILINO))
                model.MenuItems.Add(new MenuItemModel("Balance", urlHelper.Action("MisPagos", "Balance"), "fa fa-usd"));

            if (PermissionsHelper.HasRolPermissions(Constants.RoleCode.INQUILINO))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_MyPayments, urlHelper.Action("PagarExpensas", "Balance"), "fa fa-usd"));
            
            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.USER, Constants.ResourceCode.ROLE))
            {
                model.MenuItems.Add(new MenuItemModel($"{Translation.Label_Users} {Translation.Label_And} {Translation.Label_Profiles}", "#", "icon-users"));

                if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.USER))
                    model.MenuItems.Last().Children.Add(new MenuItemModel(Translation.Label_Users, urlHelper.Action("Index", "Usuario")));

                if (PermissionsHelper.HasResourcePatentsPermissions(Constants.ResourceCode.USER, Constants.PatentCode.USUARIO_PATENTE))
                    model.MenuItems.Last().Children.Add(new MenuItemModel(Translation.Label_UserPatents, urlHelper.Action("PatenteIndex", "Usuario")));

                if (activeUsuario.EsSuperUsuario || Configuration.Security.EnableRoleManagement && PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.ROLE))
                    model.MenuItems.Last().Children.Add(new MenuItemModel(Translation.Label_Profiles, urlHelper.Action("Index", "Rol")));
            }

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.BITACORA))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Log, urlHelper.Action("Index", "Bitacora"), "fa fa-history"));

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.BITACORA_HISTORICO))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_LogHistory, urlHelper.Action("HistoricoIndex", "Bitacora"), "fa fa-history"));

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.NOTICIA))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_News, urlHelper.Action("Index", "Noticia"), "fa fa-newspaper-o"));


            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.RECLAMO))
            {
                model.MenuItems.Add(new MenuItemModel($"{Translation.Label_Complaints}", "#", "fa fa-comments-o"));

                if (PermissionsHelper.HasResourcePatentsPermissions(Constants.ResourceCode.RECLAMO, Constants.PatentCode.ADD))
                    model.MenuItems.Last().Children.Add(new MenuItemModel(Translation.Label_Complaints, urlHelper.Action("Index", "Reclamo"), "fa fa-comments-o"));

                if (PermissionsHelper.HasResourcePatentsPermissions(Constants.ResourceCode.RECLAMO, Constants.PatentCode.APPROVE))
                    model.MenuItems.Last().Children.Add(new MenuItemModel(Translation.Label_ResolveComplaints, urlHelper.Action("AdminIndex", "Reclamo"), "fa fa-check-square-o"));
            }

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.SEGURIDAD))
            {
                if (PermissionsHelper.HasResourcePatentsPermissions(Constants.ResourceCode.SEGURIDAD, Constants.PatentCode.BACKUP))
                    model.MenuItems.Add(new MenuItemModel("Backup", urlHelper.Action("Backup", "Seguridad"), "fa fa-hdd-o"));

                if (PermissionsHelper.HasResourcePatentsPermissions(Constants.ResourceCode.SEGURIDAD, Constants.PatentCode.RESTORE))
                    model.MenuItems.Add(new MenuItemModel("Restore", urlHelper.Action("Restore", "Seguridad"), "fa fa-undo"));
            }

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.CONSORCIO))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Consorcio, urlHelper.Action("Index", "Consorcio"), "fa fa-building-o"));

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.UNIDAD))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Unity, urlHelper.Action("Index", "Unidad"), "fa fa-home"));

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.GASTO))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Spendings, urlHelper.Action("Index", "Gasto"), "fa fa-money"));

            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.EXPENSAS))
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Expenses, urlHelper.Action("Index", "Expensa"), "fa fa-percent"));


            if (PermissionsHelper.HasResourcePermissions(Constants.ResourceCode.BITACORA))
                model.MenuItems.Add(new MenuItemModel("XML", urlHelper.Action("Index", "Exportar"), "fa fa-file-excel-o"));

            // Superuser Items
            if (activeUsuario.EsSuperUsuario)
                model.MenuItems.Add(new MenuItemModel(Translation.Label_Support, urlHelper.Action("Index", "Soporte"), "fa fa-terminal"));


            return helper.Partial("~/Shared/Layouts/SidebarLayout/Menu/Menu.cshtml", model);
        }
        public static MvcHtmlString RenderBreadcrumbs(this HtmlHelper helper, List<BreadcrumbItemModel> breadcrumbs, string lastBreadcrumb)
        {
            var model = new BreadcrumbsModel();
            model.Breadcrumbs = breadcrumbs;
            model.LastBreadcrumb = lastBreadcrumb;

            return helper.Partial("~/Shared/Layouts/SidebarLayout/Breadcrumbs/Breadcrumbs.cshtml", model);
        }

        public static MvcHtmlString RenderUsuarioAvatar(this HtmlHelper helper, string classes)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            var activeUsuario = AppInfo.UsuarioActivo;

            if (activeUsuario.ImagenPerfilId.HasValue)
                return new MvcHtmlString($"<img alt=\"{activeUsuario.NombreCompleto}\" class=\"{classes}\" src=\"{urlHelper.Action("View", "File", new { key = CustomEncrypt.Encrypt(activeUsuario.ImagenPerfilId.Value.ToString()) })}\" />");

            return new MvcHtmlString($"<img alt=\"{activeUsuario.NombreCompleto}\" class=\"{classes}\" src=\"{urlHelper.Content("~/Content/img/avatar.png")}\" />");
        }


        public static MvcHtmlString RenderCompanyLogo(this HtmlHelper helper, string classes)
        {
            var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            //if (string.IsNullOrWhiteSpace(Configuration.Application.SiteLogoUrl))
            return new MvcHtmlString($"<img alt=\"{Configuration.Application.ApplicationName}\" class=\"{classes}\" src=\"{urlHelper.Content("~/Content/images/ubbie-logo.jpg")}\" />");

            //return new MvcHtmlString($"<img alt=\"{Configuration.Application.ApplicationName}\" class=\"{classes}\" src=\"{Configuration.Application.SiteLogoUrl}\" />");
        }

    }
}
