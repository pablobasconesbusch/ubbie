﻿using System.Security.Principal;
using Ubbie.WebSite.Aplicacion.Code;

namespace Ubbie.WebSite.Aplicacion.Autenticacion
{
    public class UsuarioAutenticado : IPrincipal
    {
        public IIdentity Identity { get; }
        public int UsuarioId { get; set; }
        public int SiteId { get; set; }

        public UsuarioAutenticado(string email)
        {
            Identity = new GenericIdentity(email);
        }

        public bool IsInRole(string role)
        {
            return role == AppInfo.UsuarioActivo.CodigoRol;
        }

    }
}