﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Security;
using Ubbie.Entidades.Seguridad;
using Ubbie.Framework.Extensiones;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Autorizacion;
using Ubbie.WebSite.Aplicacion.Code;

namespace Ubbie.WebSite.Aplicacion.Autenticacion
{
    public class AuthService
    {
        public ServiceResponse Login(int usuarioId, bool rememberMe)
        {
            var usuario = new ServicioUsuario().Get(usuarioId);
            return Login(usuario, rememberMe);
        }
        public ServiceResponse Login(Usuario usuario, bool rememberMe)
        {
            var sr = new ServiceResponse();

            var serializeModel = new SerializableUsuario
            {
                UsuarioId = usuario.Id,
            };

            var usuarioData = JsonConvert.SerializeObject(serializeModel);
            var authTicket = new FormsAuthenticationTicket(1, usuario.Email, DateTime.Now, DateTime.Now.AddMinutes(Configuration.Security.CookieExpirationTime), rememberMe, usuarioData);
            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

            HttpContext.Current.Response.Cookies.Add(authCookie);

            var authUsuario = new UsuarioAutenticado(usuario.Email)
            {
                UsuarioId = usuario.Id,
            };
            HttpContext.Current.User = authUsuario;

            CacheManager.ObtenerUsuarioActivo(true);
            sr.Data = usuario;
            return sr;
        }


        public void Logout()
        {
            HttpContext.Current.Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty) { Expires = DateTime.UtcNow.AddYears(-1) });
        }
        public static void DeserializeActiveUser()
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
                return;

            var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            if (authTicket.Expired)
            {
                new AuthService().Logout();
                UnauthorizedResults.CreateSessionExpiredResult();
            }

            var serializeModel = JsonConvert.DeserializeObject<SerializableUsuario>(authTicket.UserData);
            if (authTicket.UserData.IsNullOrEmpty())
                return;

            var usuarioActivo = new UsuarioAutenticado(authTicket.Name)
            {
                UsuarioId = serializeModel.UsuarioId,
            };

            HttpContext.Current.User = usuarioActivo;
        }
    }
}
