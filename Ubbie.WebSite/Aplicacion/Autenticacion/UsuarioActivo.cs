﻿using System.Collections.Generic;
using Ubbie.Entidades.Seguridad;

namespace Ubbie.WebSite.Aplicacion.Autenticacion
{
    public class UsuarioActivo
    {
        public int Id { get; set; }
        public string Email { get; set; }

        public int? SiteId { get; set; }
        public int? SiteLogoFileId { get; set; }

        public int? ImagenPerfilId { get; set; }

        public string TimeZoneCodigo { get; set; }
        public string TimeZoneNombre { get; set; }

        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string NombreCompleto => $"{Nombre} {Apellido}";

        public string CodigoRol { get; set; }

        public string DireccionIP { get; set; }
        public bool EsSuperUsuario { get; set; }

        public List<RolFamilia> RolFamilias { get; set; }
    }
}