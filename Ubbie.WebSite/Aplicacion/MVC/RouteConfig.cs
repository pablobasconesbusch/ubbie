﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Ubbie.WebSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            // Ignore axd resources
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Attribute Routing
            routes.MapMvcAttributeRoutes();

            // Default route
            routes.MapRoute(
                "Content",
                "Post/{action}/{id}",
                new { controller = "Content", action = "Index", id = UrlParameter.Optional }
            );

            // Default route
            routes.MapRoute(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
