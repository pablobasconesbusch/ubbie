﻿using Newtonsoft.Json;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.MVC.Clases
{
    public class JsonNewtonsoftResult : JsonResult
    {


        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;

            if (string.IsNullOrEmpty(ContentType))
                response.ContentType = "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null)
                JsonSerializer.CreateDefault().Serialize(response.Output, Data);
        }
    }
}
