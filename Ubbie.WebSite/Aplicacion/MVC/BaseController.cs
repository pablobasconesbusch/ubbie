﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Ubbie.Servicios.Base;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.Helpers;
using Ubbie.WebSite.Aplicacion.MVC.Atributos;

namespace Ubbie.WebSite.Aplicacion.MVC
{
    public class BaseController : Controller
    {
        public Autenticacion.UsuarioActivo UsuarioActivo { get; set; } = AppInfo.UsuarioActivo;

        #region Views

        protected new ViewResult View()
        {
            return View(GetFullCshtml());
        }

        protected new ViewResult View(string viewName)
        {
            return View(viewName, null);
        }

        protected new ViewResult View(object model)
        {
            return View(GetFullCshtml(), model);
        }

        protected new ViewResult View(string viewName, object model)
        {
            return base.View(ViewPath(viewName), model);
        }

        #endregion


        #region PartialViews

        protected new JsonResult PartialView()
        {
            return PartialView(GetFullCshtml(), null);
        }

        protected new JsonResult PartialView(string viewName)
        {
            return PartialView(viewName, null);
        }

        protected new JsonResult PartialView(object model)
        {
            return PartialView(GetFullCshtml(), model);
        }

        protected new JsonResult PartialView(string viewName, object model)
        {
            var sr = new ServiceResponse();
            sr.HtmlViews.Add(this.RenderRazorViewToString(ViewPath(viewName), model));
            return PartialView(sr);
        }

        protected JsonResult PartialView(ServiceResponse sr)
        {
            return Json(sr);
        }


        #endregion


        #region Methods

        protected string ViewPath(string viewName)
        {
            if (!viewName.ToLower().EndsWith(".cshtml"))
                viewName += ".cshtml";

            if (viewName.StartsWith("~"))
                return viewName;

            if (!GetType().GetCustomAttributes(typeof(ViewsPath), false).Any())
                return viewName;

            var attribute = (ViewsPath)GetType().GetCustomAttributes(typeof(ViewsPath), false).FirstOrDefault();

            return attribute?.Path + "/" + viewName;
        }


        private string GetFullCshtml()
        {
            if (ControllerContext.RouteData.Values["action"] == null)
                return GetControllerName() + "Index";

            return GetControllerName() + ControllerContext.RouteData.Values["action"];
        }

        private string GetControllerName()
        {
            return ControllerContext.RouteData.Values["controller"].ToString();
        }

        protected List<ServiceError> GetModelStateControlErrors()
        {
            var ret = new List<ServiceError>();
            ModelState.Where(x => x.Value.Errors.Count > 0).ToList().ForEach(x => x.Value.Errors.ToList().ForEach(y => ret.Add(new ServiceError(x.Key, y.ErrorMessage))));
            return ret;
        }

        protected JsonResult ModelStateErrors()
        {
            var sr = new ServiceResponse();

            sr.ReturnValue = -1;
            sr.Errors = ModelState.Where(x => x.Value.Errors.Any()).SelectMany(x => x.Value.Errors.Select(y => new ServiceError(x.Key, y.ErrorMessage)).ToList()).ToList();

            return Json(sr);
        }


        #endregion


        public class XmlResult : ActionResult
        {
            private object objectToSerialize;

            /// <summary>
            /// Initializes a new instance of the <see cref="XmlResult"/> class.
            /// </summary>
            /// <param name="objectToSerialize">The object to serialize to XML.</param>
            public XmlResult(object objectToSerialize)
            {
                this.objectToSerialize = objectToSerialize;
            }

            /// <summary>
            /// Gets the object to be serialized to XML.
            /// </summary>
            public object ObjectToSerialize
            {
                get { return this.objectToSerialize; }
            }

            /// <summary>
            /// Serialises the object that was passed into the constructor to XML and writes the corresponding XML to the result stream.
            /// </summary>
            /// <param name="context">The controller context for the current request.</param>
            public override void ExecuteResult(ControllerContext context)
            {
                if (this.objectToSerialize != null)
                {
                    context.HttpContext.Response.Clear();
                    var xs = new System.Xml.Serialization.XmlSerializer(this.objectToSerialize.GetType());
                    context.HttpContext.Response.ContentType = "text/xml";
                    xs.Serialize(context.HttpContext.Response.Output, this.objectToSerialize);
                }
            }
        }

        public sealed class XmlActionResult : ActionResult
        {
            private readonly XDocument _document;

            public Formatting Formatting { get; set; }
            public string MimeType { get; set; }

            public XmlActionResult(XDocument document)
            {
                if (document == null)
                    throw new ArgumentNullException("document");

                _document = document;

                // Default values
                MimeType = "text/xml";
                Formatting = Formatting.None;
            }

            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.Clear();
                context.HttpContext.Response.ContentType = MimeType;

                using (var writer = new XmlTextWriter(context.HttpContext.Response.OutputStream, Encoding.UTF8) { Formatting = Formatting })
                    _document.WriteTo(writer);
            }
        }

    }
}