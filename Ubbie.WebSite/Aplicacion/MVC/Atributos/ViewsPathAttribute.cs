﻿using System;

namespace Ubbie.WebSite.Aplicacion.MVC.Atributos
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ViewsPath : Attribute
    {
        public string Path { get; set; }

        public ViewsPath(string path)
        {
            Path = path;
        }
    }
}