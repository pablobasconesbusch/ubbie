﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Recursos;

namespace Ubbie.WebSite.Aplicacion.MVC.Atributos
{
    public class ASRequired : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            return new[] {
                new ModelClientValidationRule
                {
                    ErrorMessage = Translation.Validation_RequiredFieldClientSide,
                    ValidationType = "required"
                }
            };
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(value?.ToString()))
                return new ValidationResult(string.Format(Translation.Validation_RequiredField, validationContext.DisplayName));

            return ValidationResult.Success;
        }
    }
}

