﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Ubbie.Recursos;

namespace Ubbie.WebSite.Aplicacion.MVC.Atributos
{
    public class ASRequiredDropdown : ValidationAttribute, IClientValidatable
    {
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rangeRule = new ModelClientValidationRule
            {
                ErrorMessage = Translation.Validation_RequiredFieldClientSide,
                ValidationType = "range",
            };

            rangeRule.ValidationParameters.Add("min", 1);

            return new[] {
                rangeRule
            };
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null || (int)value == 0)
                return new ValidationResult(string.Format(Translation.Validation_RequiredField, validationContext.DisplayName));

            return ValidationResult.Success;
        }
    }
}

