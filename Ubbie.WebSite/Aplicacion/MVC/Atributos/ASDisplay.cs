﻿using System.ComponentModel;
using Ubbie.Recursos;

namespace Ubbie.WebSite.Aplicacion.MVC.Atributos
{
    public class ASDisplay : DisplayNameAttribute
    {
        public ASDisplay(string resourceId)
            : base(GetMessageFromResource(resourceId))
        { }

        private static string GetMessageFromResource(string resourceId)
        {
            return Translation.ResourceManager.GetString(resourceId);
        }
    }
}