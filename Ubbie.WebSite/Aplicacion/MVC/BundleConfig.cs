﻿using System.Web.Optimization;

namespace Ubbie.WebSite.Aplicacion.MVC
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css/bundle")
                .Include("~/Content/vendor/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Content/metronic/global/plugins/simple-line-icons/simple-line-icons.css", new CssRewriteUrlTransform())

                .Include(
                    "~/Content/css/bootstrap.css",
                    "~/Content/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.css",
                    "~/Content/metronic/global/plugins/datatables/datatables.css",
                    "~/Content/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
                    "~/Content/metronic/global/plugins/bootstrap-select/css/bootstrap-select.css",
                    "~/Content/metronic/global/plugins/select2/css/select2.css",
                    "~/Content/metronic/global/plugins/select2/css/select2-bootstrap.css",
                    "~/Content/metronic/global/plugins/bootstrap-toastr/toastr.css",
                    "~/Content/vendor/sweetalert2/sweetalert2.css",
                    "~/Content/vendor/css-spinners/css/spinners.css",
                    "~/Content/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css",
                    "~/Content/vendor/textarea-autosize/textarea.required.css",
                    "~/Content/vendor/dropzone-js/dropzone.css",
                    "~/Content/vendor/animate.css/animate.css")

                .Include(
                    "~/Content/metronic/global/css/components.css",
                    "~/Content/metronic/global/css/plugins.css",
                    "~/Content/metronic/layouts/layout/css/themes/light.css",
                    "~/Content/metronic/pages/css/error.css",
                    "~/Content/metronic/pages/css/login.css",
                    "~/Content/metronic/pages/css/profile.css",
                    "~/Content/css/metronic.css",
                    "~/Content/css/app.css"
                ));


            bundles.Add(new ScriptBundle("~/Content/js/bundle").Include(
                "~/Content/metronic/global/plugins/bootstrap/js/bootstrap.js",
                "~/Content/metronic/global/plugins/js.cookie.js",
                "~/Content/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.js",
                "~/Content/metronic/global/plugins/jquery.blockui.js",
                "~/Content/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.js",

                "~/Content/metronic/global/plugins/jquery-validation/js/jquery.validate.js",
                "~/Content/metronic/global/plugins/jquery-validation/js/additional-methods.js",
                "~/Content/vendor/jquery-validation-unobtrusive/jquery.validate.unobtrusive.js",
                "~/Content/vendor/jquery.validate.unobtrusive.bootstrap/jquery.validate.unobtrusive.bootstrap.js",
                "~/Content/metronic/global/plugins/datatables/datatables.js",
                "~/Content/metronic/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
                "~/Content/metronic/pages/scripts/table-datatables-responsive.js",
                "~/Content/metronic/global/plugins/bootstrap-select/js/bootstrap-select.js",
                "~/Content/metronic/global/plugins/select2/js/select2.js",
                "~/Content/metronic/global/plugins/bootbox/bootbox.js",
                "~/Content/metronic/global/plugins/bootstrap-toastr/toastr.js",
                "~/Content/vendor/sweetalert2/sweetalert2.js",
                "~/Content/vendor/dropzone-js/dropzone.js",
                "~/Content/vendor/autonumeric/autoNumeric.js",
                "~/Content/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js",
                "~/Content/vendor/bootstrap-datepicker/locales/bootstrap-datepicker.es.js",
                "~/Content/vendor/textarea-autosize/jquery.textarea_autosize.js",
                "~/Content/vendor/jquery.typewatch/jquery.typewatch.js",
                "~/Content/vendor/remarkable-bootstrap-notify/bootstrap-notify.js",
                "~/Content/vendor/devbridge-autocomplete/jquery.autocomplete.js",
                "~/Content/vendor/printThis/printThis.js",
                "~/Content/vendor/jquery-inputmask/dist/jquery.inputmask.bundle.js",

                "~/Content/metronic/global/scripts/app.js",
                "~/Content/metronic/layouts/layout/scripts/layout.js",
                "~/Content/metronic/layouts/global/scripts/quick-sidebar.js",
                "~/Content/metronic/layouts/global/scripts/quick-nav.js",
                "~/Content/metronic/pages/scripts/login.js",

                "~/Content/js/ASApp.js",
                "~/Content/js/ASApp.Ajax.js",
                "~/Content/js/ASApp.Global.js",
                "~/Content/js/ASApp.Plugins.js",
                "~/Content/js/ASApp.Utils.js",
                "~/Content/js/ASApp.Notify.js",
                "~/Content/js/ASApp.Controls.js",
                "~/Content/js/ASApp.InfoView.js",
                "~/Content/js/ASApp.GridView.js",
                "~/Content/js/ASApp.InfoView.js",
                "~/Content/js/ASApp.Session.js",

                "~/Controls/ASFileUpload/ASFileUpload.js"));

            //BundleTable.EnableOptimizations = true;

        }
    }
}
