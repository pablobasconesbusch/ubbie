﻿using System.Text;
using System.Web;
using System.Web.Mvc;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Soporte;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC.Clases;

namespace Ubbie.WebSite.Aplicacion.MVC.Filtros
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new JsonNewtonsoftFilter());
            filters.Add(new ServiceActiveUsuarioFilter());
            filters.Add(new ExceptionHandlingFilter());
        }
    }

    public class ExceptionHandlingFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var sb = new StringBuilder();
            var exception = filterContext.Exception;
            sb.AppendLine(exception.ToString());

            while (exception.InnerException != null)
            {
                sb.AppendLine("------------------- INNER EXCEPTION -------------------");
                sb.AppendLine(exception.InnerException.ToString());
                exception = exception.InnerException;
            }

            var eventLog = new EventLogModel(Constants.EventCode.APP, sb.ToString(), HttpContext.Current).ToEntity();
            EventLogService.Error(eventLog);
        }
    }

    public class JsonNewtonsoftFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var jsonResult = filterContext.Result as JsonResult;
            if (jsonResult != null)
            {
                filterContext.Result = new JsonNewtonsoftResult
                {
                    ContentEncoding = jsonResult.ContentEncoding,
                    JsonRequestBehavior = jsonResult.JsonRequestBehavior,
                    ContentType = jsonResult.ContentType,
                    Data = jsonResult.Data
                };
            }

            base.OnActionExecuted(filterContext);

        }
    }

    public class ServiceActiveUsuarioFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var usuarioActivo = AppInfo.UsuarioActivo;

            if (usuarioActivo == null)
                return;

            var usuarioActivoServicio = new UsuarioActivo
            {
                Id = usuarioActivo.Id,
                DireccionIP = usuarioActivo.DireccionIP,
            };
            ServicioBase.SetActiveUser(usuarioActivoServicio);
        }
    }
}
