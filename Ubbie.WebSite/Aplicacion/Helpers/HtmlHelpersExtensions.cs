﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using Ubbie.WebSite.Aplicacion.Code;

namespace Ubbie.WebSite.Aplicacion.Helpers
{
    public static class HtmlHelperExtensions
    {

        #region DropDownList
        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString() }).ToList();

            return new SelectList(list, "Value", "Text");
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text, int? selectedValue)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString() }).ToList();

            return new SelectList(list, "Value", "Text", selectedValue);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text, SelectListItem selectedItem)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString() }).ToList();

            list.Insert(0, selectedItem);

            return new SelectList(list, "Value", "Text", selectedItem.Value);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text, SelectListItem selectedItem, int? selectedValue)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString() }).ToList();

            list.Insert(0, selectedItem);

            return new SelectList(list, "Value", "Text", selectedValue);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text, Func<T, string> optGroup)
        {
            var list = enumerable
                .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString(), Group = new SelectListGroup { Name = optGroup(x) } }).ToList();

            return new SelectList(list, "Value", "Text", "Group.Name", (object)null);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, int> value, Func<T, string> text, Func<T, string> optGroup, int? selectedValue)
        {
            var list = enumerable
                .Select(x => new SelectListItem { Text = text(x), Value = value(x).ToString(), Group = new SelectListGroup { Name = optGroup(x) } }).ToList();

            return new SelectList(list, "Value", "Text", "Group", selectedValue);
        }



        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> value, Func<T, string> text)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x) }).ToList();

            return new SelectList(list, "Value", "Text");
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> value, Func<T, string> text, string selectedValue)
        {
            var list = enumerable
              .Select(x => new SelectListItem { Text = text(x), Value = value(x) }).ToList();

            return new SelectList(list, "Value", "Text", selectedValue);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> value, Func<T, string> text, Func<T, string> optGroup)
        {
            var list = enumerable
                .Select(x => new SelectListItem
                {
                    Text = text(x),
                    Value = value(x),
                    Group = new SelectListGroup
                    {
                        Name = optGroup(x)
                    }
                }).ToList();

            return new SelectList(list, "Value", "Text", "Group.Name", (object)null);
        }

        public static SelectList ToSelectList<T>(this IEnumerable<T> enumerable, Func<T, string> value, Func<T, string> text, Func<T, string> optGroup, string selectedValue)
        {
            var list = enumerable
                .Select(x => new SelectListItem { Text = text(x), Value = value(x), Group = new SelectListGroup { Name = optGroup(x) } }).ToList();

            return new SelectList(list, "Value", "Text", "Group", (object)selectedValue);
        }

        #endregion

        #region ModelHelpers

        public static bool HasCustomAttribute<A>(this HtmlHelper helper, object model, Expression<Func<object>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            return model.GetType().GetProperty(memberExpression.Member.Name).GetCustomAttributes(typeof(A), false).Any();
        }

        public static string GetPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            var memberExpression = propertyExpression.Body as MemberExpression;
            if (memberExpression == null)
                return "";

            return memberExpression.Member.Name;
        }

        #endregion



        public static MvcHtmlString Script(this HtmlHelper htmlHelper, string url)
        {
            var scriptTag = "<script src=\"" + UrlHelper.GenerateContentUrl(url + "?v=" + AppInfo.BuildTimestamp, new HttpContextWrapper(HttpContext.Current)) + "\"></script>";
            return new MvcHtmlString(scriptTag);
        }

        public static MvcHtmlString Style(this HtmlHelper htmlHelper, string url)
        {
            var styleTag = "<link href=\"" + UrlHelper.GenerateContentUrl(url + "?v=" + AppInfo.BuildTimestamp, new HttpContextWrapper(HttpContext.Current)) + "\" rel=\"stylesheet\" />";
            return new MvcHtmlString(styleTag);
        }

        public static MvcHtmlString RenderObject(this HtmlHelper htmlHelper, object o, string variableName, bool createVariable = true, bool camelCase = true)
        {
            var json = "null";

            if (o != null)
            {
                if (camelCase)
                    json = JsonConvert.SerializeObject(o, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                else
                    json = JsonConvert.SerializeObject(o, new JsonSerializerSettings());
            }

            if (createVariable)
                return new MvcHtmlString($"<script>var {variableName} = {json};</script>");

            return new MvcHtmlString($"<script>{variableName} = {json};</script>");
        }


        public static string RenderRazorViewToString(this Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

    }

}