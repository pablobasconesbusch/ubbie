﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Helpers
{
    public class AttributeHelpers
    {
        public static List<T> GetAuthenticationContextAttributes<T>(AuthorizationContext context)
        {
            var ret = new List<T>();

            // Find controller attributes
            ret.AddRange((T[])Convert.ChangeType(context.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(T), true), typeof(T[])));

            // Find action attributes
            ret.AddRange((T[])Convert.ChangeType(context.ActionDescriptor.GetCustomAttributes(typeof(T), true), typeof(T[])));

            return ret;
        }
    }
}