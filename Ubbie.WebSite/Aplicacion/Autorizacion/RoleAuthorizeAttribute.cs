﻿using Ubbie.WebSite.Aplicacion.Helpers;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class RolAuthorizeAttribute : AuthorizeAttribute
    {

        public string RolCode { get; set; }

        public RolAuthorizeAttribute(string roleCode)
        {
            RolCode = roleCode;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("No filter context.");

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = UnauthorizedResults.CreateSessionExpiredResult();
                return;
            }

            var attributes = AttributeHelpers.GetAuthenticationContextAttributes<RolAuthorizeAttribute>(filterContext);
            if (!PermissionsHelper.HasRolPermissions(attributes.Select(x => x.RolCode.ToString()).ToArray()))
            {
                filterContext.Result = UnauthorizedResults.CreateUnauthorizedResult();
            }
        }
    }
}