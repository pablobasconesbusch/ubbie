﻿using System;
using System.Web;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class MvcRouteAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("No filter context.");

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = UnauthorizedResults.CreateSessionExpiredResult();
                return;
            }

            if (!PermissionsHelper.HasMvcRoutePermissions(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName))
            {
                filterContext.Result = UnauthorizedResults.CreateUnauthorizedResult();
            }
        }
    }
}