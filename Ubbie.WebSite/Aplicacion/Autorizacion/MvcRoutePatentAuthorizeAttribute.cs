﻿using Ubbie.WebSite.Aplicacion.Helpers;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class MvcRoutePatentAuthorizeAttribute : AuthorizeAttribute
    {
        public string PatentCode { get; set; }

        public MvcRoutePatentAuthorizeAttribute(string patentCode)
        {
            PatentCode = patentCode;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("No filter context.");

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = UnauthorizedResults.CreateSessionExpiredResult();
                return;
            }

            var attributes = AttributeHelpers.GetAuthenticationContextAttributes<MvcRoutePatentAuthorizeAttribute>(filterContext);
            if (!attributes.Any(attr => PermissionsHelper.HasMvcRoutePatentsPermissions(filterContext.ActionDescriptor.ControllerDescriptor.ControllerName, filterContext.ActionDescriptor.ActionName, attr.PatentCode.ToString())))
            {
                filterContext.Result = UnauthorizedResults.CreateUnauthorizedResult();
            }
        }
    }
}