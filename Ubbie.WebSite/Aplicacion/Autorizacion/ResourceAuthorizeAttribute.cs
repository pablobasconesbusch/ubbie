﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.Helpers;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class ResourceAuthorizeAttribute : AuthorizeAttribute
    {
        public string ResourceCode { get; set; }

        public ResourceAuthorizeAttribute(string resourceCode)
        {
            ResourceCode = resourceCode;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = UnauthorizedResults.CreateSessionExpiredResult();
                return;
            }

            var attributes = AttributeHelpers.GetAuthenticationContextAttributes<ResourceAuthorizeAttribute>(filterContext);

            if (!PermissionsHelper.HasResourcePermissions(attributes.Select(x => x.ResourceCode.ToString()).ToArray()))
            {
                filterContext.Result = UnauthorizedResults.CreateUnauthorizedResult();
                var familia = new ServicioRol().GetResource(ResourceCode);

                var usuarioActivo = AppInfo.UsuarioActivo;

                var activeUserService = new UsuarioActivo
                {
                    Id = usuarioActivo.Id,
                    DireccionIP = usuarioActivo.DireccionIP
                };
                ServicioBase.SetActiveUser(activeUserService);

                ServicioBitacora.Log($"El usuario {usuarioActivo.NombreCompleto} ({usuarioActivo.Email}) ha intentado ingresar al módulo {familia.Descripcion}", Constants.BitacoraCriticidad.ALTA);
                ServicioDigitos.RecalcularDigitosVerificadores();
            }
        }


    }
}