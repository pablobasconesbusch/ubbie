﻿using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class UnauthorizedResults
    {
        public static ActionResult CreateUnauthorizedResult()
        {
            var request = HttpContext.Current.Request;
            var urlHelper = new UrlHelper(request.RequestContext);

            if (request.RequestContext.HttpContext.Request.IsAjaxRequest())
            {
                request.RequestContext.HttpContext.Response.StatusCode = (int) HttpStatusCode.Unauthorized;

                var partialView = new PartialViewResult();
                partialView.ViewName = "~/Shared/Views/UnauthorizedResult/_UnauthorizedResult.cshtml";

                return partialView;
            }

            return new RedirectResult(urlHelper.Action("Index", "Error"));
        }

        public static ActionResult CreateSessionExpiredResult()
        {
            var redirectUrl = new UrlHelper(HttpContext.Current.Request.RequestContext).Action("Login", "Account");
            return new RedirectResult(redirectUrl);
        }

    }
}