﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.Helpers;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class ResourcePatentAuthorizeAttribute : AuthorizeAttribute
    {

        public string ResourceCode { get; set; }
        public string PatentCode { get; set; }

        public ResourcePatentAuthorizeAttribute(string resourceCode, string patentCode)
        {
            ResourceCode = resourceCode;
            PatentCode = patentCode;
        }


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException("No filter context.");

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                filterContext.Result = UnauthorizedResults.CreateSessionExpiredResult();
                return;
            }

            var attributes = AttributeHelpers.GetAuthenticationContextAttributes<ResourcePatentAuthorizeAttribute>(filterContext);

            if (!attributes.Any(x => PermissionsHelper.HasResourcePatentsPermissions(x.ResourceCode, x.PatentCode)))
            {
                filterContext.Result = UnauthorizedResults.CreateUnauthorizedResult();
                var resource = new ServicioRol().GetResource(ResourceCode);

                ServicioBitacora.Log($"El usuario {AppInfo.UsuarioActivo.NombreCompleto} ({AppInfo.UsuarioActivo.Email}) ha intentado ingresar al módulo {resource.Descripcion}", Constants.BitacoraCriticidad.ALTA);
                ServicioDigitos.RecalcularDigitosVerificadores();
            }
        }

    }
}