﻿using System.Linq;
using Ubbie.WebSite.Aplicacion.Code;

namespace Ubbie.WebSite.Aplicacion.Autorizacion
{
    public class PermissionsHelper
    {

        public static bool HasRolPermissions(params string[] roles)
        {
            return roles.Any(x => x == AppInfo.UsuarioActivo.CodigoRol);
        }


        public static bool HasResourcePermissions(params string[] resources)
        {
            return AppInfo.UsuarioActivo.RolFamilias.Any(x => resources.Contains(x.CodigoFamilia));
        }
        public static bool HasResourcePatentsPermissions(string resource, params string[] patents)
        {
            return AppInfo.UsuarioActivo.RolFamilias.Any(x => x.CodigoFamilia == resource && x.RolFamiliaPatentes.Any(y => patents.Contains(y.CodigoPatente)));
        }


        public static bool HasMvcRoutePermissions(string controller, string action)
        {
            return AppInfo.UsuarioActivo.RolFamilias.Any(x => x.Familia.Controller.Contains(controller) && x.Familia.Action.Contains(action));
        }
        public static bool HasMvcRoutePatentsPermissions(string controller, string action, params string[] patentsCode)
        {
            return AppInfo.UsuarioActivo.RolFamilias.Any(x => x.Familia.Controller.Contains(controller) && x.Familia.Action.Contains(action) && x.RolFamiliaPatentes.Any(y => patentsCode.Any(p => p.Contains(y.CodigoPatente))));
        }
    }
}