﻿using System;
using System.Web;
using Ubbie.Entidades.Soporte;
using Ubbie.Framework.Extensiones;
using Ubbie.Framework.Mapping;
using Ubbie.Servicios.Soporte;

namespace Ubbie.WebSite.Aplicacion.Code
{
    public class EventLogModel : MapperModel<EventLogModel, EventLog, int>
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }
        public int? UsuarioId { get; set; }
        public string EventCode { get; set; }
        public string LogLevel { get; set; }
        public string ServerException { get; set; }
        public string RequestType { get; set; }
        public string UserAgent { get; set; }
        public string Path { get; set; }
        public string QueryString { get; set; }
        public string Data { get; set; }


        public string UsuarioInfo { get; set; }

        public EventLogModel()
        {

        }

        public EventLogModel(string eventCode, string serverException, HttpContext httpContext)
        {
            EventCode = eventCode;
            ServerException = serverException;
            RequestType = httpContext.Request.RequestType;
            UserAgent = httpContext.Request.UserAgent;
            Path = httpContext.Request.Path;

            foreach (string param in httpContext.Request.QueryString)
            {
                QueryString += $"{param}: {httpContext.Request.Params[param]}; ";
            }


            foreach (string data in httpContext.Request.Form)
            {
                Data += $"{data}: {httpContext.Request.Params[data]}; ";
            }
        }

        public EventLogModel(int entityId) : base(entityId, x => new EventLogService().Get(entityId))
        {

        }

        public override void InitializateData()
        {
        }

        protected override void ExtraMapFromEntity(EventLog entity)
        {
            if (entity.User != null)
                UsuarioInfo = $"{entity.UserId.Value} - {entity.User.NombreCompleto} ({entity.User.Email})";
            else
                UsuarioInfo = "-";


            if (string.IsNullOrEmpty(entity.QueryString))
                QueryString = "-";

            if (string.IsNullOrEmpty(entity.Data))
                Data = "-";

            Date = Date.ToUserDateTime();
        }

        protected override void ExtraMapToEntity(EventLog entity)
        {
        }
    }
}
