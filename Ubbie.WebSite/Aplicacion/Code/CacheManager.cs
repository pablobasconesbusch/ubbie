﻿using System;
using System.Runtime.Caching;
using System.Web;
using Ubbie.Framework.Extensiones;
using Ubbie.WebSite.Aplicacion.Autenticacion;

namespace Ubbie.WebSite.Aplicacion.Code
{
    public class CacheManager
    {
        private static readonly MemoryCache Cache = MemoryCache.Default;


        private static object ObtenerItemCacheado(string key, Func<object> accion, bool refrescar)
        {
            if (Cache.Contains(key) && !refrescar)
                return Cache.Get(key);

            var resultado = accion();
            if (resultado.IsNotNull())
            {
                var cacheItemPolicy = new CacheItemPolicy { AbsoluteExpiration = DateTime.Now.AddDays(1) };
                Cache.Set(key, resultado, cacheItemPolicy);
            }

            return Cache.Get(key);
        }

        public static UsuarioActivo ObtenerUsuarioActivo(bool refrescar = false)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return null;

            var usuarioAutenticado = (UsuarioAutenticado)HttpContext.Current.User;
            var usuarioId = usuarioAutenticado.UsuarioId;

            return (UsuarioActivo)ObtenerItemCacheado(string.Concat(usuarioId, "-", "UsuarioActivo"), () => AppService.GenerarUsuarioActivo(usuarioId), refrescar);
        }

    }
}