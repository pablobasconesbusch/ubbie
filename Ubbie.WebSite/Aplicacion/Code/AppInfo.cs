﻿using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using Ubbie.Entidades.Seguridad;
using Ubbie.Servicios.Negocio;
using Ubbie.WebSite.Aplicacion.Autenticacion;
using Configuration = Ubbie.Servicios.Models.Configuration;

namespace Ubbie.WebSite.Aplicacion.Code
{
    public static class AppInfo
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["ubbie"].ConnectionString;

        public static string EnvironmentCode => Configuration.Application.EnvironmentCode;
        public static UsuarioActivo UsuarioActivo => CacheManager.ObtenerUsuarioActivo();

        public static string BuildTimestamp { get; set; }
        public static string Version => Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public static List<UsuarioNotificacion> NotificationList => new ServicioNotificacion().GetListByUser(false);
        public static List<UsuarioNotificacion> ReadNotificationList => new ServicioNotificacion().GetListByUser(true);
    }
}