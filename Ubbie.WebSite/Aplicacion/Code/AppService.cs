﻿using Microsoft.WindowsAzure.Storage;
using System;
using System.Web;
using Ubbie.Framework.Seguridad;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Seguridad;

namespace Ubbie.WebSite.Aplicacion.Code
{
    public class AppService
    {
        public static void Initialize()
        {
            Configuration.Database.ConnectionString = AppInfo.ConnectionString;
            new ConfigurationService().LoadConfig();

            AppInfo.BuildTimestamp = DateTime.UtcNow.ToString("yyyyMMddHHmmssfff");
            EmailingService.FillEmailTemplates();

            Configuration.Application.AzureStorageAccount = CloudStorageAccount.Parse(Configuration.Application.AzureStorageConnectionString);
            Configuration.Application.AzureCloudBlobClient = Configuration.Application.AzureStorageAccount.CreateCloudBlobClient();
            Configuration.Application.AzureUploadsContainer = Configuration.Application.AzureCloudBlobClient.GetContainerReference(Configuration.Application.AzureUploadsContainerDirectory);
            Configuration.Application.AzureUploadsContainerUri = Configuration.Application.AzureUploadsContainer.Uri.AbsoluteUri;

        }

        public static void DisposeContext()
        {
            ServicioBase.DisposeContext();
        }

        public static Autenticacion.UsuarioActivo GenerarUsuarioActivo(int usuarioId)
        {
            var usuario = new ServicioUsuario().Get(usuarioId);

            var usuarioActivo = new Autenticacion.UsuarioActivo();
            usuarioActivo.Id = usuario.Id;
            usuarioActivo.Email = usuario.Email;
            usuarioActivo.Nombre = CustomEncrypt.Decrypt(usuario.Nombre);
            usuarioActivo.Apellido = CustomEncrypt.Decrypt(usuario.Apellido);
            usuarioActivo.TimeZoneCodigo = usuario.TimeZoneCode;
            usuarioActivo.TimeZoneNombre = usuario.TimeZone.Description;
            usuarioActivo.CodigoRol = usuario.Rol.Codigo;
            usuarioActivo.RolFamilias = usuario.Rol.RolFamilias;
            usuarioActivo.ImagenPerfilId = usuario.ImagenPerfilId;
            usuarioActivo.DireccionIP = HttpContext.Current.Request.UserHostAddress;
            usuarioActivo.EsSuperUsuario = usuario.EsSuperUsuario;

            return usuarioActivo;
        }

    }
}