﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Ubbie.WebSite.Aplicacion.Code
{
    public class UrlGenerator
    {
        public Assembly ControllerAssembly { get; set; }
        public string BaseUrl { get; set; }
        private TreeNode _result = new TreeNode();

        public static string GenerateUrls(Assembly controllerAssembly)
        {
            var generator = new UrlGenerator();
            generator.ControllerAssembly = controllerAssembly;
            generator.BaseUrl = VirtualPathUtility.ToAbsolute("~");

            return generator.Generate();
        }

        public string Generate()
        {
            var controllers = ControllerAssembly.GetTypes()
                    .Where(type => typeof(Controller).IsAssignableFrom(type) && !type.IsAbstract)
                    .Select(x => new ControllerInfo()
                    {
                        Controller = x,
                        RouteAttribute = x.GetCustomAttribute(typeof(RouteAttribute)) as RouteAttribute
                    })
                    .OrderByDescending(x => x.Areas.Length).ThenBy(x => x.Route)
                    .ToList();

            _result = GetNode(0, controllers);

            return "var URLs = " + _result.GetUrls(BaseUrl) + ";";
        }

        private TreeNode GetNode(int level, List<ControllerInfo> controllers)
        {
            var result = new TreeNode();

            var group = controllers.GroupBy(x => x.GetArea(level)).ToList();

            if (group.Count > 0)
            {
                foreach (var item in group)
                {
                    if (!string.IsNullOrEmpty(item.Key))
                    {
                        var node = GetNode(level + 1, item.ToList());

                        node.Key = item.Key;

                        if (node.Controllers.Count > 0)
                        {
                            result.Items.Add(node);
                        }
                        else
                        {
                            result.Controllers.AddRange(item.ToList());
                        }
                    }
                }
            }

            return result;

        }

        private class TreeNode
        {
            public string Key { private get; set; }
            public List<ControllerInfo> Controllers { get; }
            public List<TreeNode> Items { get; }

            public TreeNode()
            {
                Controllers = new List<ControllerInfo>();
                Items = new List<TreeNode>();
            }

            public string GetUrls(string baseUrl)
            {
                var sb = new StringBuilder();

                foreach (var node in Items)
                {
                    var urls = node.GetUrls(baseUrl);

                    if (urls.Length > 0)
                    {
                        if (sb.Length > 0)
                            sb.AppendLine(",");

                        sb.Append(urls);
                    }
                }

                foreach (var item in Controllers)
                {
                    // Home is special...
                    if (item.ControllerName == "HomeController")
                    {
                        if (sb.Length > 0)
                            sb.AppendLine(",");

                        sb.Append("Home: '/'");
                        continue;
                    }

                    // Any other controller
                    var urls = item.GetUrls(baseUrl);

                    if (urls.Length > 0)
                    {
                        if (sb.Length > 0)
                            sb.AppendLine(",");

                        sb.Append(urls);
                    }
                }

                var template = "{{ \n {0}\n}}";

                if (!string.IsNullOrEmpty(Key))
                    template = "{1}: {{ \n {0}\n}}";

                return string.Format(template, sb, Key);
            }
        }

        private class ControllerInfo
        {
            public string ControllerName => Controller.Name;
            public Type Controller { private get; set; }
            public RouteAttribute RouteAttribute { private get; set; }

            private string[] _area;

            public string Route
            {
                get
                {
                    if (RouteAttribute != null)
                    {
                        var route = RouteAttribute.Template;

                        if (route.Contains("/{"))
                        {
                            var start = route.StartsWith("/") ? 1 : 0;

                            route = route.Substring(start, route.LastIndexOf("/{", StringComparison.Ordinal) + 1 - start);
                        }
                        if (route.StartsWith("{"))
                            route = "Home/";

                        return route;
                    }
                    else
                        return $"{ControllerName.Substring(0, ControllerName.LastIndexOf("Controller", StringComparison.Ordinal))}/";
                }
            }

            public string[] Areas
            {
                get
                {
                    if (_area != null) return _area;

                    if (Route.EndsWith("/"))
                        _area = Route.Substring(0, Route.Length - 1).Split('/');
                    else
                        _area = Route.Split('/');

                    return _area;
                }

            }

            public string GetArea(int idx)
            {
                if (idx >= Areas.Length)
                    return "";

                return Areas[idx];
            }

            public string GetUrls(string baseUrl)
            {
                var tabs = "\n";

                var countTab = Route.Split('/').Length;

                for (var i = 0; i < countTab; i++)
                    tabs += "\t";


                var actions = GetActions();
                var urls = new StringBuilder();

                foreach (var action in actions)
                {
                    urls.AppendFormat("{3}\t{0}: '{1}{2}{0}',", action, baseUrl, Route, tabs);
                }

                if (urls.Length > 0)
                    return string.Format("{2}{0}:{{{2}{1}{2}}}", Areas.Last(), urls.Remove(urls.Length - 1, 1), tabs);


                return "";
            }

            private List<string> GetActions()
            {

                var list = Controller
                    .GetMethods(BindingFlags.Public | BindingFlags.DeclaredOnly | BindingFlags.Instance)
                    .Where(m => !m.GetCustomAttributes(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), true).Any()
                                && (m.ReturnType.IsSubclassOf(typeof(ActionResult)) || m.ReturnType.IsAssignableFrom(typeof(ActionResult))))
                    .Select(x => x.Name)
                    .ToList<string>();

                return list.Distinct().ToList();

            }
        }
    }
}