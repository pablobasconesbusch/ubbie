﻿using System.Web.Services;
using Ubbie.Entidades.Negocio;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;

namespace Ubbie.WebSite
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LiquidarExpensas : System.Web.Services.WebService
    {

        [WebMethod]
        public ServiceResponse LiquidarExpensaDelMes()
        {
            var today = Configuration.CurrentLocalTime.Date;
            var service = new ServicioExpensas();

            var expensa = new Expensa
            {
                Mes = today.Month,
                Año = today.Year,
                ConsorcioId = Constants.ConsorcioVariables.ActualConsorcioId
            };

            var sr = service.GuardarExpensa(expensa);

            if (!sr.Status)
                return sr;

            var expensaId = sr.ReturnValue;
            sr = new ServicioExpensas().LiquidarExpensas(today, expensaId);

            return sr;
        }
    }
}
