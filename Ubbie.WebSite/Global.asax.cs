﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ubbie.Servicios.Aplicacion;
using Ubbie.Servicios.Models;
using Ubbie.WebSite.Aplicacion.Autenticacion;
using Ubbie.WebSite.Aplicacion.Code;
using Ubbie.WebSite.Aplicacion.MVC;
using Ubbie.WebSite.Aplicacion.MVC.Filtros;

namespace Ubbie.WebSite
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AppService.Initialize();

            AreaRegistration.RegisterAllAreas();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BinderConfig.RegisterBinders(ModelBinders.Binders);

            SetGlobalDefaults();

            EmailingService.StartQueueManager();
        }


        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            var culture = HttpContext.Current.Request.Cookies["lang"]?.Value;
            CultureInfo lang;

            if (culture == null)
            {
                lang = new CultureInfo("es-AR");
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("lang", "es-AR"));
            }
            else
            {
                lang = new CultureInfo(culture);
            }

            Thread.CurrentThread.CurrentUICulture = lang;
            Thread.CurrentThread.CurrentCulture = lang;
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            AuthService.DeserializeActiveUser();
        }

        protected void Application_EndRequest()
        {
            AppService.DisposeContext();

            var context = new HttpContextWrapper(Context);
            if (Context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            {
                Context.Response.Clear();
                Context.Response.StatusCode = 401;
            }

        }



        private void SetGlobalDefaults()
        {
            EmailingService.EmailTemplatesPath = Server.MapPath("~/Content/emails");
            Configuration.Application.UploadsFolder = Server.MapPath("~/Content/uploads");
            Configuration.Application.MissingImagePath = Server.MapPath("~/Content/images/missing-image.png");

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }

    }
}
