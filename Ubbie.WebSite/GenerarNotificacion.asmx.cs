﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using Ubbie.Entidades.Seguridad;
using Ubbie.Servicios.Base;
using Ubbie.Servicios.Models;
using Ubbie.Servicios.Negocio;

namespace Ubbie.WebSite
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GenerarNotificacion : System.Web.Services.WebService
    {

        [WebMethod]
        public ServiceResponse NuevoReclamo(List<Usuario> usuarios, string mensaje)
        {
            var sr = new ServiceResponse();
            var service = new ServicioNotificacion();

            var usuarioNotificacion = new UsuarioNotificacion
            {
                Titulo = "Nuevo reclamo",
                Mensaje = mensaje,
                FechaEnvio = Configuration.CurrentLocalTime,
            };

            try
            {
                usuarios.ForEach(x =>
                {
                    usuarioNotificacion.UsuarioId = x.Id;
                    service.SaveEntity(usuarioNotificacion);
                });
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
            }

            return sr;
        }


        [WebMethod]
        public ServiceResponse ReclamoFinalizado(Usuario usuario, string mensaje)
        {
            var sr = new ServiceResponse();
            var service = new ServicioNotificacion();

            var usuarioNotificacion = new UsuarioNotificacion
            {
                Titulo = "Reclamo Resuelto",
                Mensaje = mensaje,
                FechaEnvio = Configuration.CurrentLocalTime,
                UsuarioId = usuario.Id
            };

            try
            {
                service.SaveEntity(usuarioNotificacion);
            }
            catch (Exception ex)
            {
                sr.AddError(ex);
            }

            return sr;
        }
    }
}
