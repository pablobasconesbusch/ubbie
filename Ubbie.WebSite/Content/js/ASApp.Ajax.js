﻿
ASApp.Ajax = {


    settings: {
        url: null,
        data: {},
        type: 'POST',

        initGlobalPlugins: true,

        onSuccess: $.noop,
        onError: $.noop,

        onSuccessOptions: {},
        onErrorOptions: {},

        append: false,

        blockUI: true,

        partialWrapper: '[data-partial]',

        showSuccessMessage: true,

        scrollToTop: false,
        scrollToPartial: false,

        renderPartialViews: null
    },


    post: function (options) {
        var s = $.extend({}, this.settings);

        $.extend(s, options);

        if (s.blockUI)
            ASApp.Plugins.BlockUI.block();

        $.ajax({
            type: s.type,
            url: s.url,
            data: $.param(s.data),
            traditional: true,
            success: function (data) {
                if (!data) {
                    ASApp.Plugins.BlockUI.unblock();
                    ASApp.Notify.error({
                        message: 'La respuesta del servidor es vacío'
                    });

                }

                if (data.Errors.length) {
                    var errors = '';
                    data.Errors.forEach(function (e) {
                        errors += e.ErrorMessage + '\n\r';
                    });

                    if (data.ReturnValue == -2 || data.ReturnValue == -3) {
                        $('#modalErrorIntegridad').modal({
                            backdrop: 'static',
                            keyboard: false
                        }).modal('show');

                        $('[data-errors]').html(errors);

                        if (data.ReturnValue == -2) {
                            $('#calculateDigits').click(function () {
                                $('#RecalcularDigitos').val(true);
                                $('#frmLogin').submit();
                                $('#modalErrorIntegridad').modal('hide');
                            });
                        }
                        else if (data.ReturnValue == -3) {
                            $('#calculateDigits').hide();
                        }

                        ASApp.Utils.ajaxForm('#frmRestore', {
                            onSuccess: function () {
                                $('#modalErrorIntegridad').modal('hide');
                            }
                        });

                    } else {
                        s.onErrorOptions.message = errors;
                        s.onErrorOptions.title = data.MessageTitle;

                        ASApp.Notify.error(s.onErrorOptions);
                        s.onError();
                    }

                    if (data.Redirect)
                        ASApp.Utils.redirect(data.RedirectUrl, data.RedirectTime);
                    else
                        ASApp.Plugins.BlockUI.unblock();
                    return;
                }

                if (data.ReturnValue === -1) {
                    if (!s.form)
                        s.form = $('form:first');

                    ASApp.Plugins.JqueryValidation.serverValidationToForm(s.form, data.Data);
                    ASApp.Plugins.BlockUI.unblock();
                    return;
                }

                s.onSuccessOptions.title = data.MessageTitle;
                s.onSuccessOptions.message = data.MessageText;

                if (s.scrollToTop)
                    ASApp.Utils.scrollToTop();

                if (s.showSuccessMessage)
                    ASApp.Notify.success(s.onSuccessOptions);

                s.onSuccess(data);

                if (data.Redirect || data.Reload) {
                    ASApp.Utils.redirect(data.RedirectUrl, data.RedirectTime);
                    return;
                }

                ASApp.Plugins.BlockUI.unblock();
            }
        });

    },

    postForm: function (form, options) {
        form = ASApp.Utils.getJqueryInstance(form);
        options.url = form.attr('action');
        ASApp.Ajax.post(options);
    },

    partial: function (options) {
        var s = $.extend({}, this.settings);
        s.scrollToPartial = true;

        $.extend(s, options);

        s.partialWrapper = ASApp.Utils.getJqueryInstance(s.partialWrapper);

        if (s.blockUI)
            ASApp.Plugins.BlockUI.block();

        $.ajax({
            type: s.type,
            url: s.url,
            data: JSON.stringify(s.data),
            contentType: 'application/json; charset=utf-8',
            success: function (sr) {
                if (s.blockUI)
                    ASApp.Plugins.BlockUI.unblock();

                if (s.scrollToPartial)
                    ASApp.Utils.scrollToPartial(s.partialWrapper);

                if (s.renderPartialViews) {
                    s.renderPartialViews(sr);
                }
                else {
                    if (s.append)
                        s.partialWrapper.append(sr.HtmlViews[0]);
                    else
                        s.partialWrapper.html(sr.HtmlViews[0]);
                }


                if (s.initGlobalPlugins)
                    ASApp.Plugins.initGlobalPlugins(s.partialWrapper);

                if (s.blockUI)
                    ASApp.Plugins.BlockUI.unblock();

                s.onSuccess(sr);
            }
        });
    },

    apiCall: function (options) {
        var s = $.extend({}, ASApp.Ajax.settings);
        s.blockUI = false;

        $.extend(s, options);

        if (s.blockUI)
            ASApp.Plugins.BlockUI.block();

        $.ajax({
            type: s.type,
            url: s.url,
            data: $.param(s.data),
            traditional: true,
            success: function (data) {
                if (s.blockUI)
                    ASApp.Plugins.BlockUI.unblock();

                s.onSuccess(data);
            }
        });
    },

    popUp: function (options) {
        var s = $.extend({}, ASApp.Ajax.settings);

        var bootboxOptions = {
            size: '',
            backdrop: true,
            callback: $.noop,
            title: '',
        }

        s.onInit = $.noop;
        s.popUpType = 'confirm';

        $.extend(s, options);
        $.extend(bootboxOptions, options);

        if (s.blockUI)
            ASApp.Plugins.BlockUI.block();

        // The callback is invoked by the plugin: callback(response)
        options.callback = options.callback || $.noop;

        ASApp.Ajax.post({
            url: s.url,
            data: s.data,
            showSuccessMessage: false,
            onSuccess: function (sr) {
                if (s.blockUI)
                    ASApp.Plugins.BlockUI.unblock();

                if (sr.ReturnName)
                    bootboxOptions.title = sr.ReturnName;
                bootboxOptions.message = sr.HtmlViews[0];

                var dialog;
                if (s.popUpType == 'confirm') {
                    dialog = bootbox.confirm(bootboxOptions);
                } else if (s.popUpType == 'dialog') {
                    dialog = bootbox.dialog(bootboxOptions);
                } else if (s.popUpType == 'alert') {
                    dialog = bootbox.alert(bootboxOptions);
                }

                if (dialog)
                    dialog.init(s.onInit());
            }
        });
    },

    attachServerErrorHandler: function () {
        $(document).ajaxError(function (event, jqxhr, ajaxSettings, thrownError) {

            ASApp.Plugins.BlockUI.unblock();

            if (jqxhr.status == 401) {
                bootbox.alert({
                    message: 'Usted no está autorizado a realizar la operación'
                });

            } else {
                if (jqxhr.responseText) {
                    bootbox.alert({
                        message: jqxhr.responseText,
                        size: 'large',
                        backdrop: true
                    });
                }
            }


        });
    }

}