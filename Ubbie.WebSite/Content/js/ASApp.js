﻿
var ASApp = {
    settings: {
        languageTwoLetter: 'es',
        dateFormat: 'dd/mm/yyyy'
    },

    init: function () {
        ASApp.Plugins.init();
        ASApp.Global.init();
    }
}


$(function () {
    ASApp.init();
});

