﻿

ASApp.Global = {

    controls: {
        notificationTimer: null,
        notificationRefresh: 1000 * 60 * 60
    },

    init: function () {
        this.initGlobalHandlers();
        this.selectSideBarMenu();

        ASApp.Global.startNotificationService();
        ASApp.Ajax.attachServerErrorHandler();

    },


    initGlobalHandlers: function () {
        $('body').on('click', 'a[href="#"]', function (e) {
            e.preventDefault();
        });

        $('body').on('click', '[data-calculate-dvv]', function (e) {
            ASApp.Ajax.post({
                url: URLs.Cuenta.DigitosVerificadores,
                onSuccess: function () {

                }
            });
        });

        ASApp.Global.attachNotificationHandlers();
    },

    selectSideBarMenu: function () {
        var currentUrl = window.location.pathname.split('?')[0];
        var sidebarMenu = $('.page-sidebar-menu');
        var selectedItem = sidebarMenu.find('a[href="' + currentUrl + '"]');

        selectedItem.parent().addClass('active');
        selectedItem.closest('.sub-menu').parent().addClass('active');
        selectedItem.closest('.sub-menu').parent().find('.arrow').addClass('open');
    },


    refreshNotifications: function (id) {
        ASApp.Ajax.partial({
            url: URLs.Notificacion.GetNotification,
            data: { entityId: id },
            partialWrapper: '#notificationDropDown',
            blockUI: false,
            onSuccess: function () {
                App.initSlimScroll('.scroller');
            }
        });
    },

    attachNotificationHandlers: function () {
        $('body').on('click', '[data-notification]', function () {
            var $this = $(this);
            var notificationId = ($this.data('notification'));
            ASApp.Ajax.partial({
                url: URLs.Notificacion.NewNotification,
                data: { entityId: notificationId },
                partialWrapper: '#notificationModalWrapper',
                blockUI: false,
                onSuccess: function () {
                    $('#notificationModal').modal('show');
                    ASApp.Global.refreshNotifications(notificationId);
                }
            });
        });

        $('body').on('click', '[data-view-all]', function () {
            ASApp.Ajax.post({
                url: URLs.Notificacion.ViewAll,
                blockUI: false,
                showSuccessMessage: false,
                onSuccess: function () {
                    ASApp.Global.refreshNotifications();
                }
            });
        });
    },

    startNotificationService: function () {
        ASApp.Global.controls.timer = setInterval(ASApp.Global.refreshNotifications, ASApp.Global.controls.notificationRefresh);

    }



}