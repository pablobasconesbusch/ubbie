﻿

ASApp.Notify = {

    config: {

    },

    success: function (options) {
        options.message = options.message || ASApp.settings.NotifySuccessMessage;

        ASApp.Notify.Toastr.success(options);
    },

    error: function (options) {
        options.message = options.message || ASApp.settings.NotifyErrorMessage;

        ASApp.Notify.Toastr.error(options);
    },

    alert: function (options, onAccept, onCancel) {
        ASApp.Notify.SweetAlert2.alert(options, onAccept, onCancel);
    },

    confirmDelete: function (options, callback) {
        ASApp.Notify.SweetAlert2.confirmDelete(options, callback);
    }

}


// Bootstrap Notify http://bootstrap-notify.remabledesigns.com/
ASApp.Notify.BootstrapNotify = {
    // http://bootstrap-notify.remabledesigns.com/

    pluginOptions: {
        title: '',
        message: ''
    },

    pluginSettings: {
        type: ''
    },

    success: function (options, settings) {
        var finalOptions = $.extend({}, this.pluginOptions);
        var finalSettings = $.extend({}, this.pluginSettings);

        finalOptions.icon = 'fa fa-smile-o';
        finalOptions.message = '';
        finalSettings.type = 'success';

        if (options)
            $.extend(finalOptions, options);

        if (settings)
            $.extend(finalSettings, settings);

        $.notify(finalOptions, finalSettings);

    },

    error: function (options, settings) {
        var finalOptions = $.extend({}, this.pluginOptions);
        var finalSettings = $.extend({}, this.pluginSettings);

        finalOptions.icon = 'fa fa-exclamation';
        finalSettings.type = 'danger';

        if (options)
            $.extend(finalOptions, options);

        if (settings)
            $.extend(finalSettings, settings);

        $.notify(finalOptions, finalSettings);
    }
}

// SweetAlert2 https://limonte.github.io/sweetalert2/
ASApp.Notify.SweetAlert2 = {

    pluginOptions: {
        title: '',
        text: '',
        html: '',
        type: '',

        timer: '',

        showConfirmButton: null,
        confirmButtonColor: null,
        confirmButtonText: null,

        showCancelButton: null,
        cancelButtonColor: null,
        cancelButtonText: null
    },


    success: function (options) {
        var finalOptions = $.extend({}, this.pluginOptions);

        finalOptions.title = '';
        finalOptions.type = 'success';
        finalOptions.timer = 1500;
        finalOptions.showConfirmButton = false;

        if (options)
            $.extend(finalOptions, options);

        swal(finalOptions);

    },

    alert: function (options, onAccept, onCancel) {
        var finalOptions = $.extend({}, this.pluginOptions);

        onAccept = onAccept || $.noop;
        onCancel = onCancel || $.noop;

        finalOptions.title = 'Give me an "title" option';
        finalOptions.type = 'warning';

        finalOptions.showConfirmButton = true;
        finalOptions.showCancelButton = true;
        finalOptions.confirmButtonColor = '#3085d6';
        finalOptions.confirmButtonText = 'Sí';
        finalOptions.cancelButtonColor = '#dd4b39';
        finalOptions.cancelButtonText = 'No';

        $.extend(finalOptions, options);

        swal(finalOptions).then(function () {
            onAccept();
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                onCancel();
            }
        });

    },
    error: function (options) {
        var finalOptions = $.extend({}, this.pluginOptions);

        finalOptions.title = ASApp.settings.NotifyErrorMessage;
        finalOptions.type = 'error';

        finalOptions.showConfirmButton = true;
        finalOptions.confirmButtonColor = '#dd4b39';
        finalOptions.confirmButtonText = ASApp.settings.NotifyConfirmText;

        if (options)
            $.extend(finalOptions, options);

        swal(finalOptions);
    },

    confirmDelete: function (options, onAccept, onCancel) {
        var finalOptions = $.extend({}, this.pluginOptions);

        onAccept = onAccept || $.noop;
        onCancel = onCancel || $.noop;

        finalOptions.title = ASApp.settings.NotifyConfirmDeleteMessage;
        finalOptions.type = 'warning';

        finalOptions.showConfirmButton = true;
        finalOptions.showCancelButton = true;
        finalOptions.confirmButtonColor = '#3085d6';
        finalOptions.confirmButtonText = ASApp.settings.NotifyYesText;
        finalOptions.cancelButtonColor = '#dd4b39';
        finalOptions.cancelButtonText = ASApp.settings.NotifyNoText;

        $.extend(finalOptions, options);

        swal(finalOptions).then(function () {
            onAccept();
        }, function (dismiss) {
            if (dismiss === 'cancel') {
                onCancel();
            }
        });

    }
}
// Toastr http://codeseven.github.io/toastr/demo.html
ASApp.Notify.Toastr = {

    pluginOptions: {
        progressBar: true,
        timeOut: 2000,
        positionClass: 'toast-top-right'
    },


    success: function (options) {
        var finalOptions = $.extend({}, this.pluginOptions);

        if (options)
            $.extend(finalOptions, options);

        toastr.success(finalOptions.message, finalOptions.title, finalOptions);
    },

    error: function (options) {
        var finalOptions = $.extend({}, this.pluginOptions);

        if (options)
            $.extend(finalOptions, options);

        toastr.error(finalOptions.message, finalOptions.title, finalOptions);
    }
}

