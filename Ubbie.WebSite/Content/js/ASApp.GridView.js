﻿ASApp.GridView = {

    settings: {
        gridId: '#setupGrid',
        grid: null,

        gridBoxId: '[data-grid-box]',
        gridBox: null,

        btnNewId: '#btnNew',
        btnNew: null,

        btnReloadGridId: '#btnReloadGrid',
        btnReloadGrid: null,

        btnCollapseId: '#btnCollapseGrid',
        btnCollapse: null,

        gridQueryId: '#GridQuery',
        gridQuery: null,

        initGlobalPlugins: true,
        initHandlers: true
    },

    initGrid: $.noop,

    onInit: $.noop,

    init: function (options) {
        var base = this;
        var s = base.settings;

        $.extend(base, options);

        s.grid = ASApp.Utils.getJqueryInstance(s.gridId);
        s.gridBox = ASApp.Utils.getJqueryInstance(s.gridBoxId);

        base.initGrid(s.grid);
        base.initPlugins();
        base.initHandlers();
    },

    initPlugins: function () {
        var base = this;
        var s = base.settings;

        if (s.initGlobalPlugins)
            ASApp.Plugins.initGlobalPlugins(s.gridBox);
    },

    initHandlers: function () {
        var base = this;
        var s = base.settings;

        s.btnNew = ASApp.Utils.getJqueryInstance(s.btnNewId);
        s.btnNew.on('click', function () {

            if (typeof (ASApp.InfoView.entity.entityId) === 'string')
                ASApp.InfoView.entity.entityId = '';
            else
                ASApp.InfoView.entity.entityId = 0;

            ASApp.InfoView.getView();
            base.collapseGrid();
        });

        s.btnCollapse = ASApp.Utils.getJqueryInstance(s.btnCollapseId);

        s.btnReloadGrid = ASApp.Utils.getJqueryInstance(s.btnReloadGridId);
        s.btnReloadGrid.click(base.reloadGrid);

        s.gridQuery = ASApp.Utils.getJqueryInstance(s.gridQueryId);

        s.gridQuery.focus();
        base.onInit(s.grid);
    },


    expandGrid: function () {
        var base = this;
        var s = base.settings;

        if (base.isGridCollapsed())
            s.btnCollapse.click();
    },

    collapseGrid: function () {
        var base = this;
        var s = base.settings;

        if (!base.isGridCollapsed())
            s.btnCollapse.click();
    },

    toggleGrid: function () {
        var base = this;
        var s = base.settings;
        if (base.isGridCollapsed())
            base.expandGrid();
        else
            base.collapseGrid();
    },

    isGridCollapsed: function () {
        var base = this;
        var s = base.settings;

        return s.btnCollapse.hasClass('expand');
    },

    reloadGrid: function () {
        ASApp.GridView.settings.grid.DataTable().ajax.reload();
    }

}