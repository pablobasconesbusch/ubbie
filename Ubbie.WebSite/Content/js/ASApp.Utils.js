﻿ASApp.Utils = {

    formToObject: function (form) {
        form = ASApp.Utils.getJqueryInstance(form);
        var arr = form.serializeArray();
        var data = {};

        $(arr).each(function (i, e) {
            if (data[e.name] && data[e.name] === 'true') {
                return;
            }
            data[e.name] = e.value;
        });

        return data;
    },

    getJqueryInstance: function (element) {
        if (element instanceof jQuery === false)
            element = $(element);

        return element;
    },

    redirect: function (url, timeOut) {
        timeOut = timeOut || 500;

        setTimeout(function () {
            if (url)
                window.location = url;
            else
                window.location.reload();
        }, timeOut);

    },

    scrollToTop: function () {
        $('html, body').animate({ scrollTop: 0 });
    },

    scrollToPartial: function (element, offset) {
        element = ASApp.Utils.getJqueryInstance(element);

        if (element.length) {
            offset = offset || 100;
            $('html, body').animate({ scrollTop: element.offset().top - offset });
        }
    },

    renderStateCheck: function (condition) {
        if (condition)
            return '<i class="fa fa-check text-success"></i>';

        return '<i class="fa fa-times text-danger"></i>';
    },

    renderColorfulLabel: function (color, text) {
        return '<span class="label" style="background-color: ' + color + '">' + text + '</span>';
    },

    createGuid: function() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },

    ajaxForm: function (form, config) {

        form = ASApp.Utils.getJqueryInstance(form);
        form.on('submit', function (e) {
            e.preventDefault();

            if (form.valid()) {
                var data = ASApp.Utils.formToObject(form);

                var options = { data: data }

                $.extend(options, config);

                ASApp.Ajax.postForm(form, options);
            }
        });

    },

    isMobile: function () {
        return /Mobi/.test(navigator.userAgent);
    },

    renderImageColumn: function(key, proportion) {
        // Key equals 0
        if (decodeURIComponent(key) === 'eA5EWJCbOSHzz5xid5M7pA==')
            return '';

        var img = document.createElement('img');
        img.setAttribute('class', 'grid-image');
        img.setAttribute('src', URLs.File.View + '?key=' + decodeURIComponent(key) + (proportion ? '&proportion=' + proportion : ''));
        img.setAttribute('style', 'max-height: 90px; max-width: 50%; width: auto;');
        img.setAttribute('data-action', 'zoom');

        return img.outerHTML;
    },

    resizeTextAreas: function () {
        $('textarea').each(function () {
            this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
        }).on('input', function () {
            this.style.height = 'auto';
            this.style.height = (this.scrollHeight) + 'px';
        });
    }

}
