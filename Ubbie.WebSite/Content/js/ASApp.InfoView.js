﻿

ASApp.InfoView = {

    settings: {
        // General Config
        infoBoxId: '[data-info-box]',
        infoBox: null,

        tabsListId: '[data-tabs]',
        tabsList: null,

        showInfoBox: null
    },

    tabSettings: {
        btnDeleteId: '[data-btn-delete]',
        btnDelete: null,

        btnCancelId: '[data-btn-cancel]',
        btnCancel: null,

        formId: '[data-form]',
        form: null,

        tabItemId: '[data-tab-item]',
        tabItem: null,

        url: '',
        initGlobalPlugins: true,

        targetContentId: null,
        targetContent: null,

        getSaveModel: $.noop,
        getDeleteModel: $.noop,
        customFormValidation: null,

        onInit: $.noop,
        beforeSubmit: $.noop,

        closeTab: false,
        reloadTab: false,
        reloadGrid: false
    },

    entity: {
        entityId: 0
    },


    mainTab: {},

    tabs: [],



    init: function (options) {
        var base = this;
        var s = base.settings;

        s.infoBox = ASApp.Utils.getJqueryInstance(s.infoBoxId);
        s.tabsList = ASApp.Utils.getJqueryInstance(s.tabsListId);
        s.showInfoBox = !s.infoBox.is(':hidden');

        $.extend(true, base, options);

        base.initMainTab();
        base.initTabs();
    },

    initMainTab: function () {
        var base = this;
        var s = base.settings;

        var tab = $.extend({}, base.tabSettings);
        $.extend(tab, base.mainTab);

        base.mainTab = tab;
        base.mainTab.reloadGrid = true;
        base.mainTab.closeTab = true;

        base.mainTab.tabItemId = '[data-tab-main]';
        base.mainTab.tabItem = ASApp.Utils.getJqueryInstance(base.mainTab.tabItemId);

        base.mainTab.targetContentId = base.mainTab.tabItem.attr('href');
        base.mainTab.targetContent = ASApp.Utils.getJqueryInstance(base.mainTab.targetContentId);

        base.mainTab.url = base.mainTab.tabItem.data('href');

        base.mainTab.tabItem.click(function () {
            if (s.tabsList.is('[data-onecontent]'))
                base.getView(base.mainTab);
            else if (!$.trim(base.mainTab.targetContent.text()))
                base.getView(base.mainTab);
        });

        if (base.entity.entityId) {
            base.getView();
            ASApp.GridView.collapseGrid();
        }
    },

    initTabs: function () {
        var base = this;
        var s = base.settings;

        $(base.tabSettings.tabItemId).each(function (i, e) {
            var tempTab = base.tabs[i];

            var newTab = $.extend({}, base.tabSettings);
            $.extend(newTab, tempTab);

            tempTab = newTab;

            tempTab.tabItem = $(e);
            tempTab.url = tempTab.tabItem.data('href');

            tempTab.targetContentId = tempTab.tabItem.attr('href');
            tempTab.targetContent = ASApp.Utils.getJqueryInstance(tempTab.targetContentId);

            base.tabs[i] = tempTab;

            tempTab.tabItem.click(function () {
                if (tempTab.tabItem.parent().hasClass('disabled'))
                    return false;

                if (s.tabsList.is('[data-onecontent]'))
                    base.getView(tempTab);
                else if (!$.trim(tempTab.targetContent.text()))
                    base.getView(tempTab);

                return true;
            });
        });

    },

    getView: function (tab, options) {
        var base = this;
        var s = base.settings;

        tab = tab || base.mainTab;
        options = options || 
        {
            scrollToPartial: false
        };


        ASApp.Ajax.partial({
            url: tab.url,
            data: base.entity,
            partialWrapper: tab.targetContent,
            scrollToPartial: options.scrollToPartial,
            initGlobalPlugins: tab.initGlobalPlugins,
            onSuccess: function () {
                if (base.entity.entityId != 0 || base.entity.entityId != '')
                    base.tabs.forEach(function (e) {
                        e.tabItem.removeClass('disabled').parent().removeClass('disabled');
                    });
                else {
                    base.mainTab.tabItem.tab('show');
                    base.tabs.forEach(function (e) {
                        e.tabItem.addClass('disabled').parent().addClass('disabled');
                    });
                }

                s.infoBox.show();

                base.initForm(tab);
                base.attachHandlers(tab);
                tab.onInit(tab);
            }
        });
    },

    initForm: function (tab) {
        var base = this;

        tab.form = ASApp.Utils.getJqueryInstance(tab.targetContent.find(tab.formId));
        tab.form.submit(function (e) {
            e.preventDefault();

            tab.beforeSubmit();

            tab.form.validateBootstrap(true);
            var form = $(this);

            if (form.valid() && (!tab.customFormValidation || (tab.customFormValidation && tab.customFormValidation()))) {
                var data = ASApp.Utils.formToObject(form);

                console.log(data);

                tab.getSaveModel(data);

                ASApp.Ajax.postForm(form, {
                    data: data,
                    onSuccess: function () {
                        base.resetView(tab);
                        ASApp.Utils.scrollToTop();
                    },
                    onSuccessOptions: {
                        message: 'Éxito'
                    }
                });
            }
        });

    },

    attachHandlers: function (tab) {
        var base = this;

        tab.btnCancel = ASApp.Utils.getJqueryInstance(tab.form.find(tab.btnCancelId));
        tab.btnCancel.on('click', function () {

            if (typeof (base.entity.entityId) === 'string')
                base.entity.entityId = '';
            else
                base.entity.entityId = 0;

            base.closeTab();
        });

        tab.btnDelete = ASApp.Utils.getJqueryInstance(tab.form.find(tab.btnDeleteId));
        tab.btnDelete.on('click', function () {

            ASApp.Notify.confirmDelete(null, function () {
                var data = tab.getDeleteModel();

                if (data.code == undefined)
                    data.id = base.entity.entityId;

                ASApp.Ajax.post({
                    url: data.url,
                    data: data,
                    onSuccess: function () {
                        base.resetView();
                    },
                    onSuccessOptions: {
                        message: 'Eliminado exitoso'
                    }
                });
            });
        });
    },


    onInit: function () {

    },

    resetView: function (tab) {
        var base = this;
        var s = base.settings;

        tab = tab || base.mainTab;

        if (typeof (base.entity.entityId) === 'string')
            base.entity.entityId = '';
        else
            base.entity.entityId = 0;

        if (tab.reloadTab) {
            base.getView(tab);
        }

        if (tab.closeTab) {
            base.closeTab();
        }

        if (tab.reloadGrid) {
            ASApp.GridView.reloadGrid();
        }

    },


    closeTab: function () {
        var base = this;
        var s = base.settings;

        s.infoBox.hide();
        base.mainTab.tabItem.tab('show');
        base.mainTab.targetContent.empty();
        base.tabs.forEach(function (e) {
            e.targetContent.empty();
        });

        ASApp.Plugins.DataTables.unselect();
        ASApp.Utils.scrollToTop();
        ASApp.GridView.expandGrid();
    }
}