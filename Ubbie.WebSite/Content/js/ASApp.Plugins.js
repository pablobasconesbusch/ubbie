﻿ASApp.Plugins = {

    init: function () {
        ASApp.Plugins.setDefaults();
        ASApp.Plugins.initGlobalPlugins();
    },

    initGlobalPlugins: function (container) {
        ASApp.Plugins.Icheck.init(container);
        ASApp.Plugins.BootstrapSelect.init(container);
        ASApp.Plugins.BootstrapTooltip.init(container);
        ASApp.Plugins.Select2.init(container);
        ASApp.Plugins.BootstrapPopover.init(container);

        //Inputmask https://github.com/RobinHerbots/Inputmask
        $('[data-inputmask]').inputmask('99-99999999-9');
    },

    setDefaults: function () {
        ASApp.Plugins.BlockUI.overrideDefaults();
        ASApp.Plugins.JqueryValidation.overrideDefaults();
    }

}


// iCheck http://icheck.fronteed.com/
ASApp.Plugins.Icheck = {

    pluginOptions: {
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue'
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-icheck]'))
            container = container.find('[data-icheck]');

        container.each(function (i, e) {
            e = $(e);

            if (e.data('checkbox'))
                options.checkboxClass = e.data('checkbox');

            if (e.data('radio'))
                options.radioClass = e.data('radio');

            if (options.checkboxClass.indexOf('_line') > -1 || options.radioClass.indexOf('_line') > -1)
                options.insert = '<div class="icheck_line-icon"></div>' + e.attr('label');

            e.iCheck(options);
        });
    }

}

// Block UI http://malsup.com/jquery/block/
ASApp.Plugins.BlockUI = {

    overrideDefaults: function () {


        $.blockUI.defaults.css.width = '100%';
        $.blockUI.defaults.css.border = 'none';
        $.blockUI.defaults.css.backgroundColor = 'transparent';
        $.blockUI.defaults.css.top = '50%';
        $.blockUI.defaults.css.left = '0';
        $.blockUI.defaults.overlayCSS.backgroundColor = '#888';
        $.blockUI.defaults.baseZ = 1060;

    },

    block: function () {
        $.blockUI();
    },

    blockElement: function (container) {
        container = ASApp.Utils.getJqueryInstance(container);
        container.block();
    },

    unblock: function () {
        $.unblockUI();
    },
    unblockElement: function (container) {
        container = ASApp.Utils.getJqueryInstance(container);
        container.unblock();
    }

}

// Jquery Validation https://jqueryvalidation.org/
ASApp.Plugins.JqueryValidation = {

    overrideDefaults: function () {
        $.validator.setDefaults({
            ignore: '.jqv-ignore'
        });
    },

    reAttach: function (form) {

        if (ASApp.Plugins.JqueryValidationBootstrap) {
            ASApp.Plugins.JqueryValidationBootstrap.reAttach(form);
        } else {
            form = ASApp.Utils.getJqueryInstance(form);

            form.removeData('validator');
            form.removeData('unobtrusiveValidation');

            $.validator.unobtrusive.parse(form);
        }

    },

    serverValidationToForm: function (form, state) {
        form = ASApp.Utils.getJqueryInstance(form);
        $(state).each(function (i, e) {
            var $valMsg = form.find('span[data-valmsg-for="' + e.Key + '"]');
            if ($valMsg.length) {
                $valMsg.empty();

                $(e.Errors).each(function (ix, el) {
                    var $span = $('<span>', { id: e.Key + '-error' });
                    $valMsg.append($span.html(el.ErrorMessage));
                });
            }
        });
    }
}

// Jquery Validation Unobtrusive Bootstrap https://github.com/sandrocaseiro/jquery.validate.unobtrusive.bootstrap
ASApp.Plugins.JqueryValidationBootstrap = {

    reAttach: function (form) {
        form = ASApp.Utils.getJqueryInstance(form);
        form.validateBootstrap(true);
    }
}

// AutoNumeric http://www.decorplanit.com/plugin/
ASApp.Plugins.AutoNumeric = {

    pluginOptions: {
        aSign: '$',
        aSep: '.',
        aDec: ','
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-autonumeric]'))
            container = container.find('[data-autonumeric]');

        container.each(function (i, e) {
            e = $(e);
            e.autoNumeric(options);
            e.autoNumeric('update');
        });
    }
}

// Bootstrap Datepicker https://bootstrap-datepicker.readthedocs.io/en/latest/
ASApp.Plugins.BootstrapDatepicker = {

    pluginOptions: {
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es'
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-bootstrap-datepicker]'))
            container = container.find('[data-bootstrap-datepicker]');

        container.each(function (i, e) {
            e = $(e);
            e.datepicker(options);
        });
    }
}

// Bootstrap Select https://silviomoreto.github.io/bootstrap-select/
ASApp.Plugins.BootstrapSelect = {

    pluginOptions: {
        iconBase: 'fa',
        tickIcon: 'fa-check',
        noneSelectedText: '',
        selectAllText: '',
        deselectAllText: ''
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        options.noneSelectedText = ASApp.settings.PluginBootstrapSelectNoneSelectedText;
        options.selectAllText = ASApp.settings.PluginBootstrapSelectAllText;
        options.deselectAllText = ASApp.settings.PluginBootstrapSelectDeselectAllText;

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-bootstrap-select]'))
            container = container.find('[data-bootstrap-select]');

        container.each(function (i, e) {
            e = $(e);
            e.selectpicker(options);
        });
    }
}

// Bootstrap Tooltip http://getbootstrap.com/javascript/#tooltips
ASApp.Plugins.BootstrapTooltip = {

    pluginOptions: {
        container: 'body',
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-bs-tooltip]'))
            container = container.find('[data-bs-tooltip]');

        container.each(function (i, e) {
            e = $(e);

            if (e.is('[data-style]')) {
                options.template = '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner" style="' + e.data('style') + '"></div></div>';
            }

            e.tooltip(options);
        });
    }
}

// DataTables https://datatables.net/
ASApp.Plugins.DataTables = {

    pluginOptions: {
        responsive: true,
        processing: true,
        serverSide: false,
        pageLength: 50,
        info: true,
        autoWidth: false,
        dom: 'lftipr',
        language: {
            url: ASApp.settings.DataTablesLanguageLocation
        },

        ajax: $.noop,
        initComplete: $.noop
    },

    initGrid: function (grid, options) {
        var me = this;
        var newConfig = $.extend({}, me.pluginOptions);

        if (options.isSetupGrid == undefined)
            options.isSetupGrid = true;


        $.extend(newConfig, options);

        newConfig.serverSide = true;
        newConfig.dom = 'trp';

        newConfig.ajax = function (data, callback, settings) {

            var pager = {
                PageIndex: 1,
                PageSize: 0,
                SortField: '',
                SortDirection: 'ASC',
                Query: data.search['value']
            };

            if (data.order && data.order.length > 0) {
                var orderColumn = data.columns[data.order[0].column];

                if (orderColumn.name != "")
                    pager.SortField = orderColumn.name;
                else
                    pager.SortField = orderColumn.data; pager.SortDirection = data.order[0].dir.toUpperCase();
            }

            if (data.length > 0) {
                pager.PageIndex = (data.start / data.length) + 1;
                pager.PageSize = data.length;
            };

            var postData = pager;

            if (settings.oInit.params) {
                postData = $.extend(settings.oInit.params(), pager);
            }

            $.ajax({
                type: 'POST',
                url: settings.oInit.url,
                data: JSON.stringify(postData),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (sr) {

                    var fillData = {
                        data: [],
                        recordsTotal: 0,
                        recordsFiltered: 0
                    }

                    if (sr.Status) {
                        fillData.data = sr.Data;
                        fillData.recordsTotal = sr.ReturnValue;
                        fillData.recordsFiltered = sr.ReturnValue;
                    } else {
                        fillData.data = sr;
                        fillData.recordsTotal = sr.length;
                        fillData.recordsFiltered = sr.length;
                    }

                    callback(fillData);
                }
            });
        };


        if (newConfig.isSetupGrid) {
            newConfig.initComplete = function (settings) {
                var table = new $.fn.dataTable.Api(settings);

                table.on('click', 'tbody tr', function () {
                    if (!settings.oInit.onSelect)
                        return;

                    var tr = this;
                    var row = table.row(this);
                    var data = row.data();

                    table.$('tr.bg-blue').removeClass('bg-blue font-white');
                    $(tr).addClass('bg-blue font-white');

                    settings.oInit.onSelect(data);

                    ASApp.GridView.toggleGrid();
                });


                ASApp.GridView.settings.gridQuery.on('keyup', function (e) {
                    if (!this.value && !e.ctrlKey)
                        $(this).change();
                });


                ASApp.GridView.settings.gridQuery.on('change', function () {
                    ASApp.GridView.settings.grid.DataTable().search(this.value).draw();
                });
            };

            newConfig.onSelect = function (item) {

                if (item) {
                    if (item.Id) {
                        ASApp.InfoView.entity = {
                            entityId: item.Id
                        }
                    } else {
                        ASApp.InfoView.entity = {
                            entityId: item.Codigo
                        }
                    }
                    ASApp.InfoView.getView(ASApp.InfoView.mainTab);
                }
            }
        }




        grid = ASApp.Utils.getJqueryInstance(grid);
        return grid.DataTable(newConfig);
    },

    initDOM: function (grid, options) {
        var me = this;

        var newConfig = $.extend({}, me.pluginOptions);
        newConfig.processing = false;
        newConfig.info = false;
        newConfig.ajax = null;
        newConfig.dom = 'lftipr';

        if (options)
            $.extend(me.newConfig, options);

        grid = ASApp.Utils.getJqueryInstance(grid);
        grid.DataTable(newConfig);
    },

    unselect: function () {
        ASApp.GridView.settings.grid.find('tr.bg-blue font-white').removeClass('bg-blue font-white');
    }
}

// Jquery Awesome Cursor https://jwarby.github.io/jquery-awesome-cursor/
ASApp.Plugins.JqueryAwesomeCursor = {

    pluginOptions: {
        color: '#000000',
        size: 14,
        hotspot: [0, 0],
        flip: '',
        rotate: 0,
        outline: null
    },

    iconName: 'arrows',

    init: function (container, iconName, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        iconName = iconName || this.iconName;

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-awesome-cursor]'))
            container = container.find('[data-awesome-cursor]');

        container.each(function (i, e) {
            e = $(e);
            e.awesomeCursor(iconName, options);
        });
    }
}

// Jquery Mini Colors https://github.com/claviska/jquery-minicolors
ASApp.Plugins.JqueryMinicolors = {

    pluginOptions: {
        animationSpeed: 50,
        animationEasing: 'swing',
        change: null,
        changeDelay: 0,
        control: 'hue',
        dataUris: true,
        defaultValue: '',
        format: 'hex',
        hide: null,
        hideSpeed: 100,
        inline: false,
        keywords: '',
        letterCase: 'lowercase',
        opacity: false,
        position: 'bottom left',
        show: null,
        showSpeed: 100,
        theme: 'bootstrap',
        swatches: []
    },


    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-minicolors]'))
            container = container.find('[data-minicolors]');

        container.each(function (i, e) {
            e = $(e);
            e.minicolors(options);
        });
    }
}

// Select2 https://select2.github.io/
ASApp.Plugins.Select2 = {

    pluginOptions: {
        theme: 'bootstrap',
        width: 'auto'
    },


    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-select-2]'))
            container = container.find('[data-select-2]');

        container.each(function (i, e) {
            e = $(e);
            e.select2(options);
        });
    }
}

// TinyMCE https://www.tinymce.com/
ASApp.Plugins.TinyMCE = {

    pluginOptions: {
        height: 500,
        language: 'es',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image'
    },


    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);


        if (!container.is('[data-tinymce]')) {
            tinymce.remove('#' + container.find('textarea[data-tinymce]').prop('id'));
            container = container.find('[data-tinymce]');
        } else {
            tinymce.remove('#' + container.prop('id'));
        }

        tinyMCE.baseURL = "/Content/vendor/tinymce";

        container.each(function (i, e) {
            e = $(e);
            e.tinymce(options);
        });
    }
}

// TypeWatch https://github.com/dennyferra/TypeWatch
ASApp.Plugins.TypeWatch = {

    pluginOptions: {
        callback: $.noop
    },


    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);


        if (!container.is('[data-typewatch]'))
            container = container.find('[data-typewatch]');

        container.each(function (i, e) {
            e = $(e);
            e.typeWatch(options);
        });
    }
}

// Textarea Autosize https://github.com/javierjulio/textarea-autosize
ASApp.Plugins.TextareaAutosize = {

    pluginOptions: {

    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);


        if (!container.is('[data-textarea-autosize]'))
            container = container.find('[data-textarea-autosize]');

        container.each(function (i, e) {
            e = $(e);
            e.textareaAutoSize(options);
        });
    }

}

// Jquery Slimscroll http://rocha.la/jQuery-slimScroll
ASApp.Plugins.JquerySlimscroll = {

    pluginOptions: {
        touchScrollStep: 50
    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-slimscroll]'))
            container = container.find('[data-slimscroll]');

        container.each(function (i, e) {
            e = $(e);

            if (e.data('height'))
                options.height = e.data('height');

            e.slimScroll(options);
        });
    }
}

// Bootstrap Tagsinput https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
ASApp.Plugins.BootstrapTagsinput = {

    pluginOptions: {
    },


    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);


        if (!container.is('[data-bootstrap-tagsinput]'))
            container = container.find('[data-bootstrap-tagsinput]');

        container.each(function (i, e) {
            e = $(e);
            e.tagsinput(options);
        });
    }
}


// Bootstrap Popover http://getbootstrap.com/javascript/#popovers
ASApp.Plugins.BootstrapPopover = {

    pluginOptions: {

    },

    init: function (container, pluginOptions) {
        container = container || 'body';

        var options = $.extend({}, this.pluginOptions);

        if (pluginOptions)
            $.extend(options, pluginOptions);

        container = ASApp.Utils.getJqueryInstance(container);

        if (!container.is('[data-bs-popover]'))
            container = container.find('[data-bs-popover]');

        container.each(function (i, e) {
            e = $(e);

            e.popover(options);
        });
    }
}