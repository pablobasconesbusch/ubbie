﻿
ASApp.Session = {

    timer: null,
    timeOut: null,
    timerRunning: false,
    activeUsuarioEmail: '',

    init: function (email) {
        var _this = ASApp.Session;

        if (!_this.activeUsuarioEmail)
            _this.activeUsuarioEmail = email;

        if (_this.timerRunning)
            clearTimeout(_this.timer);

        _this.timer = setTimeout(_this.promptLogin, _this.timeOut);
        _this.timerRunning = true;
    },

    promptLogin: function () {
        var _this = ASApp.Session;
        clearTimeout(_this.timer);

        ASApp.Ajax.partial({
            data: { email: _this.activeUsuarioEmail },
            url: URLs.Account.PromptLogin,
            partialWrapper: 'body',
            append: true
        });

    }

}