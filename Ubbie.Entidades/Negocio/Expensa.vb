﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Negocio
    Public Class Expensa

        <Key>
        Public Property Id As Integer

        Public Property ConsorcioId As Integer

        Public Property Mes As Integer
        Public Property Año As Integer

        Public Property DVH As String

        <ForeignKey("ConsorcioId")>
        Public Property Consorcio As Consorcio

        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer

    End Class
End Namespace