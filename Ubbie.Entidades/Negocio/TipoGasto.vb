﻿Imports System.ComponentModel.DataAnnotations

Namespace Negocio
    Public Class TipoGasto

        <Key>
        Public Property Id As Integer

        Public Property Nombre As String
        Public Property Letra As String

        Public Property DVH As String

    End Class
End Namespace