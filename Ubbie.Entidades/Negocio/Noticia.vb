﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable

Namespace Negocio
    Public Class Noticia
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property NoticiaCategoriaId As Integer
        Public Property ConsorcioId As Integer?

        Public Property Fecha As DateTime
        Public Property Titulo As String
        Public Property ImagenId As Integer
        Public Property ImagenHDId As Integer
        Public Property Cuerpo As String

        Public Property DVH As String

        <ForeignKey("NoticiaCategoriaId")>
        Public Property NoticiaCategoria As NoticiaCategoria

        '<ForeignKey("ConsorcioId")>
        'Public Property Consorcio As Consorcio

    End Class
End Namespace