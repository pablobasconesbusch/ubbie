﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable

Namespace Negocio
    Public Class Gasto
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property ConsorcioId As Integer
        Public Property TipoGastoId As Integer

        Public Property Descripcion As String
        Public Property NroComprobante As String
        Public Property Monto As Decimal
        Public Property Fecha As DateTime
        Public Property ProcesadoEnExpensa As Boolean

        Public Property DVH As String

        <ForeignKey("ConsorcioId")>
        Public Property Consorcio As Consorcio

        <ForeignKey("TipoGastoId")>
        Public Property TipoGasto As TipoGasto


    End Class
End Namespace