﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable
Imports Ubbie.Entidades.Seguridad

Namespace Negocio
    Public Class Reclamo
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property UsuarioId As Integer

        Public Property Numero As String
        Public Property Comentario As String
        Public Property Solucion As String

        Public Property Pendiente As Boolean
        Public Property Solucionado As Boolean

        Public Property DVH As String

        <ForeignKey("UsuarioId")>
        Public Property Usuario As Usuario

    End Class
End Namespace