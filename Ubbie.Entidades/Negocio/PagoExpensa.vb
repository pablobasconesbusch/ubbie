﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Seguridad

Namespace Negocio
    Public Class PagoExpensa

        <Key>
        Public Property Id As Integer

        Public Property UnidadId As Integer
        Public Property PropietarioId As Integer
        Public Property ExpensaId As Integer

        Public Property Monto As Decimal
        Public Property Pagado As Boolean

        <ForeignKey("UnidadId")>
        Public Property Unidad As Unidad

        <ForeignKey("PropietarioId")>
        Public Property Propietario As Usuario

        <ForeignKey("ExpensaId")>
        Public Property Expensa As Expensa

        Public Property DVH As String
    End Class
End Namespace