﻿Imports System.ComponentModel.DataAnnotations
Imports Ubbie.Entidades.Heredable

Namespace Negocio
    Public Class Consorcio
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property Nombre As String
        Public Property Direccion As String
        Public Property Numero As String
        Public Property CUIT As String
        Public Property CodigoPostal As Integer

        Public Property RecaudadoFondoReserva As Double?

        Public Property DVH As String

    End Class
End Namespace