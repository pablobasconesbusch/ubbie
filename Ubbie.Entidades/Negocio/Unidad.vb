﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable
Imports Ubbie.Entidades.Seguridad

Namespace Negocio
    Public Class Unidad
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property PropietarioId As Integer
        Public Property ConsorcioId As Integer
        Public Property TipoUnidadId As Integer

        Public Property Nombre As String

        Public Property Ambientes As Integer
        Public Property Metros As Double

        Public Property DVH As String


        <ForeignKey("PropietarioId")>
        Public Property Propietario As Usuario

        <ForeignKey("ConsorcioId")>
        Public Property Consorcio As Consorcio

        <ForeignKey("TipoUnidadId")>
        Public Property TipoUnidad As TipoUnidad

    End Class
End Namespace