﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Soporte
    Public Class EmailAttachment

        <Key>
        Public Property Id As Integer

        Public Property EmailId As Integer
        <ForeignKey("EmailId")>
        Public Property Email As Email

        Public Property Url As String
        Public Property FileName As String

        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer

    End Class
End Namespace