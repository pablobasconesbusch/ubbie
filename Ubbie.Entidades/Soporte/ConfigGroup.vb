﻿Imports System.ComponentModel.DataAnnotations

Namespace Soporte
    Public Class ConfigGroup

        <Key>
        Public Property Code As String

        Public Property Description As String

        Public Property ConfigSettings As List(Of ConfigSetting)
    End Class
End Namespace