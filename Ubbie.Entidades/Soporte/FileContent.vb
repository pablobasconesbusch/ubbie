﻿Imports System.ComponentModel.DataAnnotations

Namespace Soporte
    Public Class FileContent
        <Key>
        Public Property Id As Integer

        Public Property RawData As Byte()
    End Class
End Namespace