﻿Imports System.ComponentModel.DataAnnotations

Namespace Soporte
    Public Class TimeZone

        <Key>
        Public Property Code As String

        Public Property Description As String
        Public Property DisplayName As String

    End Class
End Namespace