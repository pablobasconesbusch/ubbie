﻿Imports System.ComponentModel.DataAnnotations

Namespace Soporte
    Public Class EmailTemplate

        <Key>
        Public Property Code As String

        Public Property Layout As String
        Public Property Subject As String

    End Class
End Namespace