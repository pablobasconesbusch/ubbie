﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Framework.Seguridad

Namespace Soporte
    Public Class File

        <Key>
        Public Property Id As Integer

        Public Property FileContentId As Integer?
        <ForeignKey("FileContentId")>
        Public Property FileContent As FileContent

        Public Property FileName As String
        Public Property MimeType As String
        Public Property Extension As String
        Public Property Folder As String
        Public Property Guid As String
        Public Property Mode As String

        Public Property ThumbnailFileId As Integer
        Public Property IsProcessing As Boolean

        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer

        <NotMapped>
        Public Property Key As String = CustomEncrypt.Encrypt(Id)
    End Class
End Namespace