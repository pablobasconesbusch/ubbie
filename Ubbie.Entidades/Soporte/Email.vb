﻿Imports System.ComponentModel.DataAnnotations

Namespace Soporte
    Public Class Email

        <Key>
        Public Property Id As Integer

        Public Property Subject As String
        Public Property Body As String
        Public Property Recipients As String
        Public Property Sender As String
        Public Property Important As Boolean

        Public Property EmailStatusCode As String
        Public Property Log As String

        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer

        Public Property EmailAttachments As New List(Of EmailAttachment)
    End Class
End Namespace