﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Seguridad

Namespace Soporte
    Public Class EventLog
        <Key>
        Public Property Id As Integer

        Public Property [Date] As DateTime

        Public Property UserId As Integer?
        <ForeignKey("UserId")>
        Public Property User As Usuario

        Public Property EventCode As String
        Public Property LogLevel As String

        Public Property ServerException As String
        Public Property RequestType As String
        Public Property UserAgent As String
        Public Property Path As String

        Public Property QueryString As String
        Public Property Data As String
    End Class
End Namespace