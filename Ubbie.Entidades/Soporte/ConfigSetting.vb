﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Soporte
    Public Class ConfigSetting

        <Key>
        Public Property Id As Integer

        Public Property ConfigGroupcode As String
        <ForeignKey("ConfigGroupcode")>
        Public Property ConfigGroup As ConfigGroup

        Public Property Key As String
        Public Property Description As String

        Public Property Value As String
        Public Property Type As String

        Public Property IsEditable As Boolean
        Public Property IsDeleted As Boolean

    End Class
End Namespace