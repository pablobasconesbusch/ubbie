﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable
Imports Ubbie.Entidades.Soporte

Namespace Seguridad

    Public Class Usuario
        Inherits EntidadAuditable

        <Key>
        Public Property Id As Integer

        Public Property Nombre As String
        Public Property Apellido As String

        <NotMapped>
        Public Property NombreCompleto As String = $"{Nombre} {Apellido}"

        Public Property Email As String
        Public Property Password As String

        Public Property DNI As Long?
        Public Property Telefono As String
        Public Property FechaNacimiento As DateTime?

        Public Property CodigoRol As String

        <ForeignKey("CodigoRol")>
        Public Property Rol As Rol

        Public Property DefaultCodigoFamilia As String
        <ForeignKey("DefaultCodigoFamilia")>
        Public Property DefaultFamilia As Familia

        Public Property PendienteActivacion As Boolean
        Public Property CII As Integer
        Public Property DebeResetearPassword As Boolean

        Public Property ImagenPerfilId As Integer?
        <ForeignKey("ImagenPerfilId")>
        Public Property ImagenPerfil As File

        Public Property TimeZoneCode As String
        <ForeignKey("TimeZoneCode")>
        Public Property TimeZone As TimeZone

        Public Property Activo As Boolean
        Public Property EsSuperUsuario As Boolean

        Public Property DVH As String
    End Class
End Namespace