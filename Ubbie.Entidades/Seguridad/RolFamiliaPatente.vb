﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad

    Public Class RolFamiliaPatente
        <Key, Column(Order:=1)>
        Public Property CodigoRol As String
        <ForeignKey("CodigoRol")>
        Public Property Rol As Rol

        <Key, Column(Order:=2)>
        Public Property CodigoPatente As String
        <ForeignKey("CodigoPatente")>
        Public Property Patente As Patente

        <Key, Column(Order:=3)>
        Public Property CodigoFamilia As String
        <ForeignKey("CodigoFamilia")>
        Public Property Familia As Familia
    End Class
End Namespace