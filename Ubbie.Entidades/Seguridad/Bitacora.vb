﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad

    Public Class Bitacora
        <Key>
        Public Property Id As Integer
        Public Property DateTime As DateTime

        Public Property UsuarioId As Integer?
        <ForeignKey("UsuarioId")>
        Public Property Usuario As Usuario

        Public Property Detalle As String
        Public Property IP As String
        Public Property Criticidad As String

        Public Property DVH As String
    End Class
End Namespace