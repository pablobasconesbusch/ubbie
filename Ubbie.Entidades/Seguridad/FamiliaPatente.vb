﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad
    Public Class FamiliaPatente
        <Key, Column(Order:=1)>
        Public Property CodigoFamilia As String
        <Key, Column(Order:=2)>
        Public Property CodigoPatente As String

        <ForeignKey("CodigoFamilia")>
        Public Property Familia As Familia
        <ForeignKey("CodigoPatente")>
        Public Property Patente As Patente
    End Class
End Namespace