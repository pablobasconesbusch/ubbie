﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad
    Public Class Familia
        <Key>
        Public Property Codigo As String

        Public Property Descripcion As String

        Public Property Controller As String
        Public Property Action As String

        Public Property ParentCode As String

        Public Property IsBackend As Boolean


        <ForeignKey("ParentCode")>
        Public Property Padre As Familia

        Public Property FamiliaPatentes As List(Of FamiliaPatente)
    End Class
End Namespace