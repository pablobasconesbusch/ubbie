﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad

    Public Class UsuarioNotificacion
        <Key>
        Public Property Id As Integer

        Public Property UsuarioId As Integer
        <ForeignKey("UsuarioId")>
        Public Property Usuario As Usuario

        Public Property Titulo As String
        Public Property Mensaje As String

        Public Property Leido As Boolean
        Public Property FechaEnvio As DateTime
    End Class
End Namespace