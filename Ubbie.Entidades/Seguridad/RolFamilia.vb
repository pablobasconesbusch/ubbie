﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Seguridad

    Public Class RolFamilia
        <Key, Column(Order:=1)>
        Public Property CodigoRol As String
        <ForeignKey("CodigoRol")>
        Public Property Rol As Rol

        <Key, Column(Order:=2)>
        Public Property CodigoFamilia As String
        <ForeignKey("CodigoFamilia")>
        Public Property Familia As Familia

        Public Property RolFamiliaPatentes As List(Of RolFamiliaPatente)
    End Class
End Namespace