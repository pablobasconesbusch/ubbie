﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema
Imports Ubbie.Entidades.Heredable

Namespace Seguridad
    Public Class Rol
        Inherits EntidadAuditable

        <Key>
        Public Property Codigo As String

        Public Property Nombre As String

        Public Property DefaultCodigoFamilia As String
        <ForeignKey("DefaultCodigoFamilia")>
        Public Property DefaultFamilia As Familia

        Public Property RolFamilias As List(Of RolFamilia)
        Public Property Usuarios As List(Of Usuario)

    End Class
End Namespace