﻿Namespace Heredable
    Public Class EntidadAuditable
        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer
        Public Property FechaEdicion As DateTime?
        Public Property EditadoPor As Integer?
        Public Property Eliminado As Boolean
    End Class
End Namespace