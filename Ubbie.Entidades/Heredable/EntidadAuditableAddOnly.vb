﻿Namespace Heredable
    Public Class EntidadAuditableAddOnly
        Public Property FechaCreacion As DateTime
        Public Property CreadoPor As Integer
        Public Property Eliminado As Boolean
    End Class
End Namespace