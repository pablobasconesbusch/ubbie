﻿using System.Collections.Generic;
using System.Linq;

namespace Ubbie.Framework.Extensiones
{
    public static class ObjectExtensions
    {
        public static bool IsNull(this object item)
        {
            return item == null;
        }
        public static T IsNull<T>(this T item, T result)
        {
            return item.IsNull() ? result : item;
        }
        public static bool IsNotNull(this object item)
        {
            return item != null;
        }



        public static bool IsIn<T>(this T item, List<T> list)
        {
            return list.Contains(item);
        }
        public static bool IsIn<T>(this T item, params T[] list)
        {
            return item.IsIn(list.ToList());
        }
    }
}