using System;
using System.Globalization;

namespace Ubbie.Framework.Extensiones
{
    public static class ExtensionMethods
    {
        public static int ToInt(this string value)
        {
            int ret;
            int.TryParse(value, out ret);
            return ret;
        }
        public static int ToInt(this int? value)
        {
            return value.IsNull(0).Value;
        }



        public static bool ToBool(this object value)
        {
            if (value.IsNull() || value == DBNull.Value)
                return false;
            if (value is int)
                return Convert.ToBoolean(value);
            return ToBool(value.ToString());
        }
        public static bool ToBool(this string value)
        {
            bool ret;
            bool.TryParse(value, out ret);
            return ret;
        }

        public static DateTime DecryptDateTime(this string value)
        {
            return DateTime.Parse(value, new CultureInfo("en-US"));
        }



        public static int GetAge(this DateTime date)
        {
            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (date.Year * 100 + date.Month) * 100 + date.Day;

            return (a - b) / 10000;
        }
        public static int GetAge(this DateTime? date)
        {
            if (date == null)
                return 0;

            return date.Value.GetAge();
        }



        public static DateTime ToDateTime(this object value, string timeZoneCode = "Argentina Standard Time")
        {
            if (value == null || value == DBNull.Value || string.IsNullOrEmpty(value.ToString()))
                return new DateTime();

            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneCode);
            var offset = new DateTimeOffset(DateTime.Parse(value.ToString()), timeZoneInfo.BaseUtcOffset);

            return offset.UtcDateTime;
        }
        public static DateTime ToUserDateTime(this DateTime value, string timeZoneCode = "Argentina Standard Time")
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneCode);

            if (value.Kind == DateTimeKind.Local)
                return TimeZoneInfo.ConvertTime(value, timeZoneInfo);

            return TimeZoneInfo.ConvertTimeFromUtc(value, timeZoneInfo);
        }

    }
}
