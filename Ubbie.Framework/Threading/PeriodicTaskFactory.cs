﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Ubbie.Framework.Threading
{
	/// <summary>
	/// Factory class to create a periodic Task to simulate a <see cref="System.Threading.Timer"/> using <see cref="Task">Tasks.</see>
	/// </summary>
	public static class PeriodicTaskFactory
	{
		/// <summary>
		/// Starts the periodic task.
		/// </summary>
		/// <param name="action">The action.</param>
		/// <param name="intervalInMilliseconds">The interval in milliseconds.</param>
		/// <param name="delayInMilliseconds">The delay in milliseconds, i.e. how long it waits to kick off the timer.</param>
		/// <param name="duration">The duration.
		/// <example>If the duration is set to 10 seconds, the maximum time this task is allowed to run is 10 seconds.</example></param>
		/// <param name="maxIterations">The max iterations.</param>
		/// <param name="synchronous">if set to <c>true</c> executes each period in a blocking fashion and each periodic execution of the task
		/// is included in the total duration of the Task.</param>
		/// <param name="cancelToken">The cancel token.</param>
		/// <param name="periodicTaskCreationOptions"><see cref="TaskCreationOptions"/> used to create the task for executing the <see cref="Task"/>.</param>
		/// <returns>A <see cref="Task"/></returns>
		/// <remarks>
		/// Exceptions that occur in the <paramref name="action"/> need to be handled in the action itself. These exceptions will not be 
		/// bubbled up to the periodic task.
		/// </remarks>
		public static Task Start(Action action,
			int intervalInMilliseconds = Timeout.Infinite,
			int delayInMilliseconds = 0,
			int duration = Timeout.Infinite,
			int maxIterations = -1,
			bool synchronous = false,
			CancellationToken cancelToken = new CancellationToken(),
			TaskCreationOptions periodicTaskCreationOptions = TaskCreationOptions.None)
		{
			var stopWatch = new Stopwatch();
			Action wrapperAction = () =>
			{
				CheckIfCancelled(cancelToken);
				action();
			};

			Action mainAction = () =>
			{
				MainPeriodicTaskAction(intervalInMilliseconds, delayInMilliseconds, duration, maxIterations, cancelToken, stopWatch, synchronous, wrapperAction, periodicTaskCreationOptions);
			};

			return Task.Factory.StartNew(mainAction, cancelToken, TaskCreationOptions.LongRunning, TaskScheduler.Current);
		}

		/// <summary>
		/// Mains the periodic task action.
		/// </summary>
		/// <param name="intervalInMilliseconds">The interval in milliseconds.</param>
		/// <param name="delayInMilliseconds">The delay in milliseconds.</param>
		/// <param name="duration">The duration.</param>
		/// <param name="maxIterations">The max iterations.</param>
		/// <param name="cancelToken">The cancel token.</param>
		/// <param name="stopWatch">The stop watch.</param>
		/// <param name="synchronous">if set to <c>true</c> executes each period in a blocking fashion and each periodic execution of the task
		/// is included in the total duration of the Task.</param>
		/// <param name="wrapperAction">The wrapper action.</param>
		/// <param name="periodicTaskCreationOptions"><see cref="TaskCreationOptions"/> used to create a sub task for executing the <see cref="Action"/>.</param>
		private static void MainPeriodicTaskAction(int intervalInMilliseconds,
			int delayInMilliseconds,
			int duration,
			int maxIterations,
			CancellationToken cancelToken,
			Stopwatch stopWatch,
			bool synchronous,
			Action wrapperAction,
			TaskCreationOptions periodicTaskCreationOptions)
		{
			var subTaskCreationOptions = TaskCreationOptions.AttachedToParent | periodicTaskCreationOptions;

			CheckIfCancelled(cancelToken);

			if (delayInMilliseconds > 0)
				Thread.Sleep(delayInMilliseconds);

			if (maxIterations == 0)
				return;
			var iteration = 0;

			stopWatch.Start();
			while (true)
			{
				CheckIfCancelled(cancelToken);

				var subTask = Task.Factory.StartNew(wrapperAction, cancelToken, subTaskCreationOptions, TaskScheduler.Current);
				subTask.Wait(cancelToken);

				iteration++;

				if (maxIterations > 0 && iteration >= maxIterations)
					break;

				Thread.Sleep(intervalInMilliseconds);

				if (duration > 0 && stopWatch.ElapsedMilliseconds >= duration)
					break;
			}
			stopWatch.Stop();
		}

        /// <summary>
        /// Checks if cancelled.
        /// </summary>
        /// <param name="cancelToken">The cancel token.</param>
        /// <param name="cancellationToken"></param>
        static void CheckIfCancelled(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
        }
    }
}