﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Ubbie.Framework.Mapping
{
	public class MappingHelper
	{
		private static string GetMemberName(Expression expression)
		{
			// Reference type
			var memberExpression = expression as MemberExpression;
			if (memberExpression != null)
			{
				if (memberExpression.Expression.NodeType == ExpressionType.MemberAccess)
				{
					return GetMemberName(memberExpression.Expression)
						+ "."
						+ memberExpression.Member.Name;
				}
				return memberExpression.Member.Name;
			}

			// Value Type
			var unaryExpression = expression as UnaryExpression;
			if (unaryExpression != null)
			{
				if (unaryExpression.NodeType != ExpressionType.Convert)
					throw new Exception(string.Format("Cannot interpret member from {0}", expression));

				return GetMemberName(unaryExpression.Operand);
			}

			// Anonymous type
			var newType = expression as NewExpression;
			if (newType != null)
			{
				return newType.Members.First().Name;
			}

			throw new Exception(string.Format("Could not determine member from {0}", expression));
		}

		private static string[] GetMemberNames(Expression expression)
		{
			var ret = new List<string>();

			// Lambda type
			var lambdaExpression = expression as LambdaExpression;
			if (lambdaExpression != null)
			{
				var arrayBody = lambdaExpression.Body as NewArrayExpression;
				if (arrayBody != null)
				{
					foreach (var exp in arrayBody.Expressions)
					{
						ret.Add(GetMemberName(exp));
					}
				}

				var expressionBody = lambdaExpression.Body as NewExpression;
				if (expressionBody != null)
				{
					foreach (var exp in expressionBody.Arguments)
					{
						ret.Add(GetMemberName(exp));
					}
				}
			}

			return ret.ToArray();
		}

		public static void MapEntity(object entity, object reference)
		{
			MapEntity(entity, reference, new string[] { });
		}

		public static void MapEntity<T>(object entity, object reference, Expression<Func<T, object>> columns)
		{
			MapEntity(entity, reference, GetMemberNames(columns));
		}

		public static void MapEntity(object entity, object reference, string[] columns)
		{
			foreach (var pd in reference.GetType().GetProperties().Where(x => columns.Length == 0 || columns.Contains(x.Name)))
			{
				var ps = entity.GetType().GetProperty(pd.Name,
					BindingFlags.IgnoreCase |
					BindingFlags.Instance |
					BindingFlags.Public);

				if (ps != null)
				{
					var value = ps.GetValue(entity);
					var model = value as IMapperModel;

					if (model != null)
					{
						var newReference = Activator.CreateInstance(model.GetEntityType());
						MapEntity(model, newReference);
						pd.SetValue(reference, newReference);
					}
					else
					{
						try
						{
							pd.SetValue(reference, ps.GetValue(entity));
						}
						catch (Exception ex)
						{
							// Ignored
						}
					}
				}
			}
		}

		public static E MapModelToEntity<E>(object model)
		{
			var entity = (E)Activator.CreateInstance(typeof(E));
			MapEntity(model, entity);
			return entity;
		}

		public static List<E> MapMapperModelListToEntityList<M, E>(List<M> list) where M : IMapperModel
		{
			var ret = new List<E>();

			foreach (var model in list)
			{
				var toEntity = model.GetType().GetMethod("ToEntity", BindingFlags.Public | BindingFlags.Instance);
				ret.Add((E)toEntity.Invoke(model, null));
			}

			return ret;
		}

		public static List<M> MapEntityListToMapperModelList<E, M>(List<E> list) where M : IMapperModel
		{
			var ret = new List<M>();

			foreach (var entity in list)
			{
				var model = MapModelToEntity<M>(entity);
				var extraMap = model.GetType().GetMethod("ExtraMapFromEntity", BindingFlags.NonPublic | BindingFlags.Instance);
				extraMap.Invoke(model, new object[] { entity });

				ret.Add(model);
			}

			return ret;
		}
	}
}