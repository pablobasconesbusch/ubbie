﻿using System;

namespace Ubbie.Framework.Mapping
{
    public interface IMapperModel
    {
        Type GetModelType();
        Type GetEntityType();
        Type GetKeyType();
    }
}