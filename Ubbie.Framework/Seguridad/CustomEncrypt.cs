﻿using Ubbie.Framework.Extensiones;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Ubbie.Framework.Seguridad
{
    public static class CustomEncrypt
    {
        private static string RijndaelKey => "Passw0rd!16chars";

        public static string Encrypt(string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            return HttpUtility.UrlEncode(EncryptRijndael(text, RijndaelKey));
        }

        public static string Encrypt(int text)
        {
            return Encrypt(text.ToString());
        }

        public static string Decrypt(string value)
        {
            if (value.IsNullOrEmpty())
                return string.Empty;

            value = HttpUtility.UrlDecode(value);
            value = value?.Replace(" ", "+");
            return DecryptRijndael(value, RijndaelKey);
        }



        private static string EncryptRijndael(string value, string encryptionKey)
        {
            var key = Encoding.UTF8.GetBytes(encryptionKey); // must be 16 chars
            var rijndael = new RijndaelManaged
            {
                BlockSize = 128,
                IV = key,
                KeySize = 128,
                Key = key
            };

            var transform = rijndael.CreateEncryptor();
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                {
                    var buffer = Encoding.UTF8.GetBytes(value);

                    cs.Write(buffer, 0, buffer.Length);
                    cs.FlushFinalBlock();
                    cs.Close();
                }
                ms.Close();
                return Convert.ToBase64String(ms.ToArray());
            }
        }
        private static string DecryptRijndael(string value, string encryptionKey)
        {
            var key = Encoding.UTF8.GetBytes(encryptionKey); // must be 16 chars
            var rijndael = new RijndaelManaged
            {
                BlockSize = 128,
                IV = key,
                KeySize = 128,
                Key = key
            };
            try
            {
                var buffer = Convert.FromBase64String(value);

                var transform = rijndael.CreateDecryptor();
                string decrypted;
                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, transform, CryptoStreamMode.Write))
                    {
                        cs.Write(buffer, 0, buffer.Length);
                        cs.FlushFinalBlock();
                        decrypted = Encoding.UTF8.GetString(ms.ToArray());
                        cs.Close();
                    }
                    ms.Close();
                }

                return decrypted;
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}
