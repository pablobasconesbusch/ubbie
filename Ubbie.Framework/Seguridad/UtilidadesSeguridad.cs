﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Ubbie.Framework.Seguridad
{
    public class UtilidadesSeguridad
    {
        public static string CrearHash(string plainText)
        {
            var hash = SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(plainText));
            return Convert.ToBase64String(hash);
        }
        public static bool CompararHash(string plainText, string hashedText)
        {
            return CrearHash(plainText).Equals(hashedText);
        }

        public static int CreateNumericCode(int length, int minValue = 0, int maxValue = 9)
        {
            var code = string.Empty;
            var random = new Random();

            for (var i = 1; i <= length; i++)
            {
                var magic = random.Next(minValue, maxValue).ToString();
                if (i == 1 && magic == "0")
                {
                    i = 0;
                }
                else
                {
                    code += magic;
                }
            }

            return Convert.ToInt32(code);
        }
    }
}
