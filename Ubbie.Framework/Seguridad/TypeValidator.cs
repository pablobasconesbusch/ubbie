using System;
using System.Text.RegularExpressions;
using Ubbie.Framework.Extensiones;

namespace Ubbie.Framework.Seguridad
{
	public static class TypeValidator
	{
		public static bool IsCreditCard(string cardTypeCode, string cardNumber)
		{
			var number = new byte[16];

			var len = 0;
			for(var i = 0; i < cardNumber.Length; i++)
			{
				if(char.IsDigit(cardNumber, i))
				{
				    if (len == 16)
				    {
				        return false;
				    }
					number[len++] = byte.Parse(Convert.ToString(cardNumber[i]));
				}
			}

			switch (cardTypeCode)
			{
				// MasterCard
				case "116":
					if(len != 16) 
						return false;
					if(number[0] != 5 || number[1] == 0 || number[1] > 5) 
						return false;
					break;

				// Visa
				case "64":
					if(len != 16 && len != 13) 
						return false;
					if(number[0] != 4) 
						return false;
					break;
		
				// American
				case "65":
					if(len != 15) 
						return false;
					if(number[0] != 3 || (number[1] != 4 && number[1] != 7)) 
						return false;
					break;
		
				// Discover
				case "66":
					if(len != 16) 
						return false;
					if(number[0] != 6 || number[1] != 0 || number[2] != 1 || number[3] != 1) 
						return false;
					break;
		
				// Diners
				case "63":
					if(len != 14) 
						return false;
					if(number[0] != 3 || (number[1] != 0 && number[1] != 6 && number[1] != 8)
						|| number[1] == 0 && number[2] > 5) 
						return false;
					break;
			}

			// Use Luhn Algorithm to validate
			var sum = 0;
			for(var i = len - 1; i >= 0; i--)
			{
				if(i % 2 == len % 2)
				{
					var n = number[i] * 2;
					sum += (n / 10) + (n % 10);
				}
				else
					sum += number[i];
			}
			return (sum % 10 == 0);
		}

		public static bool IsValidEmailAddress(string value)
		{
            return !value.IsNullOrEmpty() && Regex.IsMatch(value, @"[a-zA-Z0-9]+(?:(\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\.[a-zA-Z0-9]*\.[a-zA-Z0-9]*\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", RegexOptions.IgnoreCase);
        }

        // 8 characters min, 1 uppercase and 1 number
        public static bool IsValidPassword(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            return Regex.IsMatch(value, @"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", RegexOptions.IgnoreCase);
        }


        public static bool IsInt(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            foreach (var c in value)
            {
                if (!char.IsNumber(c))
                {
                    return false;
                }
            }

            return true;
        }

    }
}
